<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use App\Services\Lists\ListDataService;

return [
    'Overheid:TaxonomieBeleidsagenda' => [
        'name'    => 'Overheid:TaxonomieBeleidsagenda',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/overheid_taxonomiebeleidsagenda.json',
        'aliases' => [],
    ],
    'DONL:Catalogs'                   => [
        'name'    => 'DONL:Catalogs',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/donl_catalogs.json',
        'aliases' => [],
    ],
    'DEXES:Catalogs' => [
        'name'    => 'DEXES:Catalogs',
        'source'  => [ListDataService::class, 'catalogsList'],
        'aliases' => [],
    ],
    'DONL:Organization'               => [
        'name'     => 'DONL:Organization',
        'source'   => 'https://waardelijsten.dcat-ap-donl.nl/donl_organization.json',
        'aliases'  => [],
    ],
    'EXTRA:Organizations' => [
        'name'    => 'EXTRA:Organizations',
        'source'  => resource_path('lists/organizations.json'),
        'aliases' => [],
    ],
    'DEXES:Organization'               => [
        'name'     => 'DEXES:Organization',
        'source'   => [ListDataService::class, 'organizationsList'],
        'aliases'  => [],
    ],
    'DONL:Language'                   => [
        'name'    => 'DONL:Language',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/donl_language.json',
        'aliases' => [],
    ],
    'DONL:License'                    => [
        'name'    => 'DONL:License',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/donl_license.json',
        'aliases' => [],
    ],
    'EXTRA:License' => [
        'name'    => 'EXTRA:License',
        'source'  => resource_path('lists/licenses.json'),
        'aliases' => [],
    ],
    'DEXES:License' => [
        'name'     => 'DEXES:License',
        'source'   => [ListDataService::class, 'licensesList'],
        'aliases'  => [],
    ],
    'Overheid:Openbaarheidsniveau'    => [
        'name'    => 'Overheid:Openbaarheidsniveau',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/overheid_openbaarheidsniveau.json',
        'aliases' => [],
    ],
    'DONL:WOBUitzondering'            => [
        'name'    => 'DONL:WOBUitzondering',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/donl_wobuitzondering.json',
        'aliases' => [],
    ],
    'Overheid:DatasetStatus'          => [
        'name'    => 'Overheid:DatasetStatus',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/overheid_dataset_status.json',
        'aliases' => [],
    ],
    'Overheid:Frequency'              => [
        'name'    => 'Overheid:Frequency',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/overheid_frequency.json',
        'aliases' => [],
    ],
    'ADMS:Changetype'                 => [
        'name'    => 'ADMS:Changetype',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/adms_changetype.json',
        'aliases' => [],
    ],
    'ADMS:DistributieStatus'          => [
        'name'    => 'ADMS:DistributieStatus',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/adms_distributiestatus.json',
        'aliases' => [],
    ],
    'MDR:Filetype'                    => [
        'name'    => 'MDR:Filetype',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/mdr_filetype_nal.json',
        'aliases' => [],
    ],
    'IANA:MediaTypes'                 => [
        'name'    => 'IANA:MediaTypes',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/iana_mediatypes.json',
        'aliases' => [],
    ],
    'DONL:DistributionType'           => [
        'name'    => 'DONL:DistributionType',
        'source'  => 'https://waardelijsten.DCAT-ap-donl.nl/donl_distributiontype.json',
        'aliases' => [],
    ],
    'DEXES:Dataspaces' => [
        'name'     => 'DEXES:Dataspaces',
        'source'   => [ListDataService::class, 'dataspacesList'],
        'aliases'  => [],
    ],
    'DEXES:Policies' => [
        'name'    => 'DEXES:Policies',
        'source'  => resource_path('lists/policies.json'),
        'aliases' => [],
    ],
    'DEXES:RuleFrameworks' => [
        'name'    => 'DEXES:RuleFrameworks',
        'source'  => [ListDataService::class, 'ruleFrameworksList'],
        'aliases' => [],
    ],
];
