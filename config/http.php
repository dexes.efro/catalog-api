<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

return [
    'rate-limiter' => [
        'limit'          => env('RATE_LIMITER_LIMIT', 180),
        'consumer-limit' => env('RATE_LIMITER_CONSUMER_LIMIT', 2000),
    ],
    'psr-http' => array_merge([
        'headers' => [
            'User-Agent' => env('HTTP_USER_AGENT', env('APP_NAME')),
        ],
        'timeout' => (int) env('HTTP_TIMEOUT', 5),
        'verify'  => (bool) env('HTTP_SSL_VERIFY', true),
    ], env('HTTP_USE_PROXY', false) ? [
        'proxy' => [
            'http'  => env('HTTP_PROXY'),
            'https' => env('HTTPS_PROXY'),
            'no'    => explode(',', env('NO_PROXY')),
        ],
    ] : []),
];
