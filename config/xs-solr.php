<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

return [
    'indexing'           => [
        'batch_size'                => env('SOLR_BATCH_SIZE', 250),
        'suggestion_build_interval' => env('SOLR_SUGGESTION_BUILD_INTERVAL', '*/15 * * * *'),
    ],
    'default_collection' => 'search',
    'collections'        => [
        'search'    => [
            'endpoint' => 'search',
            'id_field' => 'sys_id',
        ],
        'suggester' => [
            'endpoint' => 'suggester',
            'id_field' => 'id',
        ],
        'exchange'  => [
            'endpoint' => 'exchange',
            'id_field' => 'id',
        ],
    ],
    'solarium'           => [
        'endpoint' => (function() {
            $solrConnection = array_merge([
                'scheme' => env('SOLR_SCHEME', 'http'),
                'host'   => env('SOLR_HOST', '127.0.0.1'),
                'port'   => env('SOLR_PORT', 8983),
                'path'   => env('SOLR_PATH', '/'),
            ], env('SOLR_BASIC_AUTH', false) ? [
                'username' => env('SOLR_USERNAME'),
                'password' => env('SOLR_PASSWORD'),
            ] : []);
            $collectionKey = env('SOLR_CLOUD', false)
                ? 'collection'
                : 'core';

            return [
                'search'    => array_merge($solrConnection, [
                    $collectionKey => env('SOLR_COLLECTION_SEARCH', 'xs_search'),
                ]),
                'suggester' => array_merge($solrConnection, [
                    $collectionKey => env('SOLR_COLLECTION_SUGGESTER', 'xs_suggester'),
                ]),
                'exchange'  => array_merge($solrConnection, [
                    $collectionKey => env('SOLR_COLLECTION_EXCHANGE'),
                ]),
            ];
        })(),
    ],
];
