<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

return [
    'dexes-deal'          => [
        'search' => [
            'date-fields'    => [
                'created_at',
            ],
            'mapping'        => [
                'url'       => [
                    'id',
                ],
                'dataOwner' => [
                    'data_owner',
                ],
                'resource'  => [
                    'resource',
                ],
                'recipient' => [
                    'recipient',
                ],
                'createdAt' => [
                    'created_at',
                ],
            ],
            'search-mapping' => [
                'createdAt' => [
                    'sys_created',
                ],
            ],
        ],
    ],
    'dexes-offer'         => [
        'search' => [
            'date-fields' => [
                'created_at',
            ],
            'mapping'     => [
                'url'       => [
                    'id',
                ],
                'dataOwner' => [
                    'data_owner',
                ],
                'resource'  => [
                    'resource',
                ],
                'recipient' => [
                    'recipient',
                ],
                'createdAt' => [
                    'created_at',
                ],
            ],
        ],
    ],
    'dexes-dataspace'     => [
        'search' => [
            'date-fields' => [
                'created_at',
                'updated_at',
            ],
        ],
    ],
    'dexes-ruleframework' => [
    ],
    'dexes-dataservice'   => [
        'search' => [
            'date-fields' => [
                'sys_created',
                'sys_modified',
                'date_planned',
            ],
            'mapping'     => [
                'id'                    => [
                    'sys_id',
                ],
                'name'                  => [
                    'sys_name',
                ],
                'resource_language'     => [
                    'sys_language',
                ],
                'modification_date'     => [
                    'sys_modified',
                ],
                'identifier'            => [
                    'sys_uri',
                ],
                'title'                 => [
                    'title',
                ],
                'description'           => [
                    'description',
                ],
                'keyword'               => [
                    'keyword',
                ],
                'theme'                 => [
                    'theme',
                ],
                'publisher'             => [
                    'publisher',
                    'relation_organization',
                ],
                'source_catalog'        => [
                    'relation_catalog',
                ],
                'contact_point_name'    => [
                    'contact_point',
                ],
                'contact_point_title'   => [
                    'contact_point',
                ],
                'contact_point_address' => [
                    'contact_point',
                ],
                'contact_point_email'   => [
                    'contact_point',
                ],
                'contact_point_website' => [
                    'contact_point',
                ],
                'contact_point_phone'   => [
                    'contact_point',
                ],
                'access_rights'         => [
                    'access_rights',
                ],
                'conforms_to'           => [
                    'url',
                ],
                'license'               => [
                    'license',
                ],
                'relation'              => [
                    'relation',
                ],
                'release_date'          => [
                    'sys_created',
                ],
                'endpoint_description'  => [
                    'text',
                ],
                'endpoint_URL'          => [
                    'uri_synonym_nl',
                ],
                'serves_dataset'        => [
                    'relation_dataset',
                ],
                'dataspace_uri'         => [
                    'relation_dataspace',
                ],
                'has_policy' => [
                    'policies',
                ],
            ],
        ],
    ],
    'dexes-dataset'       => [
        'license_fallback' => 'http://standaarden.overheid.nl/owms/terms/licentieonbekend',
        'search'           => [
            'date-fields' => [
                'sys_created',
                'sys_modified',
                'date_planned',
                'temporal',
                'dates',
            ],
            'mapping'     => [
                'id'                     => [
                    'sys_id',
                ],
                'name'                   => [
                    'sys_name',
                ],
                'metadata_language'      => [
                    'sys_language',
                ],
                'language'               => [
                    'language',
                ],
                'metadata_created'       => [
                    'sys_created',
                ],
                'last_modified'          => [
                    'sys_modified',
                ],
                'identifier'             => [
                    'sys_uri',
                ],
                'title'                  => [
                    'title',
                ],
                'notes'                  => [
                    'description',
                ],
                'tags'                   => [
                    'keyword',
                ],
                'theme'                  => [
                    'theme',
                ],
                'modified'               => [
                    'text',
                ],
                'authority'              => [
                    'authority',
                    'relation_organization',
                ],
                'publisher'              => [
                    'publisher',
                    'relation_organization',
                ],
                'source_catalog'         => [
                    'relation_catalog',
                ],
                'contact_point_name'     => [
                    'contact_point',
                ],
                'contact_point_title'    => [
                    'contact_point',
                ],
                'contact_point_address'  => [
                    'contact_point',
                ],
                'contact_point_email'    => [
                    'contact_point',
                ],
                'contact_point_website'  => [
                    'contact_point',
                ],
                'contact_point_phone'    => [
                    'contact_point',
                ],
                'access_rights'          => [
                    'access_rights',
                ],
                'access_rights_argument' => [
                    'url',
                ],
                'dataset_status'         => [
                    'status',
                ],
                'url'                    => [
                    'url',
                ],
                'spatial_scheme'         => [
                    'spatial',
                ],
                'spatial_value'          => [
                    'spatial',
                ],
                'temporal_label'         => [
                    'temporal_label',
                ],
                'temporal_start'         => [
                    'temporal',
                ],
                'temporal_end'           => [
                    'temporal',
                ],
                'conforms_to'            => [
                    'url',
                ],
                'alternate_identifier'   => [
                    'url',
                ],
                'related_resource'       => [
                    'url',
                ],
                'source'                 => [
                    'url',
                ],
                'has_version'            => [
                    'url',
                ],
                'is_version_of'          => [
                    'url',
                ],
                'version'                => [
                    'text',
                ],
                'verion_notes'           => [
                    'text',
                ],
                'legal_foundation_uri'   => [
                    'legal',
                ],
                'legal_foundation_ref'   => [
                    'legal',
                ],
                'legal_foundation_label' => [
                    'legal',
                ],
                'date_planned'           => [
                    'dates',
                ],
                'documentation'          => [
                    'url',
                ],
                'frequency'              => [
                    'frequency',
                ],
                'license_id'             => [
                    'license',
                ],
                'provenance'             => [
                    'url',
                ],
                'sample'                 => [
                    'url',
                ],
                'high_value'             => [
                    'classification',
                ],
                'basis_register'         => [
                    'classification',
                ],
                'referentie_data'        => [
                    'classification',
                ],
                'national_coverage'      => [
                    'classification',
                ],
                'sector_registrations'   => [
                    'classification',
                ],
                'local_registrations'    => [
                    'classification',
                ],
                'dataspace_uri'          => [
                    'relation_dataspace',
                ],
                'has_policy' => [
                    'policies',
                ],
                'resources.*.has_policy' => [
                    'policies',
                ],
                'resources.*.format' => [
                    'distribution_file_types',
                ],
            ],
        ],
    ],
];
