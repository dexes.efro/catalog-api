<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
     */
    'default'  => env('LOG_CHANNEL', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
     */
    'channels' => [
        'local'     => [
            'driver'            => 'stack',
            'channels'          => ['daily', 'bugsnag'],
            'ignore_exceptions' => false,
        ],

        'daily'     => [
            'driver' => 'daily',
            'path'   => storage_path('logs/laravel.log'),
            'level'  => env('LOG_LEVEL', 'debug'),
            'days'   => 14,
        ],

        'bugsnag'   => [
            'driver' => 'bugsnag',
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Sensitive Headers
    |--------------------------------------------------------------------------
    |
    | Here you may configure any sensitive headers that should not be logged or
    | saved to the database when an HTTP Request to the API is made. The
    | headers are not case-sensitive.
    |
     */
    'sensitive-headers' => [
        'Authorization', // Authentication credentials for HTTP authentication.
        'From', // The email address of the user making the request
        'Proxy-Authorization', //Authorization credentials for connecting to a proxy.
    ],
];
