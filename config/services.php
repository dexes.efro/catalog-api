<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
     */
    'ishare' => [
        // The registered EORI for the Certificate
        'clientId' => '',

        'sattelites' => [
            // Info for the iShare test Satellite
            'ishare-test' => [
                'URL'              => '',
                'EORI'             => '',
                'private-key'      => '',
                'x5c'              => [
                ],
            ],
        ],
    ],
];
