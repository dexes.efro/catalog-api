<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
     */
    'default'     => env('DB_CONNECTION', 'mysql'),
    'mongodb'     => env('MONGODB_CONNECTION', 'mongodb'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
     */
    'connections' => [
        'sqlite'  => [
            'driver'                  => 'sqlite',
            'url'                     => env('DATABASE_URL'),
            'database'                => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix'                  => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'mysql'   => [
            'driver'         => 'mysql',
            'read'           => [
                'host' => env('DB_HOST_READ', '127.0.0.1'),
                'port' => env('DB_PORT_READ', 3306),
            ],
            'write'          => [
                'host' => env('DB_HOST_WRITE', '127.0.0.1'),
                'port' => env('DB_PORT_WRITE', 3306),
            ],
            'sticky'         => true,
            'database'       => env('DB_DATABASE', 'forge'),
            'username'       => env('DB_USERNAME', 'forge'),
            'password'       => env('DB_PASSWORD', ''),
            'charset'        => 'utf8mb4',
            'collation'      => 'utf8mb4_unicode_ci',
            'prefix'         => '',
            'prefix_indexes' => true,
            'strict'         => true,
            'engine'         => null,
            'options'        => [],
        ],

        'pgsql'   => [
            'driver'         => 'pgsql',
            'read'           => [
                'host' => env('DB_HOST_READ', '127.0.0.1'),
                'port' => env('DB_PORT_READ', 5432),
            ],
            'write'          => [
                'host' => env('DB_HOST_WRITE', '127.0.0.1'),
                'port' => env('DB_PORT_WRITE', 5432),
            ],
            'sticky'         => true,
            'database'       => env('DB_DATABASE', 'forge'),
            'username'       => env('DB_USERNAME', 'forge'),
            'password'       => env('DB_PASSWORD', ''),
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
            'schema'         => 'public',
            'sslmode'        => 'prefer',
            'options'        => [],
        ],

        'mongodb' => [
            'driver'     => 'mongodb',
            'host'       => env('MONGODB_HOST', 'localhost'),
            'port'       => env('MONGODB_PORT', 27017),
            'database'   => env('MONGODB_DATABASE', 'dexes_api_app'),
            'username'   => env('MONGODB_USERNAME', ''),
            'password'   => env('MONGODB_PASSWORD', ''),
            'options'    => [
                'database' => env('MONGODB_AUTH_DATABASE', 'admin'),
                'appname'  => env('APP_NAME', 'catalog-api'),
            ],
        ],

        'testing' => [
            'driver'   => 'sqlite',
            'database' => ':memory:',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
     */
    'migrations'  => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
     */
    'redis'       => (function() {
        $redis_hosts       = explode(',', env('REDIS_HOST', '127.0.0.1'));
        $redis_names       = ['default', 'cache', 'queue', 'session'];
        $redis_connections = [];

        if (count($redis_hosts) > 1) {
            foreach ($redis_names as $index => $connection) {
                $redis_connections[$connection] = array_merge(array_map(function($element) {
                    return 'tcp://' . $element . ':' . env('REDIS_PORT', 26379) . '?timeout=0.100';
                }, $redis_hosts), [
                    'options' => [
                        'replication' => 'sentinel',
                        'service'     => env('REDIS_SERVICE', 'xs-redis-cluster'),
                        'parameters'  => [
                            'password' => env('REDIS_PASSWORD'),
                            'database' => env('REDIS_' . strtoupper($connection) . '_DB', $index),
                            'prefix'   => env('REDIS_' . strtoupper($connection) . '_PREFIX', ''),
                        ],
                    ],
                ]);
            }
        } else {
            foreach ($redis_names as $index => $connection) {
                $redis_connections[$connection] = [
                    'host'     => $redis_hosts[0],
                    'port'     => env('REDIS_PORT', 6379),
                    'password' => env('REDIS_PASSWORD'),
                    'database' => env('REDIS_' . strtoupper($connection) . '_DB', $index),
                    'options'  => [
                        'prefix' => env('REDIS_' . strtoupper($connection) . '_PREFIX', ''),
                    ],
                ];
            }
        }

        return array_merge([
            'client'  => env('REDIS_CLIENT', 'phpredis'),
            'options' => [
                'cluster' => env('REDIS_CLUSTER', false),
            ],
        ], $redis_connections);
    })(),
];
