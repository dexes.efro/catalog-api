const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js("resources/js/app.js", "public/js");

mix.sass("resources/sass/app.scss", "public/css/app.css");
mix.sass("resources/sass/swagger.scss", "public/css/swagger.css");

mix.copy("resources/images/favico.ico", "public/favico.ico");

mix.version();
