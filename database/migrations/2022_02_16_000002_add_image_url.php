<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use Illuminate\Database\Migrations\Migration;

/**
 * Class AddImageUrl.
 */
class AddImageUrl extends Migration
{
    public const TABLE_NAME = 'rule_frameworks';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Maintained for legacy purposes.
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
    }
}
