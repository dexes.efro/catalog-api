<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use Illuminate\Database\Migrations\Migration;

/**
 * Class DealsIntIdToUuidId.
 */
class DealsIntIdToUuidId extends Migration
{
    public const TABLE_NAME = 'deals';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Maintained for legacy purposes.
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
    }
}
