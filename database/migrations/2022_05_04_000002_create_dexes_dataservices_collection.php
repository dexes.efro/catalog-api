<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use App\Models\DataService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;

/**
 * Class CreateDexesDataservicesCollection.
 */
class CreateDexesDataservicesCollection extends Migration
{
    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * Run the migration.
     */
    public function up(): void
    {
        if (app()->environment() === 'testing') {
            return;
        }

        Schema::create(DataService::COLLECTION_NAME, function(Blueprint $collection) {
            $collection->index('name');
        });
    }

    /**
     * Reverse the migration.
     */
    public function down(): void
    {
        Schema::table(DataService::COLLECTION_NAME, function(Blueprint $collection) {
            $collection->dropIfExists();
        });
    }
}
