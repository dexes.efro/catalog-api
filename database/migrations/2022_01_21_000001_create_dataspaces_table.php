<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataspacesTable extends Migration
{
    public const TABLE_NAME = 'dataspaces';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {
            $table->id();
            $table->string('title')->nullable(false);
            $table->string('name')->nullable(false)->unique();
            $table->string('asset_url')->nullable(true);
            $table->string('intro_text')->nullable(true);
            $table->string('uri')->nullable(false)->unique();
            $table->string('logo_url')->nullable(true);
            $table->string('join_url')->nullable(true);
            $table->unsignedBigInteger('ruleframework_id');
            $table->timestamps();

            $table->foreign('ruleframework_id')
                ->references('id')
                ->on('rule_frameworks');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
