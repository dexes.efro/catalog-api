<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Database\Factories;

use App\Models\Offer;
use Illuminate\Database\Eloquent\Factories\Factory;

class OfferFactory extends Factory
{
    /**
     * {@inheritdoc}
     */
    protected $model = Offer::class;

    /**
     * {@inheritdoc}
     */
    public function definition(): array
    {
        return [
            'id'   => $this->faker->unique()->randomNumber(3),
            'url'  => $this->faker->unique()->url(),
            'json' => '[]',
        ];
    }
}
