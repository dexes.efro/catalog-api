<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Database\Factories;

use App\Models\RuleFramework;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class RuleFrameworkFactory.
 */
class RuleFrameworkFactory extends Factory
{
    /**
     * {@inheritdoc}
     */
    protected $model = RuleFramework::class;

    /**
     * {@inheritdoc}
     */
    public function definition(): array
    {
        return [
            'id'         => $this->faker->unique()->randomNumber(3),
            'name'       => $this->faker->name(),
            'intro_text' => $this->faker->realText(),
        ];
    }
}
