<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use App\Http\Controllers\API\DataServiceController;
use App\Http\Controllers\API\DatasetController;
use App\Http\Controllers\API\DataspaceController;
use App\Http\Controllers\API\DCAT2\DataServiceController as DCAT2DataServiceController;
use App\Http\Controllers\API\DCAT2\DatasetController as DCAT2DatasetController;
use App\Http\Controllers\API\DCAT2\DistributionController as DCAT2DistributionController;
use App\Http\Controllers\API\DealController;
use App\Http\Controllers\API\IshareController;
use App\Http\Controllers\API\ListsController;
use App\Http\Controllers\API\OfferController;
use App\Http\Controllers\API\RuleFrameworkController;
use App\Http\Controllers\API\StatusController;
use Illuminate\Support\Facades\Route;

Route::get('/status', [StatusController::class, 'applicationStatus'])
    ->name('api.app.status');

Route::prefix('ishare')->group(function() {
    Route::get('/parties', [IshareController::class, 'index'])->name('api.ishare.parties');
});

Route::apiResources([
    'dexes-datasets'       => DatasetController::class,
    'dexes-dataservices'   => DataServiceController::class,
    'dexes-dataspaces'     => DataspaceController::class,
    'dexes-ruleframeworks' => RuleFrameworkController::class,
    'dexes-deals'          => DealController::class,
    'dexes-offers'         => OfferController::class,
], [
    'as'         => 'api',
    'parameters' => [
        'dexes-datasets'       => 'id',
        'dexes-dataservices'   => 'id',
        'dexes-dataspaces'     => 'id',
        'dexes-ruleframeworks' => 'id',
        'dexes-deals'          => 'id',
        'dexes-offers'         => 'id',
    ],
]);

Route::prefix('dcat2')->group(function() {
    Route::apiResources([
        'datasets'     => DCAT2DatasetController::class,
        'dataservices' => DCAT2DataServiceController::class,
    ], [
        'as'         => 'api.dcat2',
        'parameters' => [
            'datasets'     => 'id',
            'dataservices' => 'id',
        ],
    ]);
    Route::prefix('datasets/{dataset_id}')->group(function() {
        Route::apiResource('distributions', DCAT2DistributionController::class, [
            'as'         => 'api.dcat2.datasets',
            'parameters' => [
                'distributions' => 'id',
            ],
        ]
        );
    });
});
Route::prefix('lists')->group(function() {
    Route::get('/organizations', [ListsController::class, 'organizationsList'])->name('api.lists.organizations');
    Route::get('/dataspaces', [ListsController::class, 'dataspacesList'])->name('api.lists.dataspaces');
    Route::get('/licenses', [ListsController::class, 'licensesList'])->name('api.lists.licenses');
    Route::get('/policies', [ListsController::class, 'policieslist'])->name('api.lists.policies');
    Route::get('/ruleframeworks', [ListsController::class, 'ruleframeworksList'])->name('api.lists.ruleframeworks');
    Route::get('/catalogs', [ListsController::class, 'catalogsList'])->name('api.lists.catalogs');
});
