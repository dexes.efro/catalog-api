<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Providers;

use App\Console\Commands\TranslatableListsList;
use App\Http\Controllers\API\DataServiceController;
use App\Http\Controllers\API\DatasetController;
use App\Http\Controllers\API\DataspaceController;
use App\Http\Controllers\API\DealController;
use App\Http\Controllers\API\OfferController;
use App\Http\Controllers\API\RuleFrameworkController;
use App\Models\DataService;
use App\Models\Dataset;
use App\Repositories\DataServiceRepository;
use App\Repositories\DatasetRepository;
use App\Repositories\DataspaceRepository;
use App\Repositories\DealRepository;
use App\Repositories\OfferRepository;
use App\Repositories\RuleFrameworkRepository;
use App\Services\Ishare\Satellite\SatelliteClient;
use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;
use App\Services\Lists\ListClient\TranslatableList\DataLoader;
use App\Services\Lists\ListClient\TranslatableList\TranslatableListFactory;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Routing\Middleware\ThrottleRequestsWithRedis;
use Illuminate\Support\Facades\DB;
use Jenssegers\Mongodb\Connection;
use MongoDB\Collection as MongoCollection;
use MongoDB\Database;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Providers\BaseXpertSelectServiceProvider as ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register(): void
    {
        parent::register();

        if (config('cache.default') === 'redis') {
            $this->app->bind(ThrottleRequests::class, function($app) {
                return $app->make(ThrottleRequestsWithRedis::class);
            });
        }

        $this->app->bind(Database::class, function(): Database {
            /** @var Connection $connection */
            $connection   = DB::connection('mongodb');
            $databaseName = config('database.connections.mongodb.database');

            return $connection->getMongoClient()->selectDatabase($databaseName);
        });

        $this->bindDealImplementations();
        $this->bindDataserviceImplementations();
        $this->bindDatasetImplementations();
        $this->bindDataspaceImplementations();
        $this->bindOfferImplementations();
        $this->bindRuleframeworkImplementations();
        $this->bindIShareImplementations();
        $this->bindListClientImplementations();
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                TranslatableListsList::class,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function repositoryImplementations(): array
    {
        return [
            'dexes-dataset'       => DatasetRepository::class,
            'dexes-dataservice'   => DataServiceRepository::class,
            'dexes-offer'         => OfferRepository::class,
            'dexes-deal'          => DealRepository::class,
            'dexes-dataspace'     => DataspaceRepository::class,
            'dexes-ruleframework' => RuleFrameworkRepository::class,
        ];
    }

    /**
     * Enables Laravel to inject Deal implementations.
     */
    private function bindDealImplementations(): void
    {
        $this->app->when(DealController::class)
            ->needs(RepositoryInterface::class)
            ->give(DealRepository::class);
    }

    /**
     * Enables Laravel to inject Offer implementations.
     */
    private function bindOfferImplementations(): void
    {
        $this->app->when(OfferController::class)
            ->needs(RepositoryInterface::class)
            ->give(OfferRepository::class);
    }

    /**
     * Enables Laravel to inject the various DataService implementations.
     */
    private function bindDataserviceImplementations(): void
    {
        $this->app->when(DataServiceRepository::class)
            ->needs(MongoCollection::class)
            ->give(function() {
                $database       = $this->app->make(Database::class);

                return $database->selectCollection(DataService::COLLECTION_NAME);
            });

        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(DataServiceRepository::class);
    }

    /**
     * Enables Laravel to inject the various Dataset implementations.
     */
    private function bindDatasetImplementations(): void
    {
        $this->app->when(DatasetRepository::class)
            ->needs(MongoCollection::class)
            ->give(function() {
                $database       = $this->app->make(Database::class);

                return $database->selectCollection(Dataset::COLLECTION_NAME);
            });

        $this->app->when(DataSetController::class)
            ->needs(RepositoryInterface::class)
            ->give(DataSetRepository::class);
    }

    /**
     * Enables Laravel to inject the various Ruleframework implementations.
     */
    private function bindRuleframeworkImplementations(): void
    {
        $this->app->when(RuleFrameworkController::class)
            ->needs(RepositoryInterface::class)
            ->give(RuleFrameworkRepository::class);
    }

    /**
     * Enables Laravel to inject the various Dataspace implementations.
     */
    private function bindDataspaceImplementations(): void
    {
        $this->app->when(DataspaceController::class)
            ->needs(RepositoryInterface::class)
            ->give(DataspaceRepository::class);
    }

    /**
     * Enables Laravel to inject various iShare related classes.
     */
    private function bindIShareImplementations(): void
    {
        $this->app->bind(SatelliteClient::class, function() {
            return new SatelliteClient(
                config('services.ishare.sattelites.ishare-test.EORI'),
                config('services.ishare.sattelites.ishare-test.URL'),
                config('services.ishare.clientId'),
                config('services.ishare.sattelites.ishare-test.x5c'),
                config('services.ishare.sattelites.ishare-test.private-key')
            );
        });
    }

    /**
     * Enables Laravel to inject the various ListClient implementations.
     */
    private function bindListClientImplementations(): void
    {
        $this->app->bind(TranslatableListFactory::class, function() {
            return new TranslatableListFactory(
                TranslatableListFactory::LANGUAGE_MAP[config('app.locale')],
                $this->app->make(DataLoader::class)
            );
        });

        $this->app->bind(DataFormatter::class, DataFormatter::class);
        $this->app->bind(DataLoader::class, DataLoader::class);

        $this->app->bind(ListClient::class, function() {
            return new ListClient(
                $this->app->make(TranslatableListFactory::class),
                $this->app->make(DataFormatter::class),
                config('xs-lists', []),
            );
        });
    }
}
