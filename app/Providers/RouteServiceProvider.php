<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Laravel\Sanctum\PersonalAccessToken;
use XpertSelect\Framework\Models\Auth\Application;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot(): void
    {
        $this->configureRateLimiting();

        if ($this->routesAreCached()) {
            return;
        }

        $this->routes(function() {
            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/api.php'));

            if (!config('xs-framework.enable_gui', false)) {
                Route::middleware('web')
                    ->group(function(Router $router) {
                        $router->redirect('/', config('l5-swagger.documentations.default.routes.api'));
                    });
            } else {
                Route::middleware('web')
                    ->group(base_path('routes/web.php'));
            }
        });
    }

    /**
     * Configure the rate limiters for the application.
     */
    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function(Request $request) {
            $potentialToken = $request->header('Authorization') ?? [''];
            $potentialToken = is_array($potentialToken) ? $potentialToken[0] : strval($potentialToken);
            $potentialToken = trim(str_replace('Bearer', '', $potentialToken));

            $token = PersonalAccessToken::findToken($potentialToken);

            if (null === $token) {
                return $this->defaultRateLimiting($request);
            }

            if (!isset($token->tokenable)) {
                return $this->defaultRateLimiting($request);
            }

            if ($token->tokenable instanceof Application) {
                return $this->createLimiter($request, (int) config('http.rate-limiter.consumer-limit', 80));
            }

            return $this->defaultRateLimiting($request);
        });
    }

    /**
     * Create a Rate Limit limiter.
     *
     * @param Request $request The incoming HTTP Request
     * @param int     $limit   The maximum amounts per request per minute
     *
     * @return Limit The Limit object
     */
    private function createLimiter(Request $request, int $limit): Limit
    {
        return Limit::perMinute($limit)
            ->by(optional($request->user())->id ?: $request->ip())
            ->response(function(Request $request, array $headers) {
                return new JsonResponse(status: 429, headers: $headers);
            });
    }

    /**
     * Default rate limiting.
     *
     * @param Request $request The request
     */
    private function defaultRateLimiting(Request $request): Limit
    {
        return $this->createLimiter($request, (int) config('http.rate-limiter.limit', 80));
    }
}
