<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\SwaggerProcessors;

use App\Services\Lists\ListClient\ListClient;
use OpenApi\Analysis;
use OpenApi\Annotations\Schema;

/**
 * Use the Schema context to extract useful information and inject that into the annotation.
 *
 * Merges properties.
 */
class AddXsListSchemas
{
    public function __invoke(Analysis $analysis): void
    {
        /** @var ListClient $listClient */
        $listClient = app(ListClient::class);

        foreach ($listClient->names() as $name) {
            $data = $listClient->list($name);

            $openapi = $analysis->openapi;

            if (!is_null($openapi)) {
                $openapi->components->schemas[] = new Schema([
                    'schema'      => $name,
                    'type'        => 'string',
                    'description' => sprintf('Acceptable values for fields that use the %s taxonomy. Source: %s', $name, $data->getSource()),
                    'enum'        => array_keys($data->getData()),
                ]);
            }
        }
    }
}
