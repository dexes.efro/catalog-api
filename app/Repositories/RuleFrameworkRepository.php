<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Repositories;

use App\Models\RuleFramework;
use Illuminate\Database\Eloquent\Builder;
use XpertSelect\Framework\Services\Repositories\BaseEloquentRepository;

/**
 * Class RuleFrameworkRepository.
 *
 * Repository implementation for interacting with Dexes RuleFrameworks  by using Eloquent as a
 * back-end.
 */
class RuleFrameworkRepository extends BaseEloquentRepository
{
    /**
     * {@inheritdoc}
     */
    public function getIDField(): string
    {
        return 'id';
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'dexes-ruleframework';
    }

    /**
     * {@inheritdoc}
     */
    public function nameSourceField(): ?string
    {
        return 'title';
    }

    /**
     * {@inheritdoc}
     */
    protected function getQueryBuilder(): Builder
    {
        return RuleFramework::query();
    }
}
