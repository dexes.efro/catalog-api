<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Repositories;

use App\Models\Deal;
use Illuminate\Database\Eloquent\Builder;
use XpertSelect\Framework\Services\Repositories\BaseEloquentRepository;

/**
 * Class DealRepository.
 *
 * Repository implementation for interacting with Dexes deals by using Eloquent as a back-end.
 */
class DealRepository extends BaseEloquentRepository
{
    /**
     * {@inheritdoc}
     */
    public function getIDField(): string
    {
        return 'id';
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'dexes-deal';
    }

    /**
     * {@inheritdoc}
     */
    public function nameSourceField(): ?string
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    protected function getQueryBuilder(): Builder
    {
        return Deal::query();
    }
}
