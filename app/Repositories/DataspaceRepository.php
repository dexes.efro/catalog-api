<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Repositories;

use App\Models\Dataspace;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use XpertSelect\Framework\Contracts\Repositories\SearchableRecordsInterface;
use XpertSelect\Framework\Services\Repositories\BaseEloquentRepository;

/**
 * Class DataspaceRepository.
 *
 * Repository implementation for interacting with Dexes Dataspaces by using Eloquent as a back-end.
 */
class DataspaceRepository extends BaseEloquentRepository implements SearchableRecordsInterface
{
    /**
     * {@inheritdoc}
     */
    public function getIDField(): string
    {
        return 'id';
    }

    /**
     * {@inheritdoc}
     */
    public function getQueryBuilder(): Builder
    {
        return Dataspace::query();
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'dexes-dataspace';
    }

    /**
     * {@inheritdoc}
     */
    public function nameSourceField(): ?string
    {
        return 'title';
    }

    /**
     * {@inheritdoc}
     */
    public function toSearchableDocuments(Arrayable|array $record): array
    {
        if (!$record instanceof Dataspace) {
            return [];
        }

        return [
            'search' => $record->asSearchSolrDocument(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toSearchableDocumentIdentifiers(Arrayable|array $record): array
    {
        if (!$record instanceof Dataspace) {
            return [];
        }

        return ['search' => $record->id];
    }

    /**
     * {@inheritdoc}
     */
    public function identifyingQueriesPerCollection(): array
    {
        return [
            'search'    => 'sys_type:"dataspace"',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toFilterSuggestionDocuments(Arrayable|array $record): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function toTitleSuggestionDocuments(Arrayable|array $record): array
    {
        return [];
    }
}
