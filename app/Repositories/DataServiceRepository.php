<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Repositories;

use App\Models\DataService;
use App\Services\Search\DCATSearchDocument;
use App\Services\Suggest\DCATTitleSuggestionDocument;
use Illuminate\Contracts\Support\Arrayable;
use Jenssegers\Mongodb\Eloquent\Builder;
use XpertSelect\Framework\Contracts\Repositories\SearchableRecordsInterface;
use XpertSelect\Framework\Services\Repositories\BaseMongoRepository;

/**
 * Class DataServiceRepository.
 *
 * XpertSelect repository exposing CRUD functionality for Dexes dataservices.
 */
class DataServiceRepository extends BaseMongoRepository implements SearchableRecordsInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'dexes-dataservice';
    }

    /**
     * {@inheritdoc}
     */
    public function nameSourceField(): ?string
    {
        return 'title';
    }

    /**
     * {@inheritdoc}
     */
    public function getIDField(): string
    {
        return '_id';
    }

    /**
     * {@inheritdoc}
     */
    public function toSearchableDocuments(Arrayable|array $record): array
    {
        $dataservice = is_array($record) ? $record : $record->toArray();

        return [
            'search' => DCATSearchDocument::fromDataService($dataservice)->getFields(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toTitleSuggestionDocuments(Arrayable|array $record): array
    {
        $dataservice = is_array($record) ? $record : $record->toArray();

        return [
            //            $dataservice
            DCATTitleSuggestionDocument::fromDataService($dataservice),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toFilterSuggestionDocuments(Arrayable|array $record): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function toSearchableDocumentIdentifiers(Arrayable|array $record): array
    {
        $dataservice = is_array($record) ? $record : $record->toArray();

        return [
            'search' => $dataservice['id'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function identifyingQueriesPerCollection(): array
    {
        return [
            'search'    => 'sys_type:"dataservice"',
            'suggester' => 'type:"dataservice"',
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getQueryBuilder(): Builder
    {
        return DataService::query();
    }
}
