<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Repositories;

use App\Models\Dataset;
use App\Services\Search\DCATSearchDocument;
use App\Services\Suggest\DCATTitleSuggestionDocument;
use Illuminate\Contracts\Support\Arrayable;
use Jenssegers\Mongodb\Eloquent\Builder;
use MongoDB\BSON\ObjectId;
use XpertSelect\Framework\Contracts\Repositories\SearchableRecordsInterface;
use XpertSelect\Framework\Services\Repositories\BaseMongoRepository;

/**
 * Class DatasetRepository.
 *
 * XpertSelect repository exposing CRUD functionality for Dexes datasets.
 */
class DatasetRepository extends BaseMongoRepository implements SearchableRecordsInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(array $attributes)
    {
        return parent::create($this->addIdentifierToDistributions($attributes));
    }

    /**
     * {@inheritdoc}
     */
    public function update(string $id, array $attributes)
    {
        return parent::update($id, $this->addIdentifierToDistributions($attributes));
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'dexes-dataset';
    }

    /**
     * {@inheritdoc}
     */
    public function nameSourceField(): ?string
    {
        return 'title';
    }

    /**
     * {@inheritdoc}
     */
    public function getIDField(): string
    {
        return '_id';
    }

    /**
     * {@inheritdoc}
     */
    public function toSearchableDocuments(Arrayable|array $record): array
    {
        $dataset = is_array($record) ? $record : $record->toArray();

        return [
            'search' => DCATSearchDocument::fromDataset($dataset)->getFields(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toTitleSuggestionDocuments(Arrayable|array $record): array
    {
        $dataset = is_array($record) ? $record : $record->toArray();

        return [
            DCATTitleSuggestionDocument::fromDataset($dataset),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toFilterSuggestionDocuments(Arrayable|array $record): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function toSearchableDocumentIdentifiers(Arrayable|array $record): array
    {
        $dataset = is_array($record) ? $record : $record->toArray();

        return [
            'search' => $dataset['id'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function identifyingQueriesPerCollection(): array
    {
        return [
            'search'    => 'sys_type:"dataset"',
            'suggester' => 'type:"dataset"',
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getQueryBuilder(): Builder
    {
        return Dataset::query();
    }

    /**
     * Loops through all distributions and adds an identifier if it doesn't exist yet.
     *
     * @param array<string, mixed> $dataset The dataset
     *
     * @return array<string, mixed> The dataset with ids added to the distributions
     */
    private function addIdentifierToDistributions(array $dataset): array
    {
        if (!array_key_exists('resources', $dataset)) {
            return $dataset;
        }

        foreach ($dataset['resources'] as &$resource) {
            if (!array_key_exists('id', $resource)) {
                $resource['id'] = (new ObjectId())->__toString();
            }
        }
        unset($resource);

        return $dataset;
    }
}
