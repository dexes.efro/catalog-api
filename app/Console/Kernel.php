<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Console;

use Illuminate\Config\Repository;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use XpertSelect\Framework\Console\Commands\FailedFormRequestsCleanup;
use XpertSelect\Framework\Console\Commands\HttpRequestCleanup;

class Kernel extends ConsoleKernel
{
    /**
     * {@inheritdoc}
     *
     * @var string[]
     */
    protected $commands = [
    ];

    /**
     * {@inheritdoc}
     */
    protected function schedule(Schedule $schedule): void
    {
        /** @var Repository $config */
        $config = $this->app->offsetGet('config');

        $schedule->exec(HttpRequestCleanup::class)
            ->onOneServer()
            ->runInBackground()
            ->cron($config->get('logging.cleanup.http-requests.cron_string'));

        $schedule->exec(FailedFormRequestsCleanup::class)
            ->onOneServer()
            ->runInBackground()
            ->cron($config->get('logging.cleanup.failed-form-requests.cron_string'));
    }

    /**
     * {@inheritdoc}
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
