<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Console\Commands;

use App\Services\Lists\ListClient\ListClient;
use Illuminate\Console\Command;

/**
 * Class TranslatableListsList.
 *
 * Implements an Artisan command that lists the configured Apache Solr collections.
 */
final class TranslatableListsList extends Command
{
    /**
     * {@inheritdoc}
     */
    protected $signature = 'translatable-lists:list';

    /**
     * {@inheritdoc}
     */
    protected $description = 'Lists the configured translatable lists';

    /**
     * Execute the console command.
     *
     * @param ListClient $listClient The client for interacting with the lists
     *
     * @return int The return code of the command
     */
    public function handle(ListClient $listClient): int
    {
        $this->table(['List', 'Aliases', 'Source', 'Entries'],
            array_map(function(string $listName) use ($listClient) {
                $list = $listClient->list($listName);

                return [
                    $list->getTitle(),
                    implode(', ', $list->getAliases()),
                    $list->getSource(),
                    $list->size(),
                ];
            }, $listClient->names())
        );

        return 0;
    }
}
