<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * {@inheritdoc}
     */
    protected $addHttpCookie = false;

    /**
     * {@inheritdoc}
     *
     * @var array<int, string>
     */
    protected $except = [
    ];
}
