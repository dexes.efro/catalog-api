<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;

/**
 * Class DataServiceRequest.
 *
 * This represents an incoming HTTP dataService request (to store/update a dataService)
 *
 * @OA\Response(
 *     response="DataServiceListResponse",
 *     description="A List of Dexes DataServices as a response.",
 *
 *     @OA\MediaType(
 *      mediaType="application/json",
 *
 *      @OA\Schema(schema="dataServicelist",type="array",@OA\Items(ref="#/components/schemas/DataService")),
 *     )
 * ),
 *
 * @OA\Response(
 *     response="DataServiceResourceResponse",
 *     description="A Dexes DataService as a response.",
 *
 *     @OA\MediaType(
 *      mediaType="application/json",
 *
 *      @OA\Schema(ref="#/components/schemas/DataService"),
 *     )
 * ),
 * @OA\Schema(
 *     schema="DataService",
 *     description="",
 *
 *      @OA\Property(
 *        property="title",
 *        type="string",
 *      ),
 *      @OA\Property(
 *        property="name",
 *        type="string",
 *      ),
 *      @OA\Property(
 *        property="access_rights",
 *        type="string",
 *        ref="#/components/schemas/Overheid:Openbaarheidsniveau",
 *      ),
 *      @OA\Property(
 *        property="contact_point",
 *        type="object",
 *        ref="#/components/schemas/ContactPoint",
 *      ),
 *      @OA\Property(
 *          property="description",
 *          type="string",
 *      ),
 *      @OA\Property(
 *          property="has_policy",
 *          type="array",
 *
 *          @OA\Items(
 *              ref="#/components/schemas/DEXES:Policies",
 *          )
 *      ),
 *
 *      @OA\Property(
 *        property="identifier",
 *        type="string",
 *      ),
 *      @OA\Property(
 *        property="license",
 *        type="string",
 *        ref="#/components/schemas/DEXES:License",
 *      ),
 *      @OA\Property(
 *        property="resource_language",
 *        type="array",
 *
 *        @OA\Items(ref="#/components/schemas/DONL:Language"),
 *      ),
 *
 *      @OA\Property(
 *        property="dataspace_uri",
 *        type="string",
 *        ref="#/components/schemas/DEXES:Dataspaces",
 *      ),
 *      @OA\Property(
 *        property="publisher",
 *        type="string",
 *        ref="#/components/schemas/DEXES:Organization",
 *      ),
 *      @OA\Property(
 *        property="theme",
 *        type="array",
 *
 *        @OA\Items(
 *            ref="#/components/schemas/Overheid:TaxonomieBeleidsagenda",
 *        ),
 *      ),
 *
 *      @OA\Property(
 *        property="endpoint_url",
 *        type="string",
 *      ),
 *      @OA\Property(
 *        property="endpoint_description",
 *        type="string",
 *      )
 * )
 */
class DataServiceRequest extends DCATRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply for this request.
     *
     * @param ListClient    $listClient    The list client to get the value lists
     * @param DataFormatter $dataFormatter The data formatter to format value lists
     *
     * @return array<string, mixed> The validation rules that apply to the request
     */
    public function rules(ListClient $listClient,
                          DataFormatter $dataFormatter): array
    {
        return [
            'name'                        => ['nullable'],
            'access_rights'               => [
                'nullable',
                $this->ruleFromList('Overheid:Openbaarheidsniveau', $listClient, $dataFormatter),
            ],
            'conforms_to'                 => ['nullable', 'array'],
            'contact_point.name'          => ['required'],
            'contact_point.title'         => [''],
            'contact_point.address'       => [''],
            'contact_point.email'         => [
                'required_without_all:contact_point.website,contact_point.phone',
                'email',
            ],
            'contact_point.website'       => [
                'required_without_all:contact_point.email,contact_point.phone',
            ],
            'contact_point.phone'         => [
                'required_without_all:contact_point.email,contact_point.website',
            ],

            'creator'                     => [
                'nullable',
                $this->ruleFromList('DEXES:Organization', $listClient, $dataFormatter),
            ],
            'description'                 => ['nullable'],
            'has_policy'                  => [
                'nullable',
                'array',
                $this->ruleFromList('DEXES:Policies', $listClient, $dataFormatter),
            ],
            'identifier'                  => ['required'],
            'is_referenced_by'            => ['nullable'],
            'keyword'                     => ['nullable', 'array'],
            'landing_page'                => ['nullable'],
            'license'                     => [
                'nullable',
                $this->ruleFromList('DEXES:License', $listClient, $dataFormatter),
            ],
            'resource_language'           => [
                'nullable',
                'array',
                $this->ruleFromList('DONL:Language', $listClient, $dataFormatter),
            ],
            'relation'                    => ['nullable'],
            'rights'                      => ['nullable'],
            'qualified_relation'          => ['nullable'],
            'dataspace_uri'               => [
                'required',
                $this->ruleFromList('DEXES:Dataspaces', $listClient, $dataFormatter),
            ],
            'publisher'                   => [
                'required',
                $this->ruleFromList('DEXES:Organization', $listClient, $dataFormatter),
            ],
            'release_date'                => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'theme'                       => [
                'nullable',
                'array',
                $this->ruleFromList('Overheid:TaxonomieBeleidsagenda', $listClient, $dataFormatter),
            ],
            'title'                       => ['required', 'max:200'],
            'type'                        => [
                'nullable',
                'array',
                // probably also some sort of list https://www.w3.org/TR/vocab-DCAT-2/#Property:resource_type
            ],
            'modification_date'           => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'qualified_attribution'       => [
                'nullable',
                $this->ruleFromList('DEXES:Organization', $listClient, $dataFormatter),
            ],
            'endpoint_description'        => ['nullable'],
            'endpoint_url'                => ['required'],
            'serves_dataset'              => ['nullable'],
            'consumer_name'               => ['nullable'],
            'consumer_checksum'           => ['nullable'],
        ];
    }
}
