<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Models\Deal;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DealPostRequest.
 *
 * This represents an incoming HTTP Deal Request (to store a deal)
 */
class DealPostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply for this request.
     *
     * @return array<string, array<int, string>>
     */
    public function rules(): array
    {
        $rules          = Deal::rules();
        $rules['id'][]  = 'unique:App\Models\Deal,id';
        $rules['url'][] = 'unique:App\Models\Deal,url';

        return $rules;
    }
}
