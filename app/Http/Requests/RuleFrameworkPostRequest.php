<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Models\RuleFramework;
use XpertSelect\Framework\Http\Requests\LoggableFormRequest;

/**
 * Class RuleFrameworkPostRequest.
 */
class RuleFrameworkPostRequest extends LoggableFormRequest
{
    /**
     * {@inheritdoc}
     */
    protected bool $withBody = true;

    /**
     * Get the validation rules that apply for this request.
     *
     * @return array<string, array<int, string>>
     */
    public function rules(): array
    {
        $rules            = RuleFramework::rules();
        $rules['name'][]  = 'unique:App\Models\RuleFramework,name';
        $rules['title'][] = 'unique:App\Models\RuleFramework,title';
        $rules['url'][]   = 'unique:App\Models\RuleFramework,url';
        $rules['image'][] = 'required';

        return $rules;
    }
}
