<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests\DCAT2;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;

/**
 * Class PatchDataServiceRequest.
 */
class PatchDataServiceRequest extends PatchResourceRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ListClient    $listClient    The list client to get the value lists
     * @param DataFormatter $dataFormatter The data formatter to format value lists
     *
     * @return array<string, mixed> The validation rules that apply to the request
     */
    public function rules(ListClient $listClient,
                          DataFormatter $dataFormatter): array
    {
        return array_merge($this->resourceRules($listClient, $dataFormatter), [
            // DataService Specific
            'attributes.dcat:endpointURL'         => ['required', 'sometimes', 'url'],
            'attributes.dcat:endpointDescription' => ['required', 'sometimes'],
            'attributes.dcat:servesDataset'       => ['nullable'],

            'meta.dataspace_uri'            => [
                'required', 'sometimes',
                $this->ruleFromList('DEXES:Dataspaces', $listClient, $dataFormatter),
            ],
            'meta.consumer_name'            => ['nullable'],
            'meta.consumer_checksum'        => ['nullable'],
            'meta.name'                     => ['required', 'sometimes'],
        ]);
    }
}
