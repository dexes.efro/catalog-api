<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests\DCAT2;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;

/**
 * Class PostDatasetRequest.
 *
 * @OA\Response(
 *   response="CreatedDCAT2DatasetResourceResponse",
 *   description="A created DCAT-2 dataset as a JSON:API resource.",
 *
 *   @OA\MediaType(
 *     mediaType="application/vnd.api+json",
 *
 *     @OA\Schema(ref="#/components/schemas/DCAT2DatasetDocument")
 *   ),
 *
 *   @OA\Header(
 *     header="Location",
 *     description="Header identifying the location of the newly created resource.",
 *
 *     @OA\Schema(
 *       type="string",
 *       format="uri"
 *     )
 *   )
 * )
 *
 * @OA\Response(
 *     response="DCAT2DatasetListResponse",
 *     description="A List of DCAT2.0 Datasets as a response.",
 *
 *     @OA\MediaType(
 *       mediaType="application/vnd.api+json",
 *
 *       @OA\Schema(ref="#/components/schemas/DCAT2DatasetListDocument")
 *     )
 * )
 *
 * @OA\Response(
 *     response="DCAT2DatasetResourceResponse",
 *     description="A DCAT2.0 Dataset as a JSON:API resource.",
 *
 *     @OA\MediaType(
 *      mediaType="application/vnd.api+json",
 *
 *      @OA\Schema(ref="#/components/schemas/DCAT2DatasetDocument")
 *     )
 * )
 * @OA\Schema(
 *   schema="DCAT2DatasetDocument",
 *   description="DCAT 2 Dataset Document",
 *
 *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
 *   @OA\Property(property="data", ref="#/components/schemas/DCAT2DatasetResource"),
 *   @OA\Property(
 *     property="included",
 *     readOnly=true,
 *     type="array",
 *
 *     @OA\Items(ref="#/components/schemas/DCAT2DistributionResource")
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="DCAT2DatasetListDocument",
 *   description="DCAT 2 Dataset List Document",
 *
 *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
 *   @OA\Property(property="meta", ref="#/components/schemas/SearchResultsMeta"),
 *   @OA\Property(
 *     property="data",
 *     type="array",
 *
 *     @OA\Items(ref="#/components/schemas/DCAT2DatasetListResource")
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="DCAT2DatasetListResource",
 *   description="DCAT 2 Dataset List Resource",
 *
 *   @OA\Property(
 *     property="type",
 *     readOnly=true,
 *     type="string",
 *     example="dataset"
 *   ),
 *   @OA\Property(
 *     property="id",
 *     readOnly=true,
 *     type="string",
 *     example="luchtfoto-2022"
 *   ),
 *   @OA\Property(
 *     property="attributes",
 *     type="object",
 *     @OA\Property(
 *       property="dct:title",
 *       type="string",
 *     ),
 *   ),
 *   @OA\Property(
 *     property="meta",
 *     type="object",
 *     @OA\Property(
 *       property="visibility",
 *       type="string",
 *       enum={"public", "private"},
 *     ),
 *   ),
 *   @OA\Property(
 *     property="links",
 *     readOnly=true,
 *     type="object",
 *     @OA\Property(
 *       property="self",
 *       type="string",
 *       example="https://example.com/dataset/luchtfoto-2022"
 *     )
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="DCAT2DatasetResource",
 *   description="DCAT 2 Dataset Resource",
 *
 *   @OA\Property(
 *     property="type",
 *     readOnly=true,
 *     type="string",
 *     example="dataset"
 *   ),
 *   @OA\Property(
 *     property="id",
 *     readOnly=true,
 *     type="string",
 *     example="luchtfoto-2022"
 *   ),
 *   @OA\Property(
 *     property="attributes",
 *     type="object",
 *     allOf={
 *
 *      @OA\Schema(ref="#/components/schemas/DCAT2Resources"),
 *     },
 *
 *     @OA\Property(
 *       property="dct:accrualPeriodicity",
 *       type="string",
 *       ref="#/components/schemas/Overheid:Frequency",
 *       description="The frequency at the which dataset is published.",
 *     ),
 *     @OA\Property(
 *       property="dct:spatial",
 *       type="object",
 *       description="The geographical area covered by the dataset.",
 *       @OA\Property(
 *         property="locn:geometry",
 *         type="string",
 *       ),
 *       @OA\Property(
 *         property="dcat:bbox",
 *         type="string",
 *       ),
 *       @OA\Property(
 *         property="dcat:centroid",
 *         type="string",
 *       ),
 *     ),
 *     @OA\Property(
 *       property="dcat:spatialResolutionInMeters",
 *       type="string",
 *       description="Minimum spatial separation resolvable in a dataset, measured in meters.",
 *     ),
 *     @OA\Property(
 *       property="dct:temporal",
 *       type="object",
 *       description="The temporal period that the dataset covers.",
 *       @OA\Property(
 *         property="dcat:startDate",
 *         type="string",
 *       ),
 *       @OA\Property(
 *         property="dcat:endDate",
 *         type="string",
 *       ),
 *       @OA\Property(
 *         property="time:hasBeginning",
 *         type="array",
 *
 *         @OA\Items(type="string"),
 *       ),
 *
 *       @OA\Property(
 *         property="time:hasEnd",
 *         type="array",
 *
 *         @OA\Items(type="string"),
 *       ),
 *     ),
 *
 *     @OA\Property(
 *       property="dcat:temporalResolution",
 *       type="string",
 *       description="Minimum time period resolvable in the dataset.",
 *     ),
 *     @OA\Property(
 *       property="prov:wasGeneratedBy",
 *       type="string",
 *       description="An activity that generated, or provides the business context for, the creation of the dataset."
 *     ),
 *   ),
 *   @OA\Property(
 *     property="meta",
 *     type="object",
 *     @OA\Property(
 *       property="dataspace_uri",
 *       type="string",
 *       description="A URI pointing to the Dexes Dataspaces this Dataset belongs to",
 *       ref="#/components/schemas/DEXES:Dataspaces",
 *     ),
 *     @OA\Property(
 *       property="consumer_name",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="consumer_checksum",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="metadata_language",
 *       type="string",
 *       ref="#/components/schemas/DONL:Language",
 *     ),
 *     @OA\Property(
 *       property="name",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="dataset_status",
 *       type="string",
 *       ref="#/components/schemas/Overheid:DatasetStatus",
 *     ),
 *     @OA\Property(
 *       property="high_value",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="basis_register",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="source_catalog",
 *       type="string",
 *       ref="#/components/schemas/DONL:Catalogs",
 *     ),
 *     required={
 *       "dataspace_uri"
 *     }
 *   ),
 *   @OA\Property(
 *     property="links",
 *     readOnly=true,
 *     type="object",
 *     @OA\Property(
 *       property="self",
 *       type="string",
 *       example="https://example.com/dataset/luchtfoto-2022"
 *     )
 *   ),
 *   @OA\Property(
 *     property="relationships",
 *     readOnly=true,
 *     type="array",
 *
 *     @OA\Items(ref="#/components/schemas/DistributionLink"),
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="DistributionLink",
 *
 *   @OA\Property(
 *     property="data",
 *     type="object",
 *     @OA\Property(
 *       property="type",
 *       readOnly=true,
 *       type="string",
 *       example="distribution"
 *     ),
 *     @OA\Property(
 *       property="id",
 *       readOnly=true,
 *       type="string",
 *       example="12345678-910a-bcde-f123-45678910abcd"
 *     ),
 *   )
 * )
 */
class PostDatasetRequest extends PostResourceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param ListClient    $listClient    The list client to get the value lists
     * @param DataFormatter $dataFormatter The data formatter to format value lists
     *
     * @return array<string, mixed> The validation rules that apply to the request
     */
    public function rules(ListClient $listClient,
                          DataFormatter $dataFormatter): array
    {
        return array_merge($this->resourceRules($listClient, $dataFormatter), [
            // Dataset Specific
            'attributes.dct:accrualPeriodicity' => [
                'nullable',
                $this->ruleFromList('Overheid:Frequency', $listClient, $dataFormatter),
            ],
            'attributes.dct:spatial'                    => ['nullable'],
            'attributes.dct:spatial.locn:geometry'      => ['nullable'],
            'attributes.dct:spatial.dcat:bbox'          => ['nullable'],
            'attributes.dct:spatial.dcat:centroid'      => ['nullable'],
            'attributes.dcat:spatialResolutionInMeters' => ['nullable'],

            'attributes.dct:temporal'                   => ['nullable'],
            'attributes.dct:temporal.dcat:startDate'    => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:temporal.dcat:endDate'      => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:temporal.time:hasBeginning' => ['nullable', 'array'],
            'attributes.dct:temporal.time:hasEnd'       => ['nullable', 'array'],

            'attributes.dcat:temporalResolution' => ['nullable'],
            'attributes.prov:wasGeneratedBy'     => ['nullable'],

            'meta.dataspace_uri'                 => [
                'required',
                $this->ruleFromList('DEXES:Dataspaces', $listClient, $dataFormatter),
            ],
            'meta.consumer_name'                 => ['nullable'],
            'meta.metadata_language'             => ['nullable'],
            'meta.consumer_checksum'             => ['nullable'],
            'meta.name'                          => ['nullable'],
            'meta.dataset_status'                => ['nullable'],
            'meta.high_value'                    => ['nullable'],
            'meta.basis_register'                => ['nullable'],
            'meta.source_catalog'                => ['nullable'],
        ]);
    }
}
