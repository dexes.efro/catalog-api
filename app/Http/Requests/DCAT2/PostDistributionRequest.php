<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests\DCAT2;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;

/**
 * Class PostDistributionRequest.
 *
 * @OA\Response(
 *   response="CreatedDCAT2DistributionResourceResponse",
 *   description="A created DCAT-2 distribution as a JSON:API resource.",
 *
 *   @OA\MediaType(
 *     mediaType="application/vnd.api+json",
 *
 *     @OA\Schema(ref="#/components/schemas/DCAT2DistributionDocument")
 *   ),
 *
 *   @OA\Header(
 *     header="Location",
 *     description="Header identifying the location of the newly created resource.",
 *
 *     @OA\Schema(
 *       type="string",
 *       format="uri"
 *     )
 *   )
 * )
 *
 * @OA\Response(
 *     response="DCAT2DistributionListResponse",
 *     description="A List of DCAT2.0 Distributions as a response.",
 *
 *     @OA\MediaType(
 *       mediaType="application/vnd.api+json",
 *
 *       @OA\Schema(ref="#/components/schemas/DCAT2DistributionListDocument")
 *     )
 * )
 *
 * @OA\Response(
 *     response="DCAT2DistributionResourceResponse",
 *     description="A DCAT2.0 Distribution as a JSON:API resource.",
 *
 *     @OA\MediaType(
 *       mediaType="application/vnd.api+json",
 *
 *       @OA\Schema(ref="#/components/schemas/DCAT2DistributionDocument")
 *     )
 * )
 * @OA\Schema(
 *   schema="DCAT2DistributionListDocument",
 *   description="DCAT 2 Distribution List Document",
 *
 *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
 *   @OA\Property(property="meta", ref="#/components/schemas/SearchResultsMeta"),
 *   @OA\Property(
 *     property="data",
 *     type="array",
 *
 *     @OA\Items(ref="#/components/schemas/DCAT2DistributionResource")
 *   )
 * )
 *
 * @OA\Schema(
 *   schema="DCAT2DistributionDocument",
 *   description="DCAT 2 Distribution Document",
 *
 *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
 *   @OA\Property(property="data", ref="#/components/schemas/DCAT2DistributionResource"),
 * )
 *
 * @OA\Schema(
 *   schema="DCAT2DistributionResource",
 *
 *   @OA\Property(
 *     property="type",
 *     readOnly=true,
 *     type="string",
 *     example="distribution"
 *   ),
 *   @OA\Property(
 *     property="id",
 *     readOnly=true,
 *     type="string",
 *     example="12345678-910a-bcde-f123-45678910abcd"
 *   ),
 *   @OA\Property(
 *     property="attributes",
 *     type="object",
 *     @OA\Property(
 *       property="dct:title",
 *       type="string",
 *       description="A name given to the distribution.",
 *     ),
 *     @OA\Property(
 *       property="dct:description",
 *       type="string",
 *       description="A comprehensive written description of the distribution.",
 *     ),
 *     @OA\Property(
 *       property="dct:issued",
 *       type="string",
 *       example="2022-03-23T08:20:49",
 *       description="Date of formal issuance (e.g., publication) of the distribution.",
 *     ),
 *     @OA\Property(
 *       property="dct:modified",
 *       type="string",
 *       example="2022-03-23T08:20:49",
 *       description="Most recent date on which the distribution was changed, updated or modified.",
 *     ),
 *     @OA\Property(
 *       property="dct:license",
 *       type="string",
 *       description="A legal document under which the resource is made available.",
 *       ref="#/components/schemas/DEXES:License",
 *     ),
 *     @OA\Property(
 *       property="dct:accessRights",
 *       type="string",
 *       ref="#/components/schemas/Overheid:Openbaarheidsniveau",
 *     ),
 *     @OA\Property(
 *       property="dct:rights",
 *       type="string",
 *       description="Information about rights held in and over the distribution.",
 *     ),
 *     @OA\Property(
 *       property="odrl:hasPolicy",
 *       type="array",
 *
 *       @OA\Items(ref="#/components/schemas/DEXES:Policies"),
 *     ),
 *
 *     @OA\Property(
 *       property="dcat:accessURL",
 *       type="string",
 *       description="A URL of the resource that gives access to a distribution of the dataset.",
 *       example="https://api.example.com",
 *     ),
 *     @OA\Property(
 *       property="dcat:accessService",
 *       type="string",
 *       description="A data service that gives access to the distribution of the dataset",
 *     ),
 *     @OA\Property(
 *       property="dcat:downloadURL",
 *       type="string",
 *       example="https://download.example.com",
 *       description="The URL of the downloadable file in a given format.",
 *     ),
 *     @OA\Property(
 *       property="dcat:byteSize",
 *       type="string",
 *       description="The size of a distribution in bytes.",
 *     ),
 *     @OA\Property(
 *       property="dcat:spatialResolutionInMeters",
 *       type="string",
 *       description="The minimum spatial separation resolvable in a dataset distribution, measured in meters.",
 *     ),
 *     @OA\Property(
 *       property="dcat:temporalResolution",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="dcat:conformsTo",
 *       type="string",
 *       description="An established standard to which the distribution conforms.",
 *     ),
 *     @OA\Property(
 *       property="dcat:mediaType",
 *       type="string",
 *       description="The media type of the distribution as defined by IANA",
 *       ref="#/components/schemas/MDR:Filetype",
 *     ),
 *     @OA\Property(
 *       property="dct:format",
 *       type="string",
 *       description="The file format of the distribution",
 *       ref="#/components/schemas/MDR:Filetype",
 *     ),
 *     @OA\Property(
 *       property="dcat:compressFormat",
 *       type="string",
 *       ref="#/components/schemas/MDR:Filetype",
 *       description="The compression format of the distribution in which the data is contained in a compressed form.",
 *     ),
 *     @OA\Property(
 *       property="dcat:packageFormat",
 *       type="string",
 *       ref="#/components/schemas/MDR:Filetype",
 *       description="The package format of the distribution in which one or more data files are grouped together, e.g. to enable a set of related files to be downloaded together.",
 *     ),
 *     required={
 *       "dct:title",
 *       "dct:description",
 *       "dct:license",
 *       "dcat:accessURL",
 *       "dct:format",
 *     }
 *   ),
 *   @OA\Property(
 *     property="links",
 *     readOnly=true,
 *     type="object",
 *     @OA\Property(
 *       property="self",
 *       type="string",
 *       example="https://example.com/dataset/12345678-910a-bcde-f123-45678910abcd#12345678-910a-bcde-f123-45678910abcd"
 *     )
 *   )
 * )
 */
class PostDistributionRequest extends BaseResourceRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ListClient    $listClient    The list client to get the value lists
     * @param DataFormatter $dataFormatter The data formatter to format value lists
     *
     * @return array<string, mixed> The validation rules that apply to the request
     */
    public function rules(ListClient $listClient,
                          DataFormatter $dataFormatter): array
    {
        return [
            // Distribution fields
            'attributes.dct:title'                      => ['required'],
            'attributes.dct:description'                => ['required'],
            'attributes.dct:issued'                     => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:modified'                   => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:license'                    => [
                'required',
                $this->ruleFromList('DEXES:License', $listClient, $dataFormatter),
            ],
            'attributes.dct:accessRights'               => [
                'nullable',
                $this->ruleFromList('Overheid:Openbaarheidsniveau', $listClient, $dataFormatter),
            ],
            'attributes.dct:rights'                     => ['nullable'], // range: dct:RightsStatement
            'attributes.odrl:hasPolicy'                 => [
                'nullable',
                'array',
                $this->ruleFromList('DEXES:Policies', $listClient, $dataFormatter),
            ],
            'attributes.dcat:accessURL'                 => ['required', 'url'],
            'attributes.dcat:accessService'             => ['nullable'], // ??? the identifier of the dataService???
            'attributes.dcat:downloadURL'               => ['nullable', 'url'],
            'attributes.dcat:byteSize'                  => ['nullable'],
            'attributes.dcat:spatialResolutionInMeters' => ['nullable'],
            'attributes.dcat:temporalResolution'        => ['nullable'],
            'attributes.dcat:conformsTo'                => ['nullable'],
            'attributes.dcat:mediaType'                 => [
                'nullable',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
            'attributes.dct:format' => [
                'required',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
            'attributes.dcat:compressFormat' => [
                'nullable',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
            'attributes.dcat:packageFormat' => [
                'nullable',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
        ];
    }
}
