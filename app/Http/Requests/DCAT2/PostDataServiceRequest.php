<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests\DCAT2;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;

/**
 * Class PostDataServiceRequest.
 *
 * @OA\Response(
 *     response="DCAT2DataServiceListResponse",
 *     description="A List of DCAT2.0 DataServices as a response.",
 *
 *     @OA\MediaType(
 *       mediaType="application/json",
 *
 *       @OA\Schema(
 *         schema="DCAT2DataServiceList",
 *         type="array",
 *
 *         @OA\Items(ref="#/components/schemas/DCAT2DataServiceResource")
 *       )
 *     )
 * )
 *
 * @OA\Response(
 *     response="DCAT2DataServiceResourceResponse",
 *     description="A DCAT2.0 DataService as a response.",
 *
 *     @OA\MediaType(
 *      mediaType="application/json",
 *
 *      @OA\Schema(ref="#/components/schemas/DCAT2DataServiceResource"),
 *     )
 * )
 *
 * @OA\Response(
 *   response="CreatedDCAT2DataServiceResourceResponse",
 *   description="A created DCAT-2 data service as a JSON:API resource.",
 *
 *   @OA\MediaType(
 *     mediaType="application/vnd.api+json",
 *
 *     @OA\Schema(ref="#/components/schemas/DCAT2DataServiceDocument")
 *   ),
 *
 *   @OA\Header(
 *     header="Location",
 *     description="Header identifying the location of the newly created resource.",
 *
 *     @OA\Schema(
 *       type="string",
 *       format="uri"
 *     )
 *   )
 * )
 * @OA\Schema(
 *   schema="DCAT2DataServiceDocument",
 *   description="DCAT 2 DataService Document",
 *
 *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
 *   @OA\Property(property="data", ref="#/components/schemas/DCAT2DataServiceResource"),
 * )
 *
 * @OA\Schema(
 *     schema="DCAT2DataServiceResource",
 *     description="DCAT 2 DataService Resource",
 *
 *   @OA\Property(
 *     property="type",
 *     readOnly=true,
 *     type="string",
 *     example="dataservice"
 *   ),
 *   @OA\Property(
 *     property="id",
 *     readOnly=true,
 *     type="string",
 *     example="luchtfoto-2022"
 *   ),
 *   @OA\Property(
 *     property="attributes",
 *     type="object",
 *     allOf={
 *
 *      @OA\Schema(ref="#/components/schemas/DCAT2Resources"),
 *     },
 *
 *     @OA\Property(
 *       property="dcat:endpointURL",
 *       type="string",
 *       description="The root location or primary endpoint of the service (a Web-resolvable IRI).",
 *       example="https://example.com/api/resource",
 *     ),
 *     @OA\Property(
 *       property="dcat:endpointDescription",
 *       type="string",
 *       description="A description of the services available via the end-points",
 *     ),
 *     @OA\Property(
 *       property="dcat:servesDataset",
 *       type="string",
 *       description="A collection of data that this data service can distribute.",
 *     ),
 *   ),
 *   @OA\Property(
 *     property="meta",
 *     type="object",
 *     @OA\Property(
 *       property="dataspace_uri",
 *       type="string",
 *       description="A URI pointing to the Dexes Dataspaces this Data Service belongs to",
 *       ref="#/components/schemas/DEXES:Dataspaces",
 *     ),
 *     @OA\Property(
 *       property="consumer_name",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="consumer_checksum",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="name",
 *       type="string",
 *     ),
 *     required={
 *       "dcat:endpointURL",
 *       "dcat:endpointDescription",
 *       "dataspace_uri"
 *     }
 *   ),
 *   @OA\Property(
 *     property="links",
 *     readOnly=true,
 *     type="object",
 *     @OA\Property(
 *       property="self",
 *       type="string",
 *       example="https://example.com/dataservice/luchtfoto-2022",
 *     )
 *   )
 * )
 */
class PostDataServiceRequest extends PostResourceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param ListClient    $listClient    The list client to get the value lists
     * @param DataFormatter $dataFormatter The data formatter to format value lists
     *
     * @return array<string, mixed> The validation rules that apply to the request
     */
    public function rules(ListClient $listClient,
                          DataFormatter $dataFormatter): array
    {
        return array_merge($this->resourceRules($listClient, $dataFormatter), [
            // DataService Specific
            'attributes.dcat:endpointURL'         => ['required', 'url'],
            'attributes.dcat:endpointDescription' => ['required'],
            'attributes.dcat:servesDataset'       => ['nullable'],

            'meta.dataspace_uri'                 => [
                'required',
                $this->ruleFromList('DEXES:Dataspaces', $listClient, $dataFormatter),
            ],
            'meta.consumer_name'                 => ['nullable'],
            'meta.consumer_checksum'             => ['nullable'],
            'meta.name'                          => ['nullable'],
        ]);
    }
}
