<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests\DCAT2;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;
use XpertSelect\Framework\Http\Requests\LoggableFormRequest;

/**
 * Class BaseResourceRequest.
 */
abstract class BaseResourceRequest extends LoggableFormRequest
{
    /**
     * {@inheritdoc}
     */
    protected bool $withBody = true;

    /**
     * {@inheritdoc}
     *
     * @return array<string, mixed> The data from the request to validate
     */
    public function validationData(): array
    {
        return $this->applyLicenseFallbackIfRequired(parent::validationData());
    }

    /**
     * Checks whether the value is found in the given list.
     *
     * @param string        $list          The name of the list
     * @param ListClient    $listClient    The list client
     * @param DataFormatter $dataFormatter The data formatter
     *
     * @return In The rule
     */
    protected function ruleFromList(string $list, ListClient $listClient,
                                    DataFormatter $dataFormatter): In
    {
        return Rule::in(array_keys($dataFormatter->flatten($listClient->list($list))));
    }

    /**
     * Apply a license fallback value to the incoming request data when no valid license information
     * was supplied for either the dataset or its distributions.
     *
     * @param array<string, mixed> $data The data to apply the license fallback to if required
     *
     * @return array<string, mixed> The, possibly, patched input data
     */
    protected function applyLicenseFallbackIfRequired(array $data): array
    {
        $licenseTaxonomy = $this->getLicenseTaxonomyData();
        $fallback        = config('xs-content-types.dexes-dataset.license_fallback');
        $licenseKey      = 'dct:license';
        $resourceKey     = 'dcat:distribution';

        if (array_key_exists($licenseKey, $data)) {
            if (!array_key_exists($data[$licenseKey], $licenseTaxonomy)) {
                $data[$licenseKey] = $fallback;
            }
        }

        if (!empty($data[$resourceKey])) {
            foreach ($data[$resourceKey] as &$resource) {
                if (array_key_exists($licenseKey, $resource)) {
                    if (!array_key_exists($resource[$licenseKey], $licenseTaxonomy)) {
                        $resource[$licenseKey] = $fallback;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Retrieve the contents of the DEXES:License taxonomy as a {uri} => {label} array.
     *
     * @return array<string, string> The license taxonomy data
     */
    protected function getLicenseTaxonomyData(): array
    {
        /** @var ListClient $listClient */
        $listClient = app(ListClient::class);

        /** @var DataFormatter $formatter */
        $formatter = app(DataFormatter::class);

        return $formatter->flatten($listClient->list('DEXES:License'));
    }
}
