<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests\DCAT2;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;

/**
 * Class PatchDistributionRequest.
 */
class PatchDistributionRequest extends BaseResourceRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ListClient    $listClient    The list client to get the value lists
     * @param DataFormatter $dataFormatter The data formatter to format value lists
     *
     * @return array<string, mixed> The validation rules that apply to the request
     */
    public function rules(ListClient $listClient,
                          DataFormatter $dataFormatter): array
    {
        return [
            'attributes.dct:title'                      => ['required', 'sometimes'],
            'attributes.dct:description'                => ['required', 'sometimes'],
            'attributes.dct:issued'                     => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:modified'                   => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:license'                    => [
                'required', 'sometimes',
                $this->ruleFromList('DEXES:License', $listClient, $dataFormatter),
            ],
            'attributes.dct:accessRights'               => [
                'nullable',
                $this->ruleFromList('Overheid:Openbaarheidsniveau', $listClient, $dataFormatter),
            ],
            'attributes.dct:rights'                     => ['nullable'], // range: dct:RightsStatement
            'attributes.odrl:hasPolicy'                 => [
                'nullable',
                'array',
                $this->ruleFromList('DEXES:Policies', $listClient, $dataFormatter),
            ],
            'attributes.dcat:accessURL'                 => ['required', 'sometimes', 'url'],
            'attributes.dcat:accessService'             => ['nullable'], // ??? the identifier of the dataService???
            'attributes.dcat:downloadURL'               => ['nullable', 'url'],
            'attributes.dcat:byteSize'                  => ['nullable'],
            'attributes.dcat:spatialResolutionInMeters' => ['nullable'],
            'attributes.dcat:temporalResolution'        => ['nullable'],
            'attributes.dcat:conformsTo'                => ['nullable'],
            'attributes.dcat:mediaType'                 => [
                'nullable',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
            'attributes.dct:format' => [
                'required', 'sometimes',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
            'attributes.dcat:compressFormat' => [
                'nullable',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
            'attributes.dcat:packageFormat' => [
                'nullable',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
        ];
    }
}
