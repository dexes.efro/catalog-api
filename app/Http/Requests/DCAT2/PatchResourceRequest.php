<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests\DCAT2;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;

/**
 * Class PatchResourceRequest.
 */
abstract class PatchResourceRequest extends BaseResourceRequest
{
    /**
     * Get the validation rules for the DCAT2.0 Resource class.
     *
     * @return array<string, mixed> The validation rules for the Resource class
     */
    protected function resourceRules(ListClient $listClient,
                                     DataFormatter $dataFormatter): array
    {
        return [
            'attributes.dct:accessRights' => [
                'nullable',
                $this->ruleFromList('Overheid:Openbaarheidsniveau', $listClient, $dataFormatter),
            ],
            'attributes.dct:conformsTo' => ['nullable'],
            // ContactPoint
            'attributes.dcat:contactPoint.vcard:fn'           => ['required', 'sometimes'],
            'attributes.dcat:contactPoint.vcard:hasEmail'     => ['required', 'sometimes', 'email'],
            'attributes.dcat:contactPoint.vcard:hasName'      => ['nullable'],
            'attributes.dcat:contactPoint.vcard:hasAddress'   => ['nullable'],
            'attributes.dcat:contactPoint.vcard:hasURL'       => ['nullable', 'url'],
            'attributes.dcat:contactPoint.vcard:hasTelephone' => ['nullable'],

            'attributes.dct:creator' => [
                'nullable',
                $this->ruleFromList('DEXES:Organization', $listClient, $dataFormatter),
            ],
            'attributes.dct:description' => ['nullable'],
            'attributes.dct:title'       => ['required', 'sometimes'],
            'attributes.dct:issued'      => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:modified'    => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:language'    => [
                'nullable',
                'array',
                $this->ruleFromList('DONL:Language', $listClient, $dataFormatter),
            ],
            'attributes.dct:publisher' => [
                'nullable',
                $this->ruleFromList('DEXES:Organization', $listClient, $dataFormatter),
            ],
            'attributes.dct:identifier' => ['required', 'sometimes'],
            'attributes.dcat:theme'     => [
                'nullable',
                'array',
                $this->ruleFromList('Overheid:TaxonomieBeleidsagenda', $listClient, $dataFormatter),
            ],
            'attributes.dct:type'                  => ['nullable'], // ??? https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
            'attributes.dct:relation'              => ['nullable'],
            'attributes.dcat:qualifiedRelation'    => ['nullable'],
            'attributes.dcat:keyword'              => ['nullable', 'array'],
            'attributes.dcat:landingPage'          => ['nullable', 'url'],
            'attributes.prov:qualifiedAttribution' => ['nullable'],
            'attributes.dct:license'               => [
                'required', 'sometimes',
                $this->ruleFromList('DEXES:License', $listClient, $dataFormatter),
            ],
            'attributes.dct:rights'     => ['nullable'],
            'attributes.odrl:hasPolicy' => [
                'nullable',
                'array',
                $this->ruleFromList('DEXES:Policies', $listClient, $dataFormatter),
            ],
            'attributes.dct:isReferencedBy' => ['nullable', 'array'],
        ];
    }
}
