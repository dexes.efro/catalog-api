<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests\DCAT2;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;

/**
 * Class PatchDatasetRequest.
 */
class PatchDatasetRequest extends PatchResourceRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ListClient    $listClient    The list client to get the value lists
     * @param DataFormatter $dataFormatter The data formatter to format value lists
     *
     * @return array<string, mixed> The validation rules that apply to the request
     */
    public function rules(ListClient $listClient,
                          DataFormatter $dataFormatter): array
    {
        return array_merge($this->resourceRules($listClient, $dataFormatter), [
            // Dataset Specific
            'attributes.dct:accrualPeriodicity' => [
                'nullable',
                $this->ruleFromList('Overheid:Frequency', $listClient, $dataFormatter),
            ],
            'attributes.dct:spatial'                    => ['nullable'],
            'attributes.dct:spatial.locn:geometry'      => ['nullable'],
            'attributes.dct:spatial.dcat:bbox'          => ['nullable'],
            'attributes.dct:spatial.dcat:centroid'      => ['nullable'],
            'attributes.dcat:spatialResolutionInMeters' => ['nullable'],

            'attributes.dct:temporal'                   => ['nullable'],
            'attributes.dct:temporal.dcat:startDate'    => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:temporal.dcat:endDate'      => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'attributes.dct:temporal.time:hasBeginning' => ['nullable', 'array'],
            'attributes.dct:temporal.time:hasEnd'       => ['nullable', 'array'],

            'attributes.dcat:temporalResolution' => ['nullable'],
            'attributes.prov:wasGeneratedBy'     => ['nullable'],

            'meta.dataspace_uri'                 => [
                'required', 'sometimes',
                $this->ruleFromList('DEXES:Dataspaces', $listClient, $dataFormatter),
            ],
            'meta.consumer_name'                 => ['nullable'],
            'meta.metadata_language'             => ['nullable'],
            'meta.consumer_checksum'             => ['nullable'],
            'meta.name'                          => ['required', 'sometimes'],
            'meta.dataset_status'                => ['nullable'],
            'meta.high_value'                    => ['nullable'],
            'meta.basis_register'                => ['nullable'],
            'meta.source_catalog'                => ['nullable'],
        ]);
    }
}
