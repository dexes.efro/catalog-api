<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Models\RuleFramework;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use XpertSelect\Framework\Http\Requests\LoggableFormRequest;

/**
 * Class RuleFrameworkPutRequest.
 */
class RuleFrameworkPutRequest extends LoggableFormRequest
{
    /**
     * {@inheritdoc}
     */
    protected bool $withBody = true;

    /**
     * Get the validation rules that apply for this request.
     *
     * @return array<string, array<int, Unique|string>>
     */
    public function rules(): array
    {
        $id    = $this->route('id');
        $rules = RuleFramework::rules();

        $rules['name'][]  = Rule::unique('rule_frameworks', 'name')->ignore($id);
        $rules['title'][] = Rule::unique('rule_frameworks', 'title')->ignore($id);
        $rules['url'][]   = Rule::unique('rule_frameworks', 'url')->ignore($id);

        return $rules;
    }
}
