<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Models\Dataspace;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use XpertSelect\Framework\Http\Requests\LoggableFormRequest;

/**
 * Class DataspacePutRequest.
 */
class DataspacePutRequest extends LoggableFormRequest
{
    /**
     * {@inheritdoc}
     */
    protected bool $withBody = true;

    /**
     * Get the validation rules that apply for this request.
     *
     * @return array<string, array<int, Unique|string>>
     */
    public function rules(): array
    {
        $id    = $this->route('id');
        $rules = Dataspace::rules();

        $rules['name'][] = Rule::unique('dataspaces', 'name')->ignore($id);
        $rules['uri'][]  = Rule::unique('dataspaces', 'uri')->ignore($id);

        return $rules;
    }
}
