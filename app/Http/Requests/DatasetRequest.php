<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;
use Illuminate\Validation\Rule;

/**
 * Class DatasetRequest.
 *
 * This represents an incoming HTTP dataset request (to store/update a dataset)
 *
 * @OA\Response(
 *     response="DatasetListResponse",
 *     description="A List of Dexes Datasets as a response.",
 *
 *     @OA\MediaType(
 *      mediaType="application/json",
 *
 *      @OA\Schema(schema="datasetlist",type="array",@OA\Items(ref="#/components/schemas/Dataset")),
 *     )
 * ),
 *
 * @OA\Response(
 *     response="DatasetResourceResponse",
 *     description="A Dexes Dataset as a response.",
 *
 *     @OA\MediaType(
 *      mediaType="application/json",
 *
 *      @OA\Schema(ref="#/components/schemas/Dataset"),
 *     )
 * ),
 * @OA\Schema(
 *      schema="Dataset",
 *      description="",
 *
 *      @OA\Property(
 *        property="title",
 *        type="string",
 *      ),
 *      @OA\Property(
 *        property="notes",
 *        type="string",
 *      ),
 *      @OA\Property(
 *        property="url",
 *        type="string",
 *        example="https://example.com",
 *      ),
 *      @OA\Property(
 *        property="theme",
 *        type="array",
 *
 *        @OA\Items(
 *            ref="#/components/schemas/Overheid:TaxonomieBeleidsagenda",
 *        ),
 *      ),
 *
 *      @OA\Property(
 *        property="identifier",
 *        type="string",
 *      ),
 *      @OA\Property(
 *        property="source_catalog",
 *        type="string",
 *        ref="#/components/schemas/DONL:Catalogs",
 *      ),
 *      @OA\Property(
 *        property="authority",
 *        type="string",
 *        ref="#/components/schemas/DEXES:Organization",
 *      ),
 *      @OA\Property(
 *        property="publisher",
 *        type="string",
 *        ref="#/components/schemas/DEXES:Organization",
 *      ),
 *      @OA\Property(
 *        property="contact_point_name",
 *        type="string",
 *      ),
 *      @OA\Property(
 *        property="contact_point_email",
 *        type="string",
 *        example="example@email.com",
 *      ),
 *      @OA\Property(
 *        property="metadata_language",
 *        type="string",
 *        ref="#/components/schemas/DONL:Language",
 *      ),
 *      @OA\Property(
 *        property="language",
 *        type="array",
 *
 *        @OA\Items(ref="#/components/schemas/DONL:Language",),
 *      ),
 *
 *      @OA\Property(
 *        property="license_id",
 *        type="string",
 *        ref="#/components/schemas/DEXES:License",
 *      ),
 *      @OA\Property(
 *        property="access_rights",
 *        type="string",
 *        ref="#/components/schemas/Overheid:Openbaarheidsniveau",
 *      ),
 *      @OA\Property(
 *        property="modified",
 *        type="string",
 *        example="2023-08-17T00:00:00",
 *      ),
 *      @OA\Property(
 *        property="dataspace_uri",
 *        type="string",
 *        ref="#/components/schemas/DEXES:Dataspaces",
 *      ),
 *      @OA\Property(
 *          property="has_policy",
 *          type="array",
 *
 *          @OA\Items(
 *              ref="#/components/schemas/DEXES:Policies",
 *          ),
 *      ),
 *
 *     @OA\Property(
 *          property="resources",
 *          type="array",
 *
 *          @OA\Items(
 *              ref="#/components/schemas/Resource"
 *          )
 *     )
 * ),
 *
 * @OA\Schema(
 *      schema="Resource",
 *      description="",
 *
 *      @OA\Property(
 *          property="url",
 *          type="string",
 *          example="https://example.com",
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *      ),
 *      @OA\Property(
 *          property="description",
 *          type="string",
 *      ),
 *      @OA\Property(
 *          property="metadata_language",
 *          type="string",
 *          ref="#/components/schemas/DONL:Language",
 *      ),
 *      @OA\Property(
 *          property="language",
 *          type="array",
 *
 *          @OA\Items(
 *              ref="#/components/schemas/DONL:Language",
 *          )
 *      ),
 *
 *      @OA\Property(
 *          property="license_id",
 *          type="string",
 *          ref="#/components/schemas/DEXES:License",
 *      ),
 *      @OA\Property(
 *          property="format",
 *          type="string",
 *          ref="#/components/schemas/MDR:Filetype",
 *      ),
 *      @OA\Property(
 *          property="has_policy",
 *          type="array",
 *
 *          @OA\Items(
 *              ref="#/components/schemas/DEXES:Policies",
 *          )
 *      ),
 *  )
 */
class DatasetRequest extends DCATRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param ListClient    $listClient    The list client to get the value lists
     * @param DataFormatter $dataFormatter The data formatter to format value lists
     *
     * @return array<string, mixed> The validation rules that apply to the request
     */
    public function rules(ListClient $listClient,
                          DataFormatter $dataFormatter): array
    {
        return [
            'name'                          => ['nullable'],
            // Dataset fields
            'title'                         => ['required', 'max:200'],
            'notes'                         => ['required'],
            'url'                           => ['nullable', 'url'],
            'theme'                         => [
                'required',
                'array',
                $this->ruleFromList('Overheid:TaxonomieBeleidsagenda', $listClient, $dataFormatter),
            ],
            'identifier'                    => ['required'],
            'alternate_identifier'          => ['nullable', 'array'],
            'source_catalog'                => [
                'nullable',
                $this->ruleFromList('DEXES:Catalogs', $listClient, $dataFormatter),
            ],
            'authority'                     => [
                'required',
                $this->ruleFromList('DEXES:Organization', $listClient, $dataFormatter),
            ],
            'publisher'                     => [
                'required',
                $this->ruleFromList('DEXES:Organization', $listClient, $dataFormatter),
            ],
            'keyword'                       => ['nullable', 'array'],
            'contact_point_name'            => ['required'],
            'contact_point_title'           => ['nullable'],
            'contact_point_address'         => ['nullable'],
            'contact_point_email'           => [
                'required_without_all:contact_point_website,contact_point_phone',
                'email',
            ],
            'contact_point_website'         => [
                'required_without_all:contact_point_email,contact_point_phone',
            ],
            'contact_point_phone'           => [
                'required_without_all:contact_point_email,contact_point_website',
            ],
            'metadata_language'             => [
                'required',
                $this->ruleFromList('DONL:Language', $listClient, $dataFormatter),
            ],
            'language'                      => [
                'required',
                'array',
                $this->ruleFromList('DONL:Language', $listClient, $dataFormatter),
            ],
            'license_id'                    => [
                'required',
                $this->ruleFromList('DEXES:License', $listClient, $dataFormatter),
            ],
            'access_rights'                 => [
                'nullable',
                $this->ruleFromList('Overheid:Openbaarheidsniveau', $listClient, $dataFormatter),
            ],
            'access_rights_reason'          => [
                'nullable',
                $this->ruleFromList('DONL:WOBUitzondering', $listClient, $dataFormatter),
            ],
            'temporal_label'                => ['nullable'],
            'temporal_start'                => ['nullable'],
            'temporal_end'                  => ['nullable'],
            'spatial_scheme'                => ['nullable', 'array'],
            'spatial_value'                 => ['nullable', 'array'],
            'legal_foundation_label'        => ['nullable'],
            'legal_foundation_ref'          => ['nullable'],
            'legal_foundation_uri'          => ['nullable', 'url'],
            'dataset_status'                => [
                'nullable',
                $this->ruleFromList('Overheid:DatasetStatus', $listClient, $dataFormatter),
            ],
            'date_planned'                  => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'issued'                        => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'modified'                      => ['required', 'date_format:Y-m-d\TH:i:s'],
            'frequency'                     => [
                'nullable',
                $this->ruleFromList('Overheid:Frequency', $listClient, $dataFormatter),
            ],
            'documentation'                 => ['nullable', 'array'],
            'documentation.*'               => ['url'],
            'sample'                        => ['nullable', 'array'],
            'sample.*'                      => ['url'],
            'provenance'                    => ['nullable', 'array'],
            'provenance.*'                  => ['url'],
            'version'                       => ['nullable'],
            'version_notes'                 => ['nullable', 'array'],
            'is_version_of'                 => ['nullable', 'array'],
            'has_version'                   => ['nullable', 'array'],
            'has_policy'                    => [
                'nullable',
                'array',
                $this->ruleFromList('DEXES:Policies', $listClient, $dataFormatter),
            ],
            'source'                        => ['nullable', 'array'],
            'related_resource'              => ['nullable', 'array'],
            'conforms_to'                   => ['nullable', 'array'],
            'high_value'                    => ['nullable'],
            'referentie_data'               => ['nullable'],
            'basis_register'                => ['nullable'],
            'national_coverage'             => ['nullable'],
            'sector_registrations'          => ['nullable'],
            'local_registrations'           => ['nullable'],
            'linked_concepts'               => ['nullable', 'array'],
            'changetype'                    => [
                'nullable',
                $this->ruleFromList('ADMS:Changetype', $listClient, $dataFormatter),
            ],
            'bytesize'                      => ['nullable'],
            'resource_type'                 => [
                'nullable',
                Rule::in(array_keys($dataFormatter->flatten($listClient->list('DONL:DistributionType')))),
            ],
            'tags'                          => ['nullable', 'array'],
            'dataspace_uri'                 => [
                'required',
                $this->ruleFromList('DEXES:Dataspaces', $listClient, $dataFormatter),
            ],
            'consumer_name'                 => ['nullable'],
            'consumer_checksum'             => ['nullable'],
            // Resource fields
            'resources'                     => ['present', 'array'],
            'resources.*.url'               => ['required', 'url'],
            'resources.*.download_url'      => ['nullable', 'url'],
            'resources.*.documentation'     => ['nullable', 'array'],
            'resources.*.documentation.*'   => ['url'],
            'resources.*.linked_schemas'    => ['nullable', 'array'],
            'resources.*.name'              => ['required'],
            'resources.*.description'       => ['required'],
            'resources.*.status'            => [
                'nullable',
                $this->ruleFromList('ADMS:DistributieStatus', $listClient, $dataFormatter),
            ],
            'resources.*.metadata_language' => [
                'required',
                $this->ruleFromList('DONL:Language', $listClient, $dataFormatter),
            ],
            'resources.*.language'          => [
                'required',
                'array',
                $this->ruleFromList('DONL:Language', $listClient, $dataFormatter),
            ],
            'resources.*.license_id'        => [
                'required',
                $this->ruleFromList('DEXES:License', $listClient, $dataFormatter),
            ],
            'resources.*.rights'            => ['nullable'],
            'resources.*.format'            => [
                'required',
                $this->ruleFromList('MDR:Filetype', $listClient, $dataFormatter),
            ],
            'resources.*.media_type'        => [
                'nullable',
                $this->ruleFromList('IANA:MediaTypes', $listClient, $dataFormatter),
            ],
            'resources.*.size'              => ['nullable', 'integer', 'numeric'],
            'resources.*.hash'              => ['nullable'],
            'resources.*.hash_algorithm'    => ['nullable'],
            'resources.*.release_date'      => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'resources.*.modification_date' => ['nullable', 'date_format:Y-m-d\TH:i:s'],
            'resources.*.distribution_type' => [
                'nullable',
                $this->ruleFromList('DONL:DistributionType', $listClient, $dataFormatter),
            ],
            'resources.*.has_policy' => [
                'nullable',
                'array',
                $this->ruleFromList('DEXES:Policies', $listClient, $dataFormatter),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array<string, mixed> The data from the request to validate
     */
    public function validationData(): array
    {
        return $this->applyLicenseFallbackIfRequired(parent::validationData());
    }

    /**
     * Apply a license fallback value to the incoming request data when no valid license information
     * was supplied for either the dataset or its distributions.
     *
     * @param array<string, mixed> $data The data to apply the license fallback to if required
     *
     * @return array<string, mixed> The, possibly, patched input data
     */
    private function applyLicenseFallbackIfRequired(array $data): array
    {
        $licenseTaxonomy = $this->getLicenseTaxonomyData();
        $fallback        = config('xs-content-types.dexes-dataset.license_fallback');
        $licenseKey      = 'license_id';
        $resourceKey     = 'resources';

        if (array_key_exists($licenseKey, $data)) {
            if (!array_key_exists($data[$licenseKey], $licenseTaxonomy)) {
                $data[$licenseKey] = $fallback;
            }
        }

        if (!empty($data[$resourceKey])) {
            foreach ($data[$resourceKey] as &$resource) {
                if (array_key_exists($licenseKey, $resource)) {
                    if (!array_key_exists($resource[$licenseKey], $licenseTaxonomy)) {
                        $resource[$licenseKey] = $fallback;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Retrieve the contents of the DONL:License taxonomy as a {uri} => {label} array.
     *
     * @return array<string, string> The license taxonomy data
     */
    private function getLicenseTaxonomyData(): array
    {
        /** @var ListClient $listClient */
        $listClient = app(ListClient::class);

        /** @var DataFormatter $formatter */
        $formatter = app(DataFormatter::class);

        return $formatter->flatten($listClient->list('DEXES:License'));
    }
}
