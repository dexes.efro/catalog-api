<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;
use XpertSelect\Framework\Http\Requests\LoggableFormRequest;

/**
 * Class DCATRequest.
 */
abstract class DCATRequest extends LoggableFormRequest
{
    /**
     * {@inheritdoc}
     */
    protected bool $withBody = true;

    protected function ruleFromList(string $list, ListClient $listClient,
                                  DataFormatter $dataFormatter): In
    {
        return Rule::in(array_keys($dataFormatter->flatten($listClient->list($list))));
    }
}
