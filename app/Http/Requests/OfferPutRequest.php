<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Models\Offer;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class OfferPutRequest.
 *
 * This represents an incoming HTTP Offer request (to update an offer).
 */
class OfferPutRequest extends FormRequest
{
    /**
     * Get the validation rules that apply for this request.
     *
     * @return array<string, array<int, string>>
     */
    public function rules(): array
    {
        return Offer::rules();
    }
}
