<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Requests;

use App\Models\Dataspace;
use XpertSelect\Framework\Http\Requests\LoggableFormRequest;

class DataspacePostRequest extends LoggableFormRequest
{
    /**
     * {@inheritdoc}
     */
    protected bool $withBody = true;

    /**
     * Get the validation rules that apply for this request.
     *
     * @return array<string, array<int, string>>
     */
    public function rules(): array
    {
        $rules            = Dataspace::rules();
        $rules['name'][]  = 'unique:App\Models\Dataspace,name';
        $rules['uri'][]   = 'unique:App\Models\Dataspace,uri';
        $rules['asset'][] = 'required';
        $rules['logo'][]  = 'required';

        return $rules;
    }
}
