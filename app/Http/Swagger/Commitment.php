<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Swagger;

/**
 * Class Commitment.
 *
 * @OA\Schema (
 *     title="Commitment",
 *     description="commitment",
 * )
 */
class Commitment
{
    /**
     * @OA\Property (
     *     title="type",
     *     description="type",
     *     default="ExplicitDisclaimerCondition"
     * )
     */
    private string $type;

    /**
     * @OA\Property (
     *     title="id",
     *     description="id",
     *     default="6",
     * )
     */
    private string $id;

    /**
     * @OA\Property (
     *     title="text",
     *     description="text",
     *     default="Voer uw naam in om de voorwaarden te accepteren"
     * )
     */
    private string $text;

    /**
     * @OA\Property (
     *     title="createdAt",
     *     description="Time the object is created",
     *     default="2021-07-07T14:31:57.345814953Z"
     * )
     */
    private string $createdAt;
}
