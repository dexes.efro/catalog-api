<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Swagger;

/**
 * @OA\Schema(
 *   schema="DCAT2Resources",
 *   description="DCAT 2 Resource",
 *
 *   @OA\Property(
 *     property="dct:accessRights",
 *     type="string",
 *     ref="#/components/schemas/Overheid:Openbaarheidsniveau",
 *     description="Information about who can access the resource or an indication of its security status.",
 *   ),
 *   @OA\Property(
 *     property="dct:conformsTo",
 *     type="string",
 *   ),
 *   @OA\Property(
 *     property="dcat:contactPoint",
 *     type="object",
 *     @OA\Property(
 *       property="vcard:fn",
 *       type="string",
 *       description="The full name of the object"
 *     ),
 *     @OA\Property(
 *       property="vcard:hasEmail",
 *       type="string",
 *       format="email",
 *       example="example@mail.com",
 *     ),
 *     @OA\Property(
 *       property="vcard:hasName",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="vcard:hasAddress",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="vcard:hasURL",
 *       type="string",
 *       example="https://example.com",
 *     ),
 *     @OA\Property(
 *       property="vcard:hasTelephone",
 *       type="string",
 *       example="06-12345678",
 *     ),
 *     required={"vcard:fn"}
 *   ),
 *   @OA\Property(
 *     property="dct:creator",
 *     type="string",
 *     ref="#/components/schemas/DEXES:Organization",
 *   ),
 *   @OA\Property(
 *     property="dct:description",
 *     type="string",
 *   ),
 *   @OA\Property(
 *     property="dct:title",
 *     type="string",
 *     description="A name given to the item",
 *   ),
 *   @OA\Property(
 *     property="dct:issued",
 *     type="string",
 *     example="2023-03-23T08:20:49",
 *     description="Date of formal issuance (e.g., publication) of the item.",
 *   ),
 *   @OA\Property(
 *     property="dct:modified",
 *     type="string",
 *     example="2023-03-23T08:20:49",
 *     description="Most recent date on which the item was changed, updated or modified.",
 *   ),
 *   @OA\Property(
 *     property="dct:language",
 *     type="array",
 *
 *     @OA\Items(ref="#/components/schemas/DONL:Language"),
 *   ),
 *
 *   @OA\Property(
 *     property="dct:publisher",
 *     type="string",
 *     ref="#/components/schemas/DEXES:Organization",
 *     description="The entity responsible for making the item available.",
 *   ),
 *   @OA\Property(
 *     property="dct:identifier",
 *     type="string",
 *     description="A unique identifier of the item.",
 *   ),
 *   @OA\Property(
 *     property="dcat:theme",
 *     type="array",
 *
 *     @OA\Items(ref="#/components/schemas/Overheid:TaxonomieBeleidsagenda"),
 *     description="A main category of the resource. A resource can have multiple themes.",
 *   ),
 *
 *   @OA\Property(
 *     property="dct:type",
 *     type="string",
 *     description="The nature or genre of the resource.",
 *   ),
 *   @OA\Property(
 *     property="dct:relation",
 *     type="string",
 *     description="A resource with an unspecified relationship to the cataloged item.",
 *   ),
 *   @OA\Property(
 *     property="dcat:qualifiedRelation",
 *     type="string",
 *     description="Link to a description of a relationship with another resource",
 *   ),
 *   @OA\Property(
 *     property="dcat:keyword",
 *     type="array",
 *
 *     @OA\Items(type="string"),
 *     description="One or more keywords or tags describing the resource."
 *   ),
 *
 *   @OA\Property(
 *     property="dcat:landingPage",
 *     type="string",
 *     description="A Web page that can be navigated to in a Web browser to gain access to the catalog, a dataset, its distributions and/or additional information.",
 *     example="https://example.com/home",
 *   ),
 *   @OA\Property(
 *     property="prov:qualifiedAttribution",
 *     type="string",
 *     description="Link to an Agent having some form of responsibility for the resource",
 *   ),
 *   @OA\Property(
 *     property="dct:license",
 *     type="string",
 *     description="A legal document under which the resource is made available.",
 *     ref="#/components/schemas/DEXES:License",
 *   ),
 *   @OA\Property(
 *     property="dct:rights",
 *     type="string",
 *     description="A statement that concerns all rights not addressed with dct:license or dct:accessRights, such as copyright statements.",
 *   ),
 *   @OA\Property(
 *     property="odrl:hasPolicy",
 *     type="array",
 *
 *     @OA\Items(ref="#/components/schemas/DEXES:Policies"),
 *     description="A policy expressing the rights associated with the resource.",
 *   ),
 *
 *   @OA\Property(
 *     property="dct:isReferencedBy",
 *     type="array",
 *
 *     @OA\Items(type="string"),
 *     description="A related resource, such as a publication, that references, cites, or otherwise points to the cataloged resource.",
 *   ),
 *   required={
 *     "dct:title",
 *     "dct:identifier",
 *     "dcat:theme",
 *     "dct:license",
 *   }
 * )
 */
class Resource
{
}
