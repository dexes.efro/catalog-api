<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Swagger;

/**
 * Class OfferURL.
 *
 * @OA\Schema(
 *     title="Offer URL",
 *     description="Model containing an unique identifier and an URL pointing to an Offer",
 * )
 */
class OfferURL
{
    /**
     * @OA\Property (
     *     title="id",
     *     description="id",
     *     default="1",
     * )
     */
    private string $id;

    /**
     * @OA\Property (
     *     title="url",
     *     description="url pointing to an Offer",
     *     default="https://broker.dexpods.eu/offers/1",
     * )
     */
    private string $url;

    /**
     * @OA\Property (
     *     title="url",
     *     description="full json",
     *     default="",
     * )
     */
    private string $json;
}
