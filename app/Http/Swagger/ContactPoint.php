<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Swagger;

/**
 * Class ContactPoint.
 *
 * @OA\Schema(
 *     title="ContactPoint",
 *     description="Contact information about the resource",
 * )
 */
class ContactPoint
{
    /**
     * @OA\Property(
     *     title="name",
     *     default="Bob"
     * )
     */
    private string $name;

    /**
     * @OA\Property(
     *     title="Email",
     *     default="test@email.com",
     * )
     */
    private string $email;
}
