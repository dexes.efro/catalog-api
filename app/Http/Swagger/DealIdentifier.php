<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Swagger;

/**
 * Class DealIdentifier.
 *
 * @OA\Schema(
 *     title="Deal Identifier",
 *     description="Model containing the unique identifiers of a Deal",
 * )
 */
class DealIdentifier
{
    /**
     * @OA\Property (
     *     title="id",
     *     description="id",
     *     default="1",
     * )
     */
    private string $id;

    /**
     * @OA\Property (
     *     title="url",
     *     description="URL Pointing to a Deal",
     *     default="https://broker.dexpods.eu/deals/10",
     * )
     */
    private string $url;

    /**
     * @OA\Property (
     *     title="url",
     *     description="full json",
     *     default="",
     * )
     */
    private string $json;
}
