<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Swagger;

/**
 * Class Offer.
 *
 * @OA\Schema (
 *     title="Offer",
 *     description="Offer",
 * )
 */
class Offer
{
    /**
     * @OA\Property (
     *     title="id",
     *     description="id",
     *     default="5",
     * )
     */
    private string $id;

    /**
     * @OA\Property (
     *     title="url",
     *     description="url",
     *     default="https://broker.dexpods.eu/offers/5",
     * )
     */
    private string $url;

    /**
     * @OA\Property (
     *     title="Data Owner",
     *     description="URI pointing to the data owner of the Offer",
     *     default="https://joep.dexpods.eu/",
     * )
     */
    private string $dataOwner;

    /**
     * @OA\Property (
     *     title="Resource",
     *     description="URI pointing to the resource of the Offer",
     *     default="https://joep.dexpods.eu/media_objects/34",
     * )
     */
    private string $resource;

    /**
     * @OA\Property (
     *     title="Permitted",
     *     description="permitted",
     *     default="",
     * )
     */
    private string $permitted;

    /**
     * @OA\Property (
     *     title="Scope",
     *     description="scope",
     *     default="",
     * )
     */
    private string $scope;

    /**
     * @OA\Property (
     *     title="Recipient",
     *     description="URI pointing to the recipient of the Offer",
     *     default="https://thom.dexpods.eu",
     * )
     */
    private string $recipient;

    /**
     * @OA\Property (
     *     title="Created At",
     *     description="created at",
     *     default="2021-07-07T14:31:57.345814953Z",
     * )
     */
    private string $createdAt;

    /**
     * @OA\Property(
     *     title="Conditions",
     *     description="Conditions",
     * )
     *
     * @var Condition[]
     */
    private array $conditions;
}
