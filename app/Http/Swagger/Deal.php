<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Swagger;

/**
 * Class Deal.
 *
 * @OA\Schema (
 *     title="Deal",
 *     description="Deal",
 * )
 */
class Deal
{
    /**
     * @OA\Property (
     *     title="id",
     *     description="id",
     *     default="10",
     * )
     */
    private string $id;

    /**
     * @OA\Property (
     *     title="url",
     *     description="url",
     *     default="https://broker.dexpods.eu/deals/10",
     * )
     */
    private string $url;

    /**
     * @OA\Property (
     *     title="offer",
     *     description="Unique identifier of an Offer",
     *     default="6",
     * )
     */
    private string $offer;

    /**
     * @OA\Property (
     *     title="dataOwner",
     *     description="dataOwner",
     *     default="https://willem.dexpods.eu",
     * )
     */
    private string $dataOwner;

    /**
     * @OA\Property (
     *     title="resource",
     *     description="resource",
     *     default="https://willem.dexpods.eu/media_objects/34",
     * )
     */
    private string $resource;

    /**
     * @OA\Property (
     *     title="recipient",
     *     description="recipient",
     *     default="https://stefan.dexpods.eu",
     * )
     */
    private string $recipient;

    /**
     * @OA\Property (
     *     title="createdAt",
     *     description="createdAt",
     *     default="2021-06-17T15:59:17.280227456Z",
     * )
     */
    private string $createdAt;

    /**
     * @OA\Property(
     *     title="Commitments",
     *     description="Commitments",
     * )
     *
     * @var Commitment[]
     */
    private array $commitments;
}
