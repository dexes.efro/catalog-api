<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API;

use App\Http\Requests\DealPostRequest;
use App\Http\Requests\DealPutRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Http\Controllers\BaseContentTypeController;
use XpertSelect\Framework\Services\Search\SearchServiceInterface;

/**
 * Class DealController.
 *
 * Controller that is responsible for deal CRUD functionality.
 */
class DealController extends BaseContentTypeController
{
    /**
     * {@inheritdoc}
     */
    public function __construct(RepositoryInterface $repository, LoggerInterface $logger, ?SearchServiceInterface $searchService = null)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        parent::__construct($repository, $logger, $searchService);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeIdentifierField(): string
    {
        return 'id';
    }

    /**
     * {@inheritdoc}
     */
    public function createContentTypeAttributesFromRequestBody(array $requestBody): array
    {
        return [
            'id'   => $requestBody['id'],
            'url'  => $requestBody['url'],
            'json' => json_encode($requestBody) ?: '',
        ];
    }

    /**
     * @OA\Get(
     *     path="/api/dexes-deals",
     *     tags={"Dexes Deals"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/StartParameter"),
     *     @OA\Parameter(ref="#/components/parameters/RowsParameter"),
     *     @OA\Parameter(ref="#/components/parameters/IdentifierOnlyParameter"),
     *
     *     @OA\Response(response="200", description="Returns all deals",
     *
     *        @OA\JsonContent(
     *          type="array",
     *
     *          @OA\Items(ref="#/components/schemas/DealIdentifier")
     *         ),
     *     ),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function index(Request $request): JsonResponse
    {
        return $this->listRecords($request);
    }

    /**
     * @OA\Get(
     *     path="/api/dexes-deals/{id}",
     *     tags={"Dexes Deals"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DealParameter"),
     *
     *     @OA\Response(response="200", description="",
     *
     *         @OA\JsonContent(ref="#/components/schemas/DealIdentifier")
     *     ),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function show(string $id): JsonResponse
    {
        return $this->showRecord($id);
    }

    /**
     * @OA\Post(
     *     path="/api/dexes-deals",
     *     tags={"Dexes Deals"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\RequestBody (
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/Deal"),
     *     ),
     *
     *     @OA\Response(response="201", description="Deal created",
     *
     *          @OA\JsonContent(ref="#/components/schemas/DealIdentifier")),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function store(DealPostRequest $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @OA\Put(
     *     path="/api/dexes-deals/{id}",
     *     tags={"Dexes Deals"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DealParameter"),
     *
     *     @OA\RequestBody (
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/Deal"),
     *     ),
     *
     *     @OA\Response(response="200", description="Deal updated",
     *
     *          @OA\JsonContent(ref="#/components/schemas/DealIdentifier")),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function update(DealPutRequest $request, string $id): JsonResponse
    {
        return $this->updateRecord($request, $id);
    }

    /**
     * @OA\Delete(
     *     path="/api/dexes-deals/{id}",
     *     tags={"Dexes Deals"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DealParameter"),
     *
     *     @OA\Response(response=204, ref="#/components/responses/204"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function destroy(string $id): JsonResponse
    {
        return $this->deleteRecord($id);
    }
}
