<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API;

use App\Services\Lists\ListDataService;
use Illuminate\Http\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * Class ListsController.
 *
 * Controller that exposes new lists
 */
final class ListsController
{
    /**
     * ListController Constructor.
     *
     * @param ListDataService $listDataService The list data service
     * @param LoggerInterface $logger          The logging implementation to use
     */
    public function __construct(private readonly ListDataService $listDataService,
                                private readonly LoggerInterface $logger)
    {
    }

    /**
     * Get the expanded organization list.
     *
     * @return JsonResponse The HTTP Response
     */
    public function organizationsList(): JsonResponse
    {
        return new JsonResponse($this->listDataService->organizationsList());
    }

    /**
     * Get the expanded license list.
     *
     * @return JsonResponse The HTTP Response
     */
    public function licensesList(): JsonResponse
    {
        return new JsonResponse($this->listDataService->licensesList());
    }

    /**
     * Get the expanded policy list.
     *
     * @return JsonResponse The HTTP Response
     */
    public function policiesList(): JsonResponse
    {
        return new JsonResponse($this->listDataService->policiesList());
    }

    /**
     * Get a list of all RuleFrameworks.
     *
     * @return JsonResponse The HTTP Response
     */
    public function ruleFrameworksList(): JsonResponse
    {
        try {
            return new JsonResponse($this->listDataService->ruleFrameworksList());
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type RuleFramework storage unavailable for index request; %s',
                $e->getMessage()
            ));

            return new JsonResponse(status: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get a list of all dataspaces.
     *
     * @return JsonResponse The HTTP Response
     */
    public function dataspacesList(): JsonResponse
    {
        try {
            return new JsonResponse($this->listDataService->dataspacesList());
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type Dataspace storage unavailable for index request; %s',
                $e->getMessage()
            ));

            return new JsonResponse(status: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get a list of all Catalogs, including all dataspaces.
     *
     * @return JsonResponse The HTTP response
     */
    public function catalogsList(): JsonResponse
    {
        return new JsonResponse($this->listDataService->catalogsList());
    }
}
