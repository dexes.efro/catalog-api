<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API;

use App\Http\Requests\OfferPostRequest;
use App\Http\Requests\OfferPutRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Http\Controllers\BaseContentTypeController;
use XpertSelect\Framework\Services\Search\SearchServiceInterface;

/**
 * Class OfferController.
 *
 * Controller that is responsible for offer CRUD functionality.
 */
class OfferController extends BaseContentTypeController
{
    /**
     * {@inheritdoc}
     */
    public function __construct(RepositoryInterface $repository, LoggerInterface $logger, ?SearchServiceInterface $searchService = null)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        parent::__construct($repository, $logger, $searchService);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeIdentifierField(): string
    {
        return 'id';
    }

    /**
     * {@inheritdoc}
     */
    public function createContentTypeAttributesFromRequestBody(array $requestBody): array
    {
        return [
            'id'   => $requestBody['id'],
            'url'  => $requestBody['url'],
            'json' => json_encode($requestBody) ?: '',
        ];
    }

    /**
     * @OA\Get(
     *     path="/api/dexes-offers",
     *     tags={"Dexes Offers"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/StartParameter"),
     *     @OA\Parameter(ref="#/components/parameters/RowsParameter"),
     *     @OA\Parameter(ref="#/components/parameters/IdentifierOnlyParameter"),
     *
     *     @OA\Response(response="200", description="Returns all offers",
     *
     *        @OA\JsonContent(
     *          type="array",
     *
     *          @OA\Items(ref="#/components/schemas/OfferURL")
     *         ),
     *     ),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function index(Request $request): JsonResponse
    {
        return $this->listRecords($request);
    }

    /**
     * @OA\Get(
     *     path="/api/dexes-offers/{id}",
     *     tags={"Dexes Offers"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/OfferParameter"),
     *
     *     @OA\Response(response="200", description="",
     *
     *         @OA\JsonContent(ref="#/components/schemas/OfferURL")
     *     ),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function show(string $id): JsonResponse
    {
        return $this->showRecord($id);
    }

    /**
     * @OA\Post(
     *     path="/api/dexes-offers",
     *     tags={"Dexes Offers"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\RequestBody (
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/Offer"),
     *     ),
     *
     *     @OA\Response(response="201", description="Offer created",
     *
     *          @OA\JsonContent(ref="#/components/schemas/OfferURL")),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function store(OfferPostRequest $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @OA\Put(
     *     path="/api/dexes-offers/{id}",
     *     tags={"Dexes Offers"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/OfferParameter"),
     *
     *     @OA\RequestBody (
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/Offer"),
     *     ),
     *
     *     @OA\Response(response="200", description="Offer updated",
     *
     *          @OA\JsonContent(ref="#/components/schemas/OfferURL")),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function update(OfferPutRequest $request, string $id): JsonResponse
    {
        return $this->updateRecord($request, $id);
    }

    /**
     * @OA\Delete(
     *     path="/api/dexes-offers/{id}",
     *     tags={"Dexes Offers"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/OfferParameter"),
     *
     *     @OA\Response(response=204, ref="#/components/responses/204"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function destroy(string $id): JsonResponse
    {
        return $this->deleteRecord($id);
    }
}
