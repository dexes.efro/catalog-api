<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API;

use App\Http\Requests\DataspacePostRequest;
use App\Http\Requests\DataspacePutRequest;
use App\Services\HasFiles;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Psr\Log\LoggerInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Http\Controllers\BaseContentTypeController;
use XpertSelect\Framework\Services\Search\SearchServiceInterface;

/**
 * Class DataspaceController.
 */
class DataspaceController extends BaseContentTypeController
{
    use HasFiles;

    /**
     * {@inheritdoc}
     */
    public function __construct(RepositoryInterface $repository, LoggerInterface $logger, ?SearchServiceInterface $searchService = null)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        parent::__construct($repository, $logger, $searchService);
    }

    /**
     * @OA\Get(
     *     path="/api/dexes-dataspaces",
     *     tags={"Dexes Dataspaces"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Parameter(ref="#/components/parameters/StartParameter"),
     *     @OA\Parameter(ref="#/components/parameters/RowsParameter"),
     *     @OA\Parameter(ref="#/components/parameters/IdentifierOnlyParameter"),
     *
     *     @OA\Response(response="200", description="Returns all Dataspaces",
     *
     *        @OA\JsonContent(
     *          type="array",
     *
     *          @OA\Items(ref="#/components/schemas/Dataspace")
     *         ),
     *     ),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function index(Request $request): JsonResponse
    {
        return $this->listRecords($request);
    }

    /**
     * @OA\Get(
     *     path="/api/dexes-dataspaces/{id}",
     *     tags={"Dexes Dataspaces"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Parameter(ref="#/components/parameters/DataspaceParameter"),
     *
     *     @OA\Response(response="200", description="",
     *
     *         @OA\JsonContent(ref="#/components/schemas/Dataspace")
     *     ),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function show(string $id): JsonResponse
    {
        return $this->showRecord($id);
    }

    /**
     * @OA\Post(
     *     path="/api/dexes-dataspaces",
     *     tags={"Dexes Dataspaces"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\RequestBody (
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/Dataspace"),
     *     ),
     *
     *     @OA\Response(response="201", description="Dataspace created",
     *
     *          @OA\JsonContent(ref="#/components/schemas/Dataspace")),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function store(DataspacePostRequest $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @OA\Put(
     *     path="/api/dexes-dataspaces/{id}",
     *     tags={"Dexes Dataspaces"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Parameter(ref="#/components/parameters/DataspaceParameter"),
     *
     *     @OA\RequestBody (
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/Dataspace"),
     *     ),
     *
     *     @OA\Response(response="200", description="Dataspace updated",
     *
     *          @OA\JsonContent(ref="#/components/schemas/Dataspace")),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function update(DataspacePutRequest $request, string $id): JsonResponse
    {
        return $this->updateRecord($request, $id);
    }

    /**
     * @OA\Delete(
     *     path="/api/dexes-dataspaces/{id}",
     *     tags={"Dexes Dataspaces"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Parameter(ref="#/components/parameters/DataspaceParameter"),
     *
     *     @OA\Response(response=204, ref="#/components/responses/204"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function destroy(string $id): JsonResponse
    {
        return $this->deleteRecord($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeIdentifierField(): string
    {
        return 'id';
    }

    /**
     * {@inheritdoc}
     */
    public function createContentTypeAttributesFromRequestBody(array $requestBody): array
    {
        $dataspace = [
            'title'            => $requestBody['title'],
            'intro_text'       => $requestBody['intro_text'] ?? '',
            'ruleframework_id' => $requestBody['ruleframework_id'],
            'uri'              => $requestBody['uri'],
            'join_url'         => $requestBody['join_url'],
        ];

        $dataspace['name'] = array_key_exists('name', $requestBody) ?
            $requestBody['name'] : Str::slug($requestBody['title']);

        if (array_key_exists('logo', $requestBody)) {
            $file = $this->uploadFile($requestBody, $this->getContentTypeName(), 'logo');
            if (null === $file) {
                return $dataspace;
            }
            $dataspace['logo_url'] = $file;
        }

        if (array_key_exists('asset', $requestBody)) {
            $file = $this->uploadFile($requestBody, $this->getContentTypeName(), 'asset');
            if (null === $file) {
                return $dataspace;
            }
            $dataspace['asset_url'] = $file;
        }

        return $dataspace;
    }

    /**
     * The name of the content type, this is used to determine where to save the uploaded files.
     *
     * @return string The name of the content type
     */
    private function getContentTypeName(): string
    {
        return 'content-type-dexes-dataspace';
    }
}
