<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StatusController.
 *
 * Implements API endpoints describing the status of this application.
 *
 * @OA\Tag(
 *   name="Core",
 *   description="Endpoints describing the application core."
 * )
 */
class StatusController extends Controller
{
    /**
     * @OA\Get(
     *   path="/api/status",
     *   description="Display the status of the application",
     *   tags={"Core"},
     *   security={},
     *
     *   @OA\Response(
     *     response=200,
     *     description="Describes the status of the application.",
     *
     *     @OA\JsonContent(
     *       type="object",
     *
     *       @OA\Property(
     *         property="application",
     *         type="string",
     *         example="Dexes Catalog"
     *       ),
     *       @OA\Property(
     *         property="interface",
     *         type="string",
     *         example="enabled"
     *       ),
     *       @OA\Property(
     *         property="registrations",
     *         type="string",
     *         example="enabled",
     *       ),
     *       @OA\Property(
     *         property="content-types",
     *         type="array",
     *
     *         @OA\Items(
     *           type="string"
     *         )
     *       )
     *     )
     *   ),
     *
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function applicationStatus(): JsonResponse
    {
        return new JsonResponse([
            'application'   => config('app.name'),
            'interface'     => config('xs-framework.enable_gui', false) ? 'enabled' : 'disabled',
            'registration'  => config('xs-auth.users.enable_registration', false) ? 'enabled' : 'disabled',
            'content-types' => array_keys(config('xs-content-types', [])),
        ], status: Response::HTTP_OK);
    }
}
