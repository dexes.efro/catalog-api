<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API;

use App\Http\Requests\DataServiceRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Http\Controllers\BaseContentTypeController;
use XpertSelect\Framework\Services\Search\SearchServiceInterface;

/**
 * Class DataServiceController.
 *
 * Controller that is responsible for DataService CRUD functionality.
 */
class DataServiceController extends BaseContentTypeController
{
    /**
     * {@inheritdoc}
     */
    public function __construct(RepositoryInterface $repository, LoggerInterface $logger, ?SearchServiceInterface $searchService = null)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        parent::__construct($repository, $logger, $searchService);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeIdentifierField(): string
    {
        return 'identifier';
    }

    /**
     * {@inheritdoc}
     */
    public function createContentTypeAttributesFromRequestBody(array $requestBody): array
    {
        return $requestBody;
    }

    /**
     * Return a list of DataServices.
     *
     * @OA\Get(
     *     path="/api/dexes-dataservices",
     *     tags={"Dexes Data Service"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/StartParameter"),
     *     @OA\Parameter(ref="#/components/parameters/RowsParameter"),
     *     @OA\Parameter(ref="#/components/parameters/IdentifierOnlyParameter"),
     *     @OA\Parameter(ref="#/components/parameters/Filters"),
     *
     *     @OA\Response(response=200, ref="#/components/responses/DataServiceListResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function index(Request $request): JsonResponse
    {
        return $this->listRecords($request);
    }

    /**
     * Get a single DataService.
     *
     * @OA\Get(
     *     path="/api/dexes-dataservices/{id}",
     *     tags={"Dexes Data Service"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DataServiceParameter"),
     *
     *     @OA\Response(response=200, ref="#/components/responses/DataServiceResourceResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function show(string $id): JsonResponse
    {
        return $this->showRecord($id);
    }

    /**
     * Store a new DataService.
     *
     * @OA\Post(
     *     path="/api/dexes-dataservices",
     *     tags={"Dexes Data Service"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\RequestBody(
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/DataService"),
     *     ),
     *
     *     @OA\Response(response=201, ref="#/components/responses/DataServiceResourceResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function store(DataServiceRequest $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * Update an existing DataService.
     *
     * @OA\Put(
     *     path="/api/dexes-dataservices/{id}",
     *     tags={"Dexes Data Service"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DataServiceParameter"),
     *
     *     @OA\RequestBody (
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/DataService"),
     *     ),
     *
     *     @OA\Response(response=200, ref="#/components/responses/DataServiceResourceResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function update(DataServiceRequest $request, string $id): JsonResponse
    {
        return $this->updateRecord($request, $id);
    }

    /**
     * Delete a DataService.
     *
     * @OA\Delete(
     *     path="/api/dexes-dataservices/{id}",
     *     tags={"Dexes Data Service"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DataServiceParameter"),
     *
     *     @OA\Response(response=204, ref="#/components/responses/204"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     *
     * @param string $id The identifier of the DataService to delete
     */
    public function destroy(string $id): JsonResponse
    {
        return $this->deleteRecord($id);
    }
}
