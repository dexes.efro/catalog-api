<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API;

use App\Http\Requests\DatasetRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Http\Controllers\BaseContentTypeController;
use XpertSelect\Framework\Services\Search\SearchServiceInterface;

/**
 * Class DatasetController.
 *
 * Controller that is responsible for dataset CRUD functionality.
 */
class DatasetController extends BaseContentTypeController
{
    /**
     * {@inheritdoc}
     */
    public function __construct(RepositoryInterface $repository, LoggerInterface $logger, ?SearchServiceInterface $searchService = null)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        parent::__construct($repository, $logger, $searchService);
    }

    /**
     * {@inheritdoc}
     */
    public function getContentTypeIdentifierField(): string
    {
        return 'identifier';
    }

    /**
     * {@inheritdoc}
     */
    public function createContentTypeAttributesFromRequestBody(array $requestBody): array
    {
        return $requestBody;
    }

    /**
     * @OA\Get(
     *     path="/api/dexes-datasets",
     *     description="Retreive a list of Datasets",
     *     tags={"Dexes Datasets"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/StartParameter"),
     *     @OA\Parameter(ref="#/components/parameters/RowsParameter"),
     *     @OA\Parameter(ref="#/components/parameters/IdentifierOnlyParameter"),
     *     @OA\Parameter(ref="#/components/parameters/Filters"),
     *
     *     @OA\Response(response=200, ref="#/components/responses/DatasetListResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function index(Request $request): JsonResponse
    {
        return $this->listRecords($request);
    }

    /**
     * @OA\Get(
     *     path="/api/dexes-datasets/{id}",
     *     description="Retrieve a specific Dataset",
     *     tags={"Dexes Datasets"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DatasetParameter"),
     *
     *     @OA\Response(response=200, ref="#/components/responses/DatasetResourceResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function show(string $id): JsonResponse
    {
        return $this->showRecord($id);
    }

    /**
     * @OA\Post(
     *     path="/api/dexes-datasets",
     *     description="Create a new Dataset",
     *     tags={"Dexes Datasets"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\RequestBody(
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/Dataset"),
     *     ),
     *
     *     @OA\Response(response=201, ref="#/components/responses/DatasetResourceResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function store(DatasetRequest $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * Update an existing dataset.
     *
     * @OA\Put(
     *     path="/api/dexes-datasets/{id}",
     *     tags={"Dexes Datasets"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DatasetParameter"),
     *
     *     @OA\RequestBody (
     *          request="",
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/Dataset"),
     *     ),
     *
     *     @OA\Response(response=200, ref="#/components/responses/DatasetResourceResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function update(DatasetRequest $request, string $id): JsonResponse
    {
        return $this->updateRecord($request, $id);
    }

    /**
     * Delete a dataset.
     *
     * @OA\Delete(
     *     path="/api/dexes-datasets/{id}",
     *     tags={"Dexes Datasets"},
     *     security={{"bearer_token":{}}},
     *     deprecated=true,
     *
     *     @OA\Parameter(ref="#/components/parameters/DatasetParameter"),
     *
     *     @OA\Response(response=204, ref="#/components/responses/204"),
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     *     @OA\Response(response=409, ref="#/components/responses/409"),
     *     @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     *
     * @param string $id The identifier of the dataset to delete
     */
    public function destroy(string $id): JsonResponse
    {
        return $this->deleteRecord($id);
    }
}
