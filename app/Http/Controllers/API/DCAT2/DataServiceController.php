<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API\DCAT2;

use App\Http\Controllers\Controller;
use App\Http\Requests\DCAT2\PatchDataServiceRequest;
use App\Http\Requests\DCAT2\PostDataServiceRequest;
use App\Repositories\DataServiceRepository;
use App\Services\DCAT2\DocumentGenerator\DataServiceDocumentGenerator;
use App\Services\DCAT2\Serializers\DataServiceSerializer;
use App\Services\DCAT2\Unserializers\DataServiceUnserializer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse as SymfonyJsonResponse;
use Symfony\Component\HttpFoundation\Response;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\InvalidFilterFieldException;
use XpertSelect\Framework\Exceptions\StorageRecordExistsException;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;
use XpertSelect\Framework\Http\HasPagination;
use XpertSelect\Framework\Services\FilterFieldValidator;
use XpertSelect\JsonApi\Document\CollectionDocument;
use XpertSelect\JsonApi\Document\ResourceDocument;
use XpertSelect\JsonApi\Generator\CollectionDocumentGenerator;
use XpertSelect\JsonApi\JsonApiResponseUtilities;

/**
 * Class DataServiceController.
 *
 * DCAT2 compliant.
 */
class DataServiceController extends Controller
{
    use JsonApiResponseUtilities;
    use HasPagination;
    use FilterFieldValidator;

    /**
     * DataServiceController Constructor.
     *
     * @param DataServiceRepository   $repository   The data service repository
     * @param DataServiceSerializer   $serializer   The data service serializer
     * @param LoggerInterface         $logger       The logger implementation to use
     * @param DataServiceUnserializer $unserializer The data service unserializer
     */
    public function __construct(private readonly DataServiceRepository $repository,
                                private readonly DataServiceSerializer $serializer,
                                private readonly LoggerInterface $logger,
                                private readonly DataServiceUnserializer $unserializer)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }

    /**
     * @OA\Get(
     *   path="/api/dcat2/dataservices",
     *   tags={"DCAT2 DataServices"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(ref="#/components/parameters/StartParameter"),
     *   @OA\Parameter(ref="#/components/parameters/RowsParameter"),
     *   @OA\Parameter(ref="#/components/parameters/Filters"),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DataServiceListResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function index(Request $request): SymfonyJsonResponse
    {
        try {
            $start        = $this->determinePaginatedValue($request, 'start', RepositoryInterface::START_DEFAULT);
            $rows         = $this->determinePaginatedValue($request, 'rows', RepositoryInterface::ROWS_DEFAULT);
            $filterFields = $this->processFilters((array) $request->input('filters', []));

            $dataService = $this->repository->paginate($start, $rows, $filterFields, ['id', 'title', 'name']);

            /** @var CollectionDocument $documents */
            $documents = (new CollectionDocumentGenerator())
                ->addResources($dataService->toArray(), $this->serializer)
                ->asDocument();

            $documents->setMetaContainer([
                'start' => $start,
                'rows'  => count($dataService),
                'total' => $this->repository->count($filterFields),
            ]);

            return new JsonResponse($documents->toArray(), headers: $this->jsonApiHeaders());
        } catch (InvalidFilterFieldException) {
            $this->logger->error(sprintf(
                'Content-type %s invalid filter field: %s',
                $this->repository->getName(),
                $request->input('filters')
            ));

            return $this->badRequestErrorResponse([[
                'filter-field' => 'Invalid filter fields: ' . $request->input('filters'),
            ]]);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for index request; %s',
                $this->repository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Get(
     *   path="/api/dcat2/dataservices/{id}",
     *   tags={"DCAT2 DataServices"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataservice", ref="#/components/parameters/DataServiceParameter"),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DataServiceResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=404, ref="#/components/responses/NotFoundErrorResponse"),
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     *
     * @return SymfonyJsonResponse The appropriate JSON response
     */
    public function show(string $id): SymfonyJsonResponse
    {
        try {
            $dataService = $this->repository->find($id);

            $document = (new DataServiceDocumentGenerator())
                ->setResource($dataService, $this->serializer)
                ->asDocument();

            return new JsonResponse($document->toArray(), headers: $this->jsonApiHeaders());
        } catch (StorageRecordNotFoundException) {
            $this->logger->info(sprintf(
                'Request for content-type %s with id %s that does not exist',
                $this->repository->getName(),
                $id
            ));

            return $this->notFoundErrorResponse();
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for show request; %s',
                $this->repository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Post(
     *   path="/api/dcat2/dataservices",
     *   description="Create a new Data Service",
     *   tags={"DCAT2 DataServices"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\RequestBody(
     *
     *     @OA\MediaType(
     *       mediaType="application/vnd.api+json",
     *
     *       @OA\Schema(ref="#/components/schemas/DCAT2DataServiceResource")
     *     )
     *   ),
     *
     *   @OA\Response(response=201, ref="#/components/responses/CreatedDCAT2DataServiceResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function store(PostDataServiceRequest $request): SymfonyJsonResponse
    {
        try {
            $dataService = $this->prepareDataService($request->validated());

            $response = $this->repository->create($dataService);

            /** @var ResourceDocument $document */
            $document = (new DataServiceDocumentGenerator())
                ->setResource($response, $this->serializer)
                ->asDocument();

            $idOrName = array_key_exists('name', $response) ? $response['name'] : $response['identifier'];

            return new JsonResponse($document, 201, $this->jsonApiHeaders() + [
                'Location' => route('api.dcat2.dataservices.show', ['id' => $idOrName]),
            ]);
        } catch (StorageRecordExistsException) {
            $message = sprintf(
                'Request to create content-type %s that already exists',
                $this->repository->getName(),
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for create request; %s',
                $this->repository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Patch(
     *   path="/api/dcat2/dataservices/{id}",
     *   description="Update a dataservice",
     *   tags={"DCAT2 DataServices"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataservice", ref="#/components/parameters/DataServiceParameter"),
     *
     *   @OA\RequestBody(
     *
     *     @OA\MediaType(
     *       mediaType="application/vnd.api+json",
     *
     *       @OA\Schema(ref="#/components/schemas/DCAT2DataServiceResource")
     *     )
     *   ),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DataServiceResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function update(PatchDataServiceRequest $request, string $id): Response
    {
        try {
            $newValues = $this->prepareDataService($request->validated());

            $currentDataservice = $this->repository->find($id);

            $dataService = array_replace_recursive($currentDataservice, $newValues);

            $response = $this->repository->update($currentDataservice['id'], array_filter($dataService));

            /** @var ResourceDocument $document */
            $document = (new DataserviceDocumentGenerator())
                ->setResource($response, $this->serializer)
                ->asDocument();

            return new JsonResponse($document, 200, $this->jsonApiHeaders());
        } catch (StorageRecordNotFoundException) {
            $message = sprintf(
                'Request to update content-type %s with ID %s that does not exist',
                $this->repository->getName(), $id
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for update request; %s',
                $this->repository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/dcat2/dataservices/{id}",
     *     tags={"DCAT2 DataServices"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Parameter(ref="#/components/parameters/DataServiceParameter"),
     *
     *     @OA\Response(response=204, ref="#/components/responses/204"),
     *     @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *     @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *     @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     *
     * @param string $id The identifier of the dataservice to delete
     */
    public function destroy(string $id): Response
    {
        try {
            if (!$this->repository->delete($id)) {
                return $this->internalServerErrorResponse();
            }

            return $this->noContentResponse();
        } catch (StorageRecordNotFoundException) {
            $message = sprintf(
                'Request to delete content-type %s that does not exist; ID %s',
                $this->repository->getName(),
                $id
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for delete request; %s',
                $this->repository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * Transform a JSON API DCAT 2 data service to a dexes dataservice.
     *
     * @param array<string, mixed> $dataService the dataservice to transform
     *
     * @return array<string, mixed> The transformed dataservice
     */
    private function prepareDataService(array $dataService): array
    {
        $dexesDataService                      = $this->unserializer->unserialize($dataService);
        $dexesDataService['modification_date'] = date('Y-m-d\TH:i:s');

        return $dexesDataService;
    }
}
