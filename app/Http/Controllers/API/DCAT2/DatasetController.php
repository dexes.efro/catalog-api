<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API\DCAT2;

use App\Http\Controllers\Controller;
use App\Http\Requests\DCAT2\PatchDatasetRequest;
use App\Http\Requests\DCAT2\PostDatasetRequest;
use App\Repositories\DatasetRepository;
use App\Services\DCAT2\DocumentGenerator\DatasetDocumentGenerator;
use App\Services\DCAT2\Serializers\DatasetSerializer;
use App\Services\DCAT2\Unserializers\DatasetUnserializer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse as SymfonyJsonResponse;
use Symfony\Component\HttpFoundation\Response;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\InvalidFilterFieldException;
use XpertSelect\Framework\Exceptions\StorageRecordExistsException;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;
use XpertSelect\Framework\Http\HasPagination;
use XpertSelect\Framework\Services\FilterFieldValidator;
use XpertSelect\JsonApi\Document\CollectionDocument;
use XpertSelect\JsonApi\Document\ResourceDocument;
use XpertSelect\JsonApi\Generator\CollectionDocumentGenerator;
use XpertSelect\JsonApi\JsonApiResponseUtilities;

/**
 * Class DatasetController.
 *
 * DCAT2.0 compliant
 */
class DatasetController extends Controller
{
    use JsonApiResponseUtilities;
    use HasPagination;
    use FilterFieldValidator;

    /**
     * DatasetController Constructor.
     *
     * @param DatasetRepository   $datasetRepository The dataset repository
     * @param DatasetSerializer   $serializer        The dataset serializer
     * @param LoggerInterface     $logger            The logging implementation to use
     * @param DatasetUnserializer $unserializer      The dataset unserializer
     */
    public function __construct(private readonly DatasetRepository $datasetRepository,
                                private readonly DatasetSerializer $serializer,
                                private readonly LoggerInterface $logger,
                                private readonly DatasetUnserializer $unserializer)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }

    /**
     * @OA\Get(
     *   path="/api/dcat2/datasets",
     *   tags={"DCAT2 Datasets"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(ref="#/components/parameters/StartParameter"),
     *   @OA\Parameter(ref="#/components/parameters/RowsParameter"),
     *   @OA\Parameter(ref="#/components/parameters/Filters"),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DatasetListResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function index(Request $request): SymfonyJsonResponse
    {
        try {
            $start        = $this->determinePaginatedValue($request, 'start', RepositoryInterface::START_DEFAULT);
            $rows         = $this->determinePaginatedValue($request, 'rows', RepositoryInterface::ROWS_DEFAULT);
            $filterFields = $this->processFilters((array) $request->input('filters', []));

            $datasets = $this->datasetRepository->paginate($start, $rows, $filterFields, ['id', 'title', 'name']);

            /** @var CollectionDocument $documents */
            $documents = (new CollectionDocumentGenerator())
                ->addResources($datasets->toArray(), $this->serializer)
                ->asDocument();

            $documents->setMetaContainer([
                'start' => $start,
                'rows'  => count($datasets),
                'total' => $this->datasetRepository->count($filterFields),
            ]);

            return new JsonResponse($documents->toArray(), headers: $this->jsonApiHeaders());
        } catch (InvalidFilterFieldException) {
            $this->logger->error(sprintf(
                'Content-type %s invalid filter field: %s',
                $this->datasetRepository->getName(),
                $request->input('filters')
            ));

            return $this->badRequestErrorResponse([[
                'filter-field' => 'Invalid filter fields: ' . $request->input('filters'),
            ]]);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for index request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Get(
     *   path="/api/dcat2/datasets/{id}",
     *   tags={"DCAT2 Datasets"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataset", ref="#/components/parameters/DatasetParameter"),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DatasetResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=404, ref="#/components/responses/NotFoundErrorResponse"),
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     *
     * @return SymfonyJsonResponse The appropriate JSON response
     */
    public function show(string $id): SymfonyJsonResponse
    {
        try {
            $dataset = $this->datasetRepository->find($id);

            $document = (new DatasetDocumentGenerator())
                ->setResource($dataset, $this->serializer)
                ->asDocument();

            return new JsonResponse($document->toArray(), headers: $this->jsonApiHeaders());
        } catch (StorageRecordNotFoundException) {
            $this->logger->info(sprintf(
                'Request for content-type %s with id %s that does not exist',
                $this->datasetRepository->getName(),
                $id
            ));

            return $this->notFoundErrorResponse();
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for show request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Post(
     *   path="/api/dcat2/datasets",
     *   description="Create a new Dataset",
     *   tags={"DCAT2 Datasets"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\RequestBody(
     *
     *     @OA\MediaType(
     *       mediaType="application/vnd.api+json",
     *
     *       @OA\Schema(ref="#/components/schemas/DCAT2DatasetResource")
     *     )
     *   ),
     *
     *   @OA\Response(response=201, ref="#/components/responses/CreatedDCAT2DatasetResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function store(PostDatasetRequest $request): SymfonyJsonResponse
    {
        try {
            $dataset = $this->prepareDataset($request->validated());

            $response = $this->datasetRepository->create($dataset);

            /** @var ResourceDocument $document */
            $document = (new DatasetDocumentGenerator())
                ->setResource($response, $this->serializer)
                ->asDocument();

            $idOrName = array_key_exists('name', $response) ? $response['name'] : $response['identifier'];

            return new JsonResponse($document, 201, $this->jsonApiHeaders() + [
                'Location' => route('api.dcat2.datasets.show', ['id' => $idOrName]),
            ]);
        } catch (StorageRecordExistsException) {
            $message = sprintf(
                'Request to create content-type %s that already exists',
                $this->datasetRepository->getName(),
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for create request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Patch(
     *   path="/api/dcat2/datasets/{id}",
     *   description="Update a dataset",
     *   tags={"DCAT2 Datasets"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataset", ref="#/components/parameters/DatasetParameter"),
     *
     *   @OA\RequestBody(
     *
     *     @OA\MediaType(
     *       mediaType="application/vnd.api+json",
     *
     *       @OA\Schema(ref="#/components/schemas/DCAT2DatasetResource")
     *     )
     *   ),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DatasetResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function update(PatchDatasetRequest $request, string $id): Response
    {
        try {
            $newValues = $this->prepareDataset($request->validated());

            $currentDataset = $this->datasetRepository->find($id);

            $dataset = array_replace_recursive($currentDataset, $newValues);

            $response = $this->datasetRepository->update($currentDataset['id'], array_filter($dataset));

            /** @var ResourceDocument $document */
            $document = (new DatasetDocumentGenerator())
                ->setResource($response, $this->serializer)
                ->asDocument();

            return new JsonResponse($document, 200, $this->jsonApiHeaders());
        } catch (StorageRecordNotFoundException) {
            $message = sprintf(
                'Request to update content-type %s with ID %s that does not exist',
                $this->datasetRepository->getName(), $id
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for update request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/dcat2/datasets/{id}",
     *     tags={"DCAT2 Datasets"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Parameter(ref="#/components/parameters/DatasetParameter"),
     *
     *     @OA\Response(response=204, ref="#/components/responses/204"),
     *     @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *     @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *     @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     *
     * @param string $id The identifier of the dataset to delete
     */
    public function destroy(string $id): Response
    {
        try {
            if (!$this->datasetRepository->delete($id)) {
                return $this->internalServerErrorResponse();
            }

            return $this->noContentResponse();
        } catch (StorageRecordNotFoundException) {
            $message = sprintf(
                'Request to delete content-type %s that does not exist; ID %s',
                $this->datasetRepository->getName(),
                $id
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for delete request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * Transform a JSON API DCAT 2 dataset to a dexes dataset.
     *
     * @param array<string, mixed> $dataset the dataset to transform
     *
     * @return array<string, mixed> The transformed dataset
     */
    private function prepareDataset(array $dataset): array
    {
        $dexesDataset             = $this->unserializer->unserialize($dataset);
        $dexesDataset['modified'] = date('Y-m-d\TH:i:s');

        if (!array_key_exists('resources', $dexesDataset)) {
            $dexesDataset['resources'] = [];
        }

        return $dexesDataset;
    }
}
