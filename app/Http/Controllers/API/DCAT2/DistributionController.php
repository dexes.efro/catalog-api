<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API\DCAT2;

use App\Http\Controllers\Controller;
use App\Http\Requests\DCAT2\PatchDistributionRequest;
use App\Http\Requests\DCAT2\PostDistributionRequest;
use App\Repositories\DatasetRepository;
use App\Services\DCAT2\DocumentGenerator\DistributionDocumentGenerator;
use App\Services\DCAT2\Serializers\DistributionSerializer;
use App\Services\DCAT2\Unserializers\DistributionUnserializer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse as SymfonyJsonResponse;
use Symfony\Component\HttpFoundation\Response;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;
use XpertSelect\Framework\Http\HasPagination;
use XpertSelect\Framework\Services\FilterFieldValidator;
use XpertSelect\JsonApi\Document\CollectionDocument;
use XpertSelect\JsonApi\Document\ResourceDocument;
use XpertSelect\JsonApi\Generator\CollectionDocumentGenerator;
use XpertSelect\JsonApi\JsonApiResponseUtilities;

/**
 * Class DistributionController.
 */
class DistributionController extends Controller
{
    use JsonApiResponseUtilities;
    use HasPagination;
    use FilterFieldValidator;

    /**
     * DistributionController Constructor.
     *
     * @param DatasetRepository        $datasetRepository The dataset repository
     * @param DistributionSerializer   $serializer        The distribution serializer
     * @param LoggerInterface          $logger            The logging implementation to use
     * @param DistributionUnserializer $unserializer      The distribution unserializer
     */
    public function __construct(private readonly DatasetRepository $datasetRepository,
                                private readonly DistributionSerializer $serializer,
                                private readonly LoggerInterface $logger,
                                private readonly DistributionUnserializer $unserializer)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }

    /**
     * @OA\Get(
     *   path="/api/dcat2/datasets/{dataset_id}/distributions",
     *   tags={"DCAT2 Distributions"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataset_id", ref="#/components/parameters/DatasetIdParameter"),
     *   @OA\Parameter(ref="#/components/parameters/StartParameter"),
     *   @OA\Parameter(ref="#/components/parameters/RowsParameter"),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DistributionListResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=404, ref="#/components/responses/NotFoundErrorResponse"),
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function index(Request $request, string $dataset_id): SymfonyJsonResponse
    {
        try {
            $start = $this->determinePaginatedValue($request, 'start', RepositoryInterface::START_DEFAULT);
            $rows  = $this->determinePaginatedValue($request, 'rows', RepositoryInterface::ROWS_DEFAULT);

            $dataset       = $this->datasetRepository->find($dataset_id);
            $packageName   = $dataset['name'];

            if (!array_key_exists('resources', $dataset)) {
                $dataset['resources'] = [];
            }

            $distributions = $dataset['resources'];

            $selectedItems = array_slice($distributions, $start, $rows);

            /*
             * TODO when creating or updating a distribution make sure the package_name is included
             * Then we don't have to add it manually each time
             */
            foreach ($selectedItems as &$distribution) {
                $distribution['package_name'] = $packageName;
            }
            unset($distribution);

            /** @var CollectionDocument $documents */
            $documents = (new CollectionDocumentGenerator())
                ->addResources($selectedItems, $this->serializer)
                ->asDocument();

            $documents->setMetaContainer([
                'start' => $start,
                'rows'  => count($selectedItems),
                'total' => count($distributions),
            ]);

            return new JsonResponse($documents->toArray(), headers: $this->jsonApiHeaders());
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for index request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        } catch (StorageRecordNotFoundException) {
            $this->logger->info(sprintf(
                'Request for content-type %s with id %s that does not exist',
                $this->datasetRepository->getName(),
                $dataset_id
            ));

            return $this->notFoundErrorResponse();
        }
    }

    /**
     * @OA\Post(
     *   path="/api/dcat2/datasets/{dataset_id}/distributions",
     *   description="Create a new Distribution",
     *   tags={"DCAT2 Distributions"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataset_id", ref="#/components/parameters/DatasetIdParameter"),
     *
     *   @OA\RequestBody(
     *
     *     @OA\MediaType(
     *       mediaType="application/vnd.api+json",
     *
     *       @OA\Schema(ref="#/components/schemas/DCAT2DistributionResource")
     *     )
     *   ),
     *
     *   @OA\Response(response=201, ref="#/components/responses/CreatedDCAT2DistributionResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function store(PostDistributionRequest $request, string $dataset_id): SymfonyJsonResponse
    {
        try {
            $dataset = $this->datasetRepository->find($dataset_id);

            $distribution = $this->prepareDistribution($request->validated(), $dataset);

            $dataset['resources'][] = $distribution;

            $dataset = $this->datasetRepository->update($dataset['id'], $dataset);

            $distributions = $dataset['resources'];
            $distribution  = end($distributions);

            /** @var ResourceDocument $document */
            $document = (new DistributionDocumentGenerator())
                ->setResource($distribution, $this->serializer)
                ->asDocument();

            return new JsonResponse($document, 201, $this->jsonApiHeaders() + [
                'Location' => route('api.dcat2.datasets.distributions.show', ['dataset_id' => $dataset['name'], 'id' => $distribution['id']]),
            ]);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for create request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        } catch (StorageRecordNotFoundException) {
            $message = sprintf(
                'Request to update content-type %s with ID %s that does not exist',
                $this->datasetRepository->getName(), $dataset_id
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        }
    }

    /**
     * @OA\Patch(
     *   path="/api/dcat2/datasets/{dataset_id}/distributions/{id}",
     *   description="Update a distribution",
     *   tags={"DCAT2 Distributions"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataset_id", ref="#/components/parameters/DatasetIdParameter"),
     *   @OA\Parameter(parameter="dataset", ref="#/components/parameters/DistributionParameter"),
     *
     *   @OA\RequestBody(
     *
     *     @OA\MediaType(
     *       mediaType="application/vnd.api+json",
     *
     *       @OA\Schema(ref="#/components/schemas/DCAT2DistributionResource")
     *     )
     *   ),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DistributionResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=404, ref="#/components/responses/NotFoundErrorResponse"),
     *   @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     */
    public function update(PatchDistributionRequest $request, string $dataset_id, string $id): Response
    {
        try {
            $dataset = $this->datasetRepository->find($dataset_id);

            $newValues = $this->prepareDistribution($request->validated(), $dataset);

            if (!$this->distributionInDataset($dataset, $id)) {
                $message = sprintf(
                    'Distribution with id: %s not found in dataset %s',
                    $id, $dataset_id
                );
                $this->logger->warning($message);

                return $this->conflictErrorResponse($message);
            }

            foreach ($dataset['resources'] as &$distribution) {
                if ($distribution['id'] === $id) {
                    $distribution = array_replace_recursive($distribution, $newValues);
                }
            }
            unset($distribution);

            $response = $this->datasetRepository->update($dataset['id'], $dataset);

            $distribution = $this->distributionFromDataset($response, $id);

            if (null === $distribution) {
                $message = sprintf('Failed to update distribution with ID: %s', $id);
                $this->logger->warning($message);

                return $this->conflictErrorResponse($message);
            }

            /** @var ResourceDocument $document */
            $document = (new DistributionDocumentGenerator())
                ->setResource($distribution, $this->serializer)
                ->asDocument();

            return new JsonResponse($document, 200, $this->jsonApiHeaders());
        } catch (StorageRecordNotFoundException) {
            $message = sprintf(
                'Request to update distribution %s from content-type %s with ID %s that does not exist',
                $id, $this->datasetRepository->getName(), $dataset_id
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for update request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Get(
     *   path="/api/dcat2/datasets/{dataset_id}/distributions/{id}",
     *   tags={"DCAT2 Distributions"},
     *   security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataset", ref="#/components/parameters/DatasetIdParameter"),
     *   @OA\Parameter(parameter="dataset", ref="#/components/parameters/DistributionParameter"),
     *
     *   @OA\Response(response=200, ref="#/components/responses/DCAT2DistributionResourceResponse"),
     *   @OA\Response(response=400, ref="#/components/responses/BadRequestErrorResponse"),
     *   @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *   @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *   @OA\Response(response=404, ref="#/components/responses/NotFoundErrorResponse"),
     *   @OA\Response(response=429, ref="#/components/responses/429"),
     *   @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     *
     * @return SymfonyJsonResponse The appropriate JSON response
     */
    public function show(string $dataset_id, string $id): SymfonyJsonResponse
    {
        try {
            $dataset = $this->datasetRepository->find($dataset_id);

            $distribution                 = $this->distributionFromDataset($dataset, $id);
            if (null === $distribution) {
                return $this->notFoundErrorResponse();
            }
            $distribution['package_name'] = $dataset['name'];

            $document = (new DistributionDocumentGenerator())
                ->setResource($distribution, $this->serializer)
                ->asDocument();

            return new JsonResponse($document->toArray(), headers: $this->jsonApiHeaders());
        } catch (StorageRecordNotFoundException) {
            $this->logger->info(sprintf(
                'Request for content-type %s with id %s that does not exist',
                $this->datasetRepository->getName(),
                $id
            ));

            return $this->notFoundErrorResponse();
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for show request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/dcat2/datasets/{dataset_id}/distributions/{id}",
     *     tags={"DCAT2 Distributions"},
     *     security={{"bearer_token":{}}},
     *
     *   @OA\Parameter(parameter="dataset", ref="#/components/parameters/DatasetIdParameter"),
     *   @OA\Parameter(parameter="dataset", ref="#/components/parameters/DistributionParameter"),
     *
     *     @OA\Response(response=204, ref="#/components/responses/204"),
     *     @OA\Response(response=401, ref="#/components/responses/UnauthenticatedErrorResponse"),
     *     @OA\Response(response=403, ref="#/components/responses/NoAccessErrorResponse"),
     *     @OA\Response(response=409, ref="#/components/responses/ConflictErrorResponse"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/InternalServerErrorResponse")
     * )
     *
     * @param string $id The identifier of the distribution to delete
     */
    public function destroy(string $dataset_id, string $id): Response
    {
        try {
            $dataset = $this->datasetRepository->find($dataset_id);

            if (!$this->distributionInDataset($dataset, $id)) {
                $message = sprintf(
                    'Request to delete distribution that does not exist: ID %s',
                    $id
                );
                $this->logger->info($message);

                return $this->conflictErrorResponse($message);
            }

            foreach ($dataset['resources'] as $key => $distribution) {
                if ($distribution['id'] === $id) {
                    unset($dataset['resources'][$key]);

                    break;
                }
            }

            $updatetDataset = $this->datasetRepository->update($dataset_id, $dataset);

            if ($this->distributionInDataset($updatetDataset, $id)) {
                return $this->internalServerErrorResponse();
            }

            return $this->noContentResponse();
        } catch (StorageRecordNotFoundException) {
            $message = sprintf(
                'Request to delete distribution from content-type %s that does not exist; ID %s',
                $this->datasetRepository->getName(),
                $id
            );
            $this->logger->info($message);

            return $this->conflictErrorResponse($message);
        } catch (StorageRecordStorageException $e) {
            $this->logger->critical(sprintf(
                'Content-type %s storage unavailable for delete request; %s',
                $this->datasetRepository->getName(),
                $e->getMessage()
            ));

            return $this->internalServerErrorResponse();
        }
    }

    /**
     * Checks whether the given distribution id exists in the dataset.
     *
     * @param array<string, mixed> $dataset The dataset to check
     * @param string               $id      The distribution id
     *
     * @return bool whether the distribution exists in the dataset or not
     */
    private function distributionInDataset(array $dataset, string $id): bool
    {
        if (!array_key_exists('resources', $dataset)) {
            return false;
        }

        foreach ($dataset['resources'] as $distribution) {
            if ($distribution['id'] === $id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Given a dataset and distribution_id, return the distribution. If there is no corresponding
     * distribution for the given ID return NULL.
     *
     * @param array<string, mixed> $dataset The dataset
     * @param string               $id      the distribution id
     *
     * @return ?array<string, mixed> The distribution or NULL
     */
    private function distributionFromDataset(array $dataset, string $id): ?array
    {
        if (!array_key_exists('resources', $dataset)) {
            return null;
        }

        foreach ($dataset['resources'] as $distribution) {
            if ($distribution['id'] === $id) {
                return $distribution;
            }
        }

        return null;
    }

    /**
     * Transform a JSON API DCAT 2 Distribution to a dexes distribution.
     *
     * @param array<string, mixed> $distribution the Distribution to transform
     * @param array<string, mixed> $dataset      the dataset the distribution belongs to
     *
     * @return array<string, mixed> The transformed distribution
     */
    private function prepareDistribution(array $distribution, array $dataset): array
    {
        $serialized                 = $this->unserializer->unserialize($distribution);
        $serialized['package_name'] = $dataset['name'];

        return $serialized;
    }
}
