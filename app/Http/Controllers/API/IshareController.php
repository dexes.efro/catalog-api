<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers\API;

use App\Exceptions\IshareException;
use App\Services\Ishare\PartiesService;
use Illuminate\Http\JsonResponse;

/**
 * Class IShareController.
 */
class IshareController
{
    /**
     * IShareController constructor.
     *
     * @param PartiesService $partiesService a service to retrieve parties information from an iShare satellite
     */
    public function __construct(private readonly PartiesService $partiesService)
    {
    }

    /**
     * @OA\Get(
     *     path="/api/ishare/parties",
     *     tags={"Ishare"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Response(response="200", description="test",
     *
     *        @OA\JsonContent(
     *          type="array",
     *
     *          @OA\Items()
     *         ),
     *     ),
     *
     *     @OA\Response(response=401, ref="#/components/responses/401"),
     *     @OA\Response(response=403, ref="#/components/responses/403"),
     *     @OA\Response(response=429, ref="#/components/responses/429"),
     *     @OA\Response(response=500, ref="#/components/responses/500")
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $parties = $this->partiesService->all();

            return new JsonResponse($parties);
        } catch (IshareException $e) {
            return new JsonResponse($e->getMessage(), 500);
        }
    }
}
