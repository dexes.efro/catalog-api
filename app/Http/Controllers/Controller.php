<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class Controller.
 *
 * @OA\Info(
 *   title=APP_NAME,
 *   version="1.0",
 *   description="The HTTP REST API specification for this Dexes application.",
 *
 *   @OA\Contact(
 *     name=CONTACT_NAME,
 *     url=CONTACT_URL,
 *     email=CONTACT_EMAIL
 *   )
 * )
 *
 * @OA\PathItem(
 *   path="/",
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;
}
