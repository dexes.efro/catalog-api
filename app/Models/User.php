<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use XpertSelect\Framework\Models\Auth\User as XSUser;

/**
 * Class User.
 *
 * The User model as defined by XS/Accounts. Can be extended by including new definitions in this
 * model class.
 */
class User extends XSUser
{
}
