<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class Spatial.
 *
 * @property string $scheme The scheme of a Spatial
 * @property string $value  The value of a spatial
 */
class Spatial extends Model
{
    public const NAME = 'spatial';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * {@inheritdoc}
     */
    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'scheme',
        'value',
    ];
}
