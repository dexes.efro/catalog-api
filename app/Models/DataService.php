<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Builder;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Relations\EmbedsOne;
use XpertSelect\SolariumClient\Services\SolrDocumentMapper;

/**
 * Class DataService.
 *
 * @method static Builder query()
 *
 * @OA\Parameter(
 *     parameter="DataServiceParameter",
 *     description="The ID attribute identifying the DataService. Alternatively, you can also find a DataService by the 'name' attribute.",
 *     name="id",
 *
 *     @OA\Schema(
 *          type="string",
 *          default="uuid",
 *     ),
 *     in="path",
 *     required=true
 * )
 */
class DataService extends Model
{
    use SolrDocumentMapper;

    /**
     * The name of the mongodb collection.
     */
    public const COLLECTION_NAME = 'dexes-dataservices';

    /**
     * {@inheritdoc}
     */
    protected $collection = self::COLLECTION_NAME;

    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name', 'consumer_name', 'consumer_checksum',
    ];

    public function contact_point(): EmbedsOne
    {
        return $this->embedsOne(ContactPoint::class);
    }
}
