<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use App\Services\HasAssets;
use App\Services\Lists\ListItemInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Builder;
use XpertSelect\SolariumClient\Services\SolrDocumentMapper;

/**
 * Class Dataspace.
 *
 * @property string $id
 * @property string $name
 * @property string $title
 * @property string $intro_text
 * @property string $uri
 * @property string $logo_url
 * @property string $join_url
 * @property string $asset_url
 *
 * @method static Builder query()
 *
 * @OA\Schema(
 *   schema="Dataspace",
 *   description="Describes a Dataspace resource.",
 *
 *     @OA\Property(
 *       property="id",
 *       type="string",
 *       example="1",
 *     ),
 *     @OA\Property(
 *       property="name",
 *       type="string",
 *       example="marineterrein",
 *     ),
 *     @OA\Property(
 *       property="title",
 *       type="string",
 *       example="Marineterrein",
 *     ),
 *     @OA\Property(
 *       property="intro_text",
 *       type="string",
 *       example="Publieke data marktplaats voor het delen van data uit de sensoren op het marineterrein Amsterdam.",
 *     ),
 *     @OA\Property(
 *       property="ruleframework_id",
 *       type="string",
 *       example="1"
 *     ),
 *     @OA\Property(
 *      property="uri",
 *      type="string",
 *      example="https://marineterrein.dexspace.nl"
 *    ),
 *    @OA\Property(
 *      property="logo_url",
 *      type="string",
 *      example="https://catalog-api.com/images/image.jpg"
 *    ),
 *    @OA\Property(
 *      property="asset_url",
 *      type="string",
 *      example="https://catalog-api.com/assets/asset.jpg"
 *    ),
 *    @OA\Property(
 *      property="join_url",
 *      type="string",
 *      example="https://dexspace.nl/join"
 *    )
 * )
 *
 * @OA\Parameter(
 *   parameter="DataspaceParameter",
 *   description="The ID attribute identifying the dataspace.",
 *   name="id",
 *
 *   @OA\Schema(
 *     type="string",
 *     default="1"
 *   ),
 *   in="path",
 *   required=true
 * )
 *
 * @OA\Response(
 *   response="DataspaceResponse",
 *   description="The Dataspace resource in question.",
 *
 *   @OA\JsonContent(ref="#/components/schemas/Dataspace")
 * )
 */
class Dataspace extends Model implements ListItemInterface
{
    use HasFactory;
    use SolrDocumentMapper;
    use HasAssets;

    protected $fillable = [
        'id', 'name', 'title', 'intro_text', 'asset_url', 'ruleframework_id', 'uri', 'logo_url',
        'join_url',
    ];

    /**
     * Get the validation rules that apply for this model.
     *
     * @return array<string, array<int, string>>
     */
    public static function rules(): array
    {
        return [
            'name'             => ['max:150'],
            'title'            => ['required', 'max:150'],
            'asset'            => ['file', 'mimes:png,jpg,jpeg,svg'],
            'logo'             => ['file', 'mimes:png,jpg,jpeg,svg'],
            'intro_text'       => ['required', 'max:3000'],
            'ruleframework_id' => ['required', 'exists:App\Models\RuleFramework,id'],
            'uri'              => ['required', 'url'],
            'join_url'         => ['required', 'url'],
        ];
    }

    /**
     * Transform the Dataspace model into a key => value array that can be sent to Apache Solr.
     *
     * @return array<string, ?string> The solr document as a key => value array
     */
    public function asSearchSolrDocument(): array
    {
        return [
            'sys_type'    => 'dataspace',
            'sys_id'      => $this->id,
            'sys_name'    => $this->name,
            'sys_uri'     => $this->uri,
            'title'       => $this->title,
            'description' => $this->intro_text,
            'asset_logo'  => $this->logo_url,
        ];
    }

    /**
     * Get the image_url.
     *
     * @param ?string $value
     *
     * @return null|string the logo asset path, or null
     */
    public function getLogoUrlAttribute(?string $value): ?string
    {
        return $this->getAssetPath($value, 'logo', $this->id);
    }

    /**
     * Get the asset_url.
     *
     * @param ?string $value
     *
     * @return null|string the url asset path, or null
     */
    public function getAssetUrlAttribute(?string $value): ?string
    {
        return $this->getAssetPath($value, 'asset', $this->id);
    }

    /**
     * {@inheritdoc}
     */
    public function getListItem(): array
    {
        return [
            $this->uri => [
                'labels' => [
                    'nl-NL' => $this->title,
                    'en-US' => $this->title,
                ],
            ],
        ];
    }
}
