<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class Resource.
 *
 * @property string $url               The url of a Resource
 * @property string $download_url      The download_url of a Resource
 * @property array  $documentation     The documentation of a Resource
 * @property array  $linked_schemas    The linked schemas of a Resource
 * @property string $name              The name of a Resource
 * @property string $description       The description of a Resource
 * @property string $status            The status of a Resource
 * @property string $metadata_language The metadata language of a Resource
 * @property array  $language          The language of a Resource
 * @property string $license_id        The license_id of a Resource
 * @property string $rights            The rights of a Resource
 * @property string $format            The format of a Resource
 * @property string $media_type        The media_type of a Resource
 * @property string $size              The size of a Resource
 * @property string $hash              The hash of a Resource
 * @property string $hash_algorithm    The hash algorithm of a Resource
 * @property string $release_date      The release date of a Resource
 * @property string $modification_date The modification date of a Resource
 * @property string $distribution_type The distribution_type of a Resource
 *
 * @OA\Parameter(
 *   parameter="DistributionParameter",
 *   description="The ID attribute identifying the distribution.",
 *   name="id",
 *
 *   @OA\Schema(
 *     type="string"
 *   ),
 *   in="path",
 *   required=true
 * )
 */
class Resource extends Model
{
    public const NAME = 'resources';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * {@inheritdoc}
     */
    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'url',
        'download_url',
        'documentation',
        'linked_schemas',
        'name',
        'description',
        'status',
        'metadata_language',
        'language',
        'license_id',
        'rights',
        'format',
        'media_type',
        'size',
        'hash',
        'hash_algorithm',
        'release_date',
        'modification_date',
        'distribution_type',
        'id',
    ];
}
