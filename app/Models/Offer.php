<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use Database\Factories\OfferFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Builder;
use RuntimeException;
use XpertSelect\SolariumClient\Services\SolrDocumentMapper;

/**
 * Class Offer.
 *
 * @property string $id
 * @property string $url
 * @property string $json
 *
 * @method static Builder query()
 *
 * @OA\Parameter(
 *   parameter="OfferParameter",
 *   description="The ID attribute identifying the offer.",
 *   name="id",
 *
 *   @OA\Schema(
 *     type="string",
 *     default="uuid"
 *   ),
 *   in="path",
 *   required=true
 * )
 */
class Offer extends Model
{
    use HasFactory;
    use SolrDocumentMapper;

    /**
     * {@inheritdoc}
     */
    public $incrementing = false;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'id', 'url', 'json',
    ];

    /**
     * Get the validation rules that apply for this model.
     *
     * @return array<string, array<int, mixed>> The validation rules
     */
    public static function rules(): array
    {
        return [
            'id'             => ['required'],
            'url'            => ['required'],
            'dataOwner'      => ['required', 'string'],
            'ownerSignature' => ['nullable'],
            'resource'       => ['required', 'string'],
            'conditions'     => ['array'],
            'permitted'      => ['nullable'],
            'scope'          => ['nullable'],
            'basedOn'        => ['nullable'],
            'sourceTemplate' => ['nullable'],
            'recipient'      => ['nullable'],
            'createdAt'      => ['required'],
            'updatedAt'      => ['nullable'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected static function newFactory(): OfferFactory
    {
        return new OfferFactory();
    }

    /**
     * Transform the Offer model into a key => value array that can be sent to Apache Solr.
     *
     * @return array<string, array<int, mixed>> The Solr document as a key => value array
     */
    public function asSolrDocument(): array
    {
        $sourceData = json_decode($this->json, true);
        $defaults   = [
            'title'           => 'Offer ' . $this->id,
            'exchange_type'   => 'offer',
            'exchange_json'   => $this->json,
            'condition_count' => 0,
            'condition_types' => [],
        ];

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new RuntimeException('Offer contains invalid JSON');
        }

        $document = $this->mapToSolrKeyValues(
            $sourceData,
            config('xs-content-types.dexes-offer.search.mapping', []),
            config('xs-content-types.dexes-offer.search.date-fields', []),
            $defaults
        );

        if (array_key_exists('conditions', $sourceData)) {
            $conditions     = $sourceData['conditions'];
            $conditionTypes = [];

            foreach ($conditions as $condition) {
                if (array_key_exists('type', $condition)) {
                    $conditionTypes[] = $condition['type'];
                }
            }

            $document['condition_count'] = count($conditions);
            $document['condition_types'] = array_unique($conditionTypes);
        }

        return $document;
    }
}
