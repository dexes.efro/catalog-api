<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class LegalFoundation.
 *
 * @property string $label The label of a legal foundation
 * @property string $ref   The ref of a legal foundation
 * @property string $uri   The uri of a legal foundation
 */
class LegalFoundation extends Model
{
    public const NAME = 'legal_foundation';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * {@inheritdoc}
     */
    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'label',
        'ref',
        'uri',
    ];
}
