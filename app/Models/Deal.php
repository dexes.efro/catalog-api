<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use App\Repositories\DatasetRepository;
use App\Repositories\OfferRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Jenssegers\Mongodb\Eloquent\Builder;
use RuntimeException;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordException;
use XpertSelect\SolariumClient\Services\SolrDocumentMapper;

/**
 * Class Deal.
 *
 * @property string $id
 * @property string $url
 * @property string $json
 *
 * @method static Builder query()
 *
 * @OA\Parameter(
 *   parameter="DealParameter",
 *   description="The ID attribute identifying the deal.",
 *   name="id",
 *
 *   @OA\Schema(
 *     type="string",
 *     default="uuid"
 *   ),
 *   in="path",
 *   required=true
 * )
 */
class Deal extends Model
{
    use SolrDocumentMapper;

    /**
     * {@inheritdoc}
     */
    public $incrementing = false;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'id', 'url', 'json',
    ];

    /**
     * Defines the Laravel validation rules that determine if an incoming deal is valid.
     *
     * @return array<string, array<int, mixed>> The validation rules
     */
    public static function rules(): array
    {
        return [
            'id'             => ['required'],
            'url'            => ['required'],
            'offer'          => ['nullable'],
            'dataOwner'      => ['required', 'string'],
            'resource'       => ['required', 'string'],
            'recipient'      => ['nullable'],
            'permitted'      => ['nullable'],
            'scope'          => ['nullable'],
            'basedOn'        => ['nullable'],
            'sourceTemplate' => ['nullable'],
            'commitments'    => ['array'],
            'signature'      => ['nullable'],
            'createdAt'      => ['required', 'date'],
            'updatedAt'      => ['nullable'],
        ];
    }

    /**
     * Transform the Deal model into a key => value array that can be sent to Apache Solr.
     *
     * @return array<string, array<string, mixed>> The Solr document as a key => value array
     */
    public function asSolrDocument(): array
    {
        $sourceData = json_decode($this->json, true);
        $defaults   = [
            'title'           => 'Deal ' . $this->id,
            'exchange_type'   => 'deal',
            'exchange_json'   => $this->json,
            'condition_count' => 0,
            'condition_types' => [],
        ];

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new RuntimeException('Deal contains invalid JSON');
        }

        $document = $this->mapToSolrKeyValues(
            $sourceData,
            config('xs-content-types.dexes-deal.search.mapping', []),
            config('xs-content-types.dexes-deal.search.date-fields', []),
            $defaults
        );

        if (array_key_exists('offer', $sourceData)) {
            try {
                /** @var OfferRepository $offerRepository */
                $offerRepository = app(OfferRepository::class);

                /** @var Offer $offer */
                $offer = $offerRepository->find($sourceData['offer']);

                $document['offer'] = $offer->url;
            } catch (StorageRecordException) {
                Log::info('Deal refers to unknown offer, omitting reference');
            }
        }

        if (array_key_exists('commitments', $sourceData)) {
            $conditions                  = $sourceData['commitments'];
            $document['condition_count'] = count($conditions);
            $conditionTypes              = [];

            foreach ($conditions as $condition) {
                if (array_key_exists('type', $condition)) {
                    $conditionTypes[] = $condition['type'];
                }
            }

            $document['condition_types'] = array_unique($conditionTypes);
        }

        $dataset = $this->getDatasetOfResource($sourceData['resource']);

        if (null !== $dataset) {
            $document['dataset_title']      = $dataset['title'];
            $document['dataset_identifier'] = $dataset['identifier'];
            $document['title']              = 'Deal - ' . $dataset['title'];
        }

        return $document;
    }

    /**
     * Transform the Deal model into a key => value array that can be sent to Apache Solr.
     *
     * @return array<string, array<string, mixed>> The Solr document as a key => value array
     */
    public function asSearchSolrDocument(): array
    {
        $sourceData = json_decode($this->json, true);

        $defaults   = [
            'sys_type' => 'deal',
            'sys_id'   => $this->id,
            'sys_uri'  => $this->url,
            'title'    => 'Deal ' . $this->id,
        ];

        $document = $this->mapToSolrKeyValues($sourceData,
            config('xs-content-types.dexes-deal.search.search-mapping', []),
            config('xs-content-types.dexes-deal.search.date-fields', []),
            $defaults);

        $dataset = $this->getDatasetOfResource($sourceData['resource']);

        if (null !== $dataset) {
            $document['relation_dataset'] = $dataset['identifier'];
            $document['title']            = 'Deal - ' . $dataset['title'];

            // temporary fix so we can use the dataset name to filter in the search results page
            $document['sys_name'] = $dataset['name'];
            if (array_key_exists('dataspace_uri', $dataset)) {
                $document['relation_dataspace'] = $dataset['dataspace_uri'];
            }
        }

        return $document;
    }

    /**
     * Get the dataset of a given resource.
     *
     * @param string $resource The resource
     *
     * @return null|array<string, mixed> The dataset
     */
    private function getDatasetOfResource(string $resource): ?array
    {
        try {
            /** @var RepositoryInterface $datasetRepo */
            $datasetRepo = app(DatasetRepository::class);
            $dataset     = $datasetRepo->findByAttribute('resources.url', $resource)->first();

            if (null === $dataset) {
                return null;
            }

            if (is_array($dataset)) {
                return $dataset;
            }

            return $dataset->toArray();
        } catch (StorageRecordException $e) {
            Log::info(sprintf(
                'Error thrown while trying to fetch a dataset by resource: %s. Error: %s',
                $resource, $e->getMessage()
            ));

            return null;
        }
    }
}
