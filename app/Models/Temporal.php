<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class Temporal.
 *
 * @property string $label The label of a temporal
 * @property string $start The start of a temporal
 * @property string $end   The end of a temporal
 */
class Temporal extends Model
{
    public const NAME = 'temporal';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * {@inheritdoc}
     */
    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'label',
        'start',
        'end',
    ];
}
