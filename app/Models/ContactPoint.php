<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class ContactPoint.
 *
 * @property string $name    The name of a contact point
 * @property string $title   The title of a contact point
 * @property string $address The address of a contact point
 * @property string $email   The email of a contact point
 * @property string $website The website of a contact point
 * @property string $phone   The phone of a contact point
 */
class ContactPoint extends Model
{
    public const NAME = 'contact_point';

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * {@inheritdoc}
     */
    protected $primaryKey = 'id';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name',
        'title',
        'address',
        'email',
        'website',
        'phone',
    ];
}
