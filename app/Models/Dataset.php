<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Builder;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Relations\EmbedsMany;
use Jenssegers\Mongodb\Relations\EmbedsOne;

/**
 * Class Dataset.
 *
 * @method static Builder query()
 *
 * @OA\Parameter(
 *   parameter="DatasetParameter",
 *   description="The ID attribute identifying the dataset. Alternatively you can also find a dataset by the 'name' attribute.",
 *   name="id",
 *
 *   @OA\Schema(
 *     type="string",
 *     default="uuid"
 *   ),
 *   in="path",
 *   required=true
 * )
 *
 * @OA\Parameter(
 *     parameter="DatasetIdParameter",
 *     description="The ID attribute identifying the dataset. Alternatively you can also find a dataset by the 'name' attribute.",
 *     name="dataset_id",
 *
 *     @OA\Schema(type="string"),
 *     in="path",
 *     required=true
 * )
 */
class Dataset extends Model
{
    /**
     * The name of the mongodb collection.
     */
    public const COLLECTION_NAME = 'dexes-datasets';

    /**
     * {@inheritdoc}
     */
    protected $collection = self::COLLECTION_NAME;

    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'title', 'notes', 'url', 'theme', 'identifier', 'alternate_identifier', 'source_catalog',
        'authority', 'publisher', 'metadata_language', 'language', 'license_id', 'access_rights',
        'access_rights_reason', 'dataset_status', 'date_planned', 'issued', 'modified', 'frequency',
        'documentation', 'sample', 'provenance', 'version', 'version_notes', 'is_version_of',
        'has_version', 'source', 'related_resource', 'conforms_to', 'high_value', 'referentie_data',
        'basis_register', 'national_coverage', 'sector_registrations', 'local_registrations',
        'linked_concepts', 'changetype', 'name', 'consumer_name', 'consumer_checksum', 'has_policy',
    ];

    public function contact_point(): EmbedsOne
    {
        return $this->embedsOne(ContactPoint::class);
    }

    public function temporal(): EmbedsOne
    {
        return $this->embedsOne(Temporal::class);
    }

    public function spatial(): EmbedsOne
    {
        return $this->embedsOne(Spatial::class);
    }

    public function legal_foundation(): EmbedsOne
    {
        return $this->embedsOne(LegalFoundation::class);
    }

    public function resources(): EmbedsMany
    {
        return $this->embedsMany(Resource::class);
    }
}
