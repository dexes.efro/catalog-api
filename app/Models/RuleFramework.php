<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Models;

use App\Services\HasAssets;
use App\Services\Lists\ListItemInterface;
use Database\Factories\RuleFrameworkFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Builder;

/**
 * Class RuleFramework.
 *
 * @property string $id
 * @property string $title
 * @property string $name
 * @property string $intro_text
 * @property string $image_url
 * @property string $url
 *
 * @method static Builder query()
 *
 * @OA\Schema(
 *     schema="RuleFramework",
 *     description="Describes an RuleFramework resource.",
 *
 *     @OA\Property(
 *         property="id",
 *         type="string",
 *         example="1"
 *     ),
 *     @OA\Property(
 *         property="title",
 *         type="string",
 *         example="Dexes"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         example="dexes"
 *     ),
 *     @OA\Property(
 *         property="intro_text",
 *         type="string",
 *         example="Op deze dataspace gelden de regels van Dexes."
 *     ),
 *     @OA\Property(
 *         property="image_url",
 *         type="string",
 *         example="https://catalog-api.com/images/image.jpg"
 *     ),
 *     @OA\Property(
 *         property="url",
 *         type="string",
 *         example="https://dexes.eu"
 *     )
 * )
 *
 * @OA\Parameter(
 *     parameter="RuleFrameworkParameter",
 *     description="The ID attribute identifying the RuleFramework",
 *     name="id",
 *
 *     @OA\Schema(
 *          type="string",
 *          default="1",
 *     ),
 *     in="path",
 *     required=true
 * )
 */
class RuleFramework extends Model implements ListItemInterface
{
    use HasFactory;
    use HasAssets;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'id', 'name', 'intro_text', 'image_url', 'title', 'url',
    ];

    /**
     * @return array<string, array<int, string>>
     */
    public static function rules(): array
    {
        return [
            'title'       => ['required', 'max:150'],
            'name'        => ['max:150'],
            'intro_text'  => ['nullable', 'max:3000'],
            'image'       => ['file', 'mimes:png,jpg,jpeg,svg'],
            'url'         => ['nullable', 'url'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected static function newFactory(): RuleFrameworkFactory
    {
        return new RuleFrameworkFactory();
    }

    /**
     * Get the image_url.
     *
     * @param ?string $value
     *
     * @return null|string The image_url asset path, or null
     */
    public function getImageUrlAttribute(?string $value): ?string
    {
        return $this->getAssetPath($value, 'image', $this->id);
    }

    /**
     * {@inheritdoc}
     */
    public function getListItem(): array
    {
        return [
            config('app.url') . '/ruleframeworks/' . $this->id => [
                'labels' => [
                    'nl-NL' => $this->title,
                    'en-US' => $this->title,
                ],
            ],
        ];
    }
}
