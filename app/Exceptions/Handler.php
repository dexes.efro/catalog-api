<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use XpertSelect\JsonApi\JsonApiResponseUtilities;

class Handler extends ExceptionHandler
{
    use JsonApiResponseUtilities;

    /**
     * {@inheritdoc}
     */
    protected $dontReport = [
    ];

    /**
     * {@inheritdoc}
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * {@inheritdoc}
     */
    public function register(): void
    {
        $this->renderable(function(ValidationException $e, Request $request) {
            if ($request->is('api/*')) {
                return $this->validationExceptionToJsonApiResponse($e);
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    protected function unauthenticated($request, AuthenticationException $exception): Response
    {
        return $this->shouldReturnJson($request, $exception)
            ? $this->unauthenticatedErrorResponse()
            : parent::unauthenticated($request, $exception);
    }

    /**
     * Return the default validationException as a valid JSON API Error response.
     *
     * @param ValidationException $e The validation exception
     *
     * @return JsonResponse The HTTP Response
     */
    private function validationExceptionToJsonApiResponse(ValidationException $e): JsonResponse
    {
        return $this->badRequestErrorResponse(array_merge(...array_values(array_map(function($messages) {
            return array_map(function($message) {
                return [
                    'title' => $message,
                ];
            }, $messages);
        }, $e->validator->errors()->messages()))));
    }
}
