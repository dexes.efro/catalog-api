<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Exceptions;

use Exception;

/**
 * Class IshareException.
 *
 * Must be thrown when an exception occurs while communicating with an iShare satellite.
 */
class IshareException extends Exception
{
}
