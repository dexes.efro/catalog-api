<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Search;

use Illuminate\Support\Carbon;
use Solarium\QueryType\Update\Query\Document;

/**
 * Class DCATSearchDocument.
 *
 * Exposes functionality for creating search documents from a given DCAT object.
 */
class DCATSearchDocument extends Document
{
    /**
     * Creates a document that can be sent to the search engine from the given DCAT Dataset.
     *
     * @param array<string, mixed> $dataset The Dataset acting as the source
     *
     * @return DCATSearchDocument<mixed> The created search document
     */
    public static function fromDataset(array $dataset): DCATSearchDocument
    {
        $mappingConfig  = config('xs-content-types.dexes-dataset.search.mapping', []);
        $dateFields     = config('xs-content-types.dexes-dataset.search.date-fields', []);
        $keysToIgnore   = self::createKeysToIgnoreList($dataset, $mappingConfig);
        $searchDocument = ['sys_type' => 'dataset'];

        return new DCATSearchDocument(self::mapToSearchDocument(
            $dataset,
            $mappingConfig,
            $dateFields,
            $keysToIgnore,
            $searchDocument
        ));
    }

    /**
     * Creates a document that can be sent to the search engine from the given DCAT DataService.
     *
     * @param array<string, mixed> $dataService The DataService acting as the source
     *
     * @return DCATSearchDocument<mixed> The created search document
     */
    public static function fromDataService(array $dataService): DCATSearchDocument
    {
        $mappingConfig  = config('xs-content-types.dexes-dataservice.search.mapping', []);
        $dateFields     = config('xs-content-types.dexes-dataservice.search.date-fields', []);
        $keysToIgnore   = self::createKeysToIgnoreList($dataService, $mappingConfig);
        $searchDocument = ['sys_type' => 'dataservice'];

        return new DCATSearchDocument(self::mapToSearchDocument(
            $dataService,
            $mappingConfig,
            $dateFields,
            $keysToIgnore,
            $searchDocument
        ));
    }

    /**
     * Maps the fields of the given DCAT structure to the fields that hold that data in the search
     * schema.
     *
     * @param array<string, mixed> $dcat           The DCAT structure, DataService or Dataset
     * @param array<string, mixed> $mappingConfig  The mapping that is used for that DCAT class,
     *                                             which supports dot notation
     * @param array<string, mixed> $dateFields     Which fields should be treated as dates
     * @param array<int, string>   $keysToIgnore   The keys that can be ignored
     * @param array<string, mixed> $searchDocument The base document to expand upon
     *
     * @return array<string, mixed> The DCAT class as a representation that can be indexed in the
     *                              search collection
     */
    private static function mapToSearchDocument(array $dcat, array $mappingConfig,
                                                           array $dateFields, array $keysToIgnore,
                                                           array $searchDocument): array
    {
        foreach ($mappingConfig as $key => $targetKeys) {
            if (in_array($key, $keysToIgnore)) {
                continue;
            }

            $value = data_get($dcat, $key);

            if (!$value) {
                continue;
            }

            foreach ($targetKeys as $targetKey) {
                $targetKey = strval($targetKey);

                if (!array_key_exists($targetKey, $searchDocument)) {
                    $searchDocument[$targetKey] = [];
                }

                if (in_array($targetKey, $dateFields)) {
                    $value = Carbon::parse($value)->format('Y-m-d\TH:i:s\Z');
                }

                $searchDocument[$targetKey] = array_merge(
                    (array) $searchDocument[$targetKey],
                    (array) $value
                );
            }
        }

        return $searchDocument;
    }

    /**
     * Creates a list of fields which should be ignored.
     *
     * @param array<string, mixed>                $item          The target item
     * @param array<string, array<string, mixed>> $mappingConfig The search-mapping data
     *
     * @return array<int, string> The names of the keys that should be ignored
     */
    private static function createKeysToIgnoreList(array &$item, array $mappingConfig): array
    {
        $keysToIgnore = [];

        foreach ($item as $key => $value) {
            if (!array_key_exists($key, $mappingConfig)) {
                continue;
            }

            if (is_bool($value)) {
                if ($value) {
                    $item[$key] = $key;
                } else {
                    $keysToIgnore[] = $key;
                }
            }
        }

        return $keysToIgnore;
    }
}
