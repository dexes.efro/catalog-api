<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services;

use Illuminate\Support\Facades\Log;

/**
 * Trait HasAssets.
 */
trait HasAssets
{
    /**
     * Generate an asset path for the application or return NULL if no value is provided.
     *
     * @param string     $name  the name of the asset
     * @param string|int $id    The identifier of the object
     * @param ?string    $value
     *
     * @return null|string the asset path or null
     */
    public function getAssetPath(?string $value, string $name, string|int $id): ?string
    {
        if (null === $value) {
            Log::warning(sprintf(
                'Expected %s on %s with id %s, but no %s found',
                $name, self::class, $id, $name
            ));

            return null;
        }

        return asset($value);
    }
}
