<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\JWT;

use Firebase\JWT\JWT as FirebaseJWT;
use stdClass;
use UnexpectedValueException;

/**
 * Class JWT.
 */
class JWT
{
    /**
     * Simple JWT Decode function to receive the payload from an encoded JWT string.
     *
     * @param string $jwt The JWT
     *
     * @return stdClass The JWT's payload
     */
    public static function decode(string $jwt): stdClass
    {
        $tks = explode('.', $jwt);

        if (count($tks) !== 3) {
            throw new UnexpectedValueException('Wrong number of segments');
        }

        [$headb64, $bodyb64, $cryptob64] = $tks;
        $headerRaw                       = FirebaseJWT::urlsafeB64Decode($headb64);
        if (null === ($header = FirebaseJWT::jsonDecode($headerRaw))) {
            throw new UnexpectedValueException('Invalid header encoding');
        }

        $payloadRaw = FirebaseJWT::urlsafeB64Decode($bodyb64);
        if (null === ($payload = FirebaseJWT::jsonDecode($payloadRaw))) {
            throw new UnexpectedValueException('Invalid claims encoding');
        }
        if (is_array($payload)) {
            // prevent PHP Fatal Error in edge-cases when payload is empty array
            $payload = (object) $payload;
        }
        if (!$payload instanceof stdClass) {
            throw new UnexpectedValueException('Payload must be a JSON object');
        }

        return $payload;
    }
}
