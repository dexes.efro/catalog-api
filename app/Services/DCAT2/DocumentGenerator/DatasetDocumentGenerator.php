<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\DCAT2\DocumentGenerator;

use XpertSelect\JsonApi\Generator\ResourceDocumentGenerator;

/**
 * Class DatasetDocumentGenerator.
 */
final class DatasetDocumentGenerator extends ResourceDocumentGenerator
{
}
