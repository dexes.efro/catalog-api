<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\DCAT2\Serializers;

use App\Services\ObjectMapper;
use XpertSelect\JsonApi\Relationship;
use XpertSelect\JsonApi\Serializer\BaseSerializer;

/**
 * Class DatasetSerializer.
 *
 * Serializers a Dexes Dataset into JSON API resources
 */
class DatasetSerializer extends BaseSerializer
{
    /**
     * DatasetSerializer constructor.
     *
     * @param ObjectMapper $mapper The object mapper
     */
    public function __construct(private readonly ObjectMapper $mapper)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return 'dataset';
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(array $model): array
    {
        return $this->mapper->map($model, $this->datasetMapping());
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier(array $model): string
    {
        return $model['name'];
    }

    /**
     * {@inheritdoc}
     */
    public function getLinks(array $model): array
    {
        return [
            'self' => $this->getLinkFromModel($model),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMeta(array $model): array
    {
        $metadataFields = [
            'dataspace_uri',
            'consumer_name',
            'consumer_checksum',
            'metadata_language',
            'name',
            'dataset_status',
            'high_value',
            'basis_register',
            'source_catalog',
        ];

        $meta = [];

        foreach ($metadataFields as $field) {
            if (array_key_exists($field, $model)) {
                $meta[$field] = $model[$field];
            }
        }

        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function getRelationships(array $model): array
    {
        $relationships          = [];
        $distributionSerializer = new DistributionSerializer($this->mapper);
        foreach ($model['resources'] ?? [] as $index => $distribution) {
            $distribution['index']        = $index;
            $distribution['package_name'] = $model['name'];
            $relationships[]              = new Relationship($distributionSerializer->fromObject($distribution), true);
        }

        return $relationships;
    }

    /**
     * Retrieve the dataset mapping to pass to the object mapper.
     *
     * @return array<string, string> The mapping to use
     */
    private function datasetMapping(): array
    {
        return [
            'access_rights'                     => 'dct:accessRights',
            'dct:conformsTo'                    => 'dct:conformsTo',
            'contact_point_name'                => 'dcat:contactPoint.vcard:fn',
            'contact_point_email'               => 'dcat:contactPoint.vcard:hasEmail',
            'contact_point.title'               => 'dcat:contactPoint.vcard:hasName',
            'contact_point.address'             => 'dcat:contactPoint.vcard:hasAddress',
            'contact_point.website'             => 'dcat:contactPoint.vcard:hasURL',
            'contact_point.phone'               => 'dcat:contactPoint.vcard:hasTelephone',
            'authority'                         => 'dct:creator',
            'notes'                             => 'dct:description',
            'title'                             => 'dct:title',
            'dct:issued'                        => 'dct:issued',
            'modified'                          => 'dct:modified',
            'language'                          => 'dct:language',
            'publisher'                         => 'dct:publisher',
            'identifier'                        => 'dct:identifier',
            'theme'                             => 'dcat:theme',
            'dct:type'                          => 'dct:type',
            'related_resource'                  => 'dct:relation',
            'dcat:qualifiedRelation'            => 'dcat:qualifiedRelation',
            'tags'                              => 'dcat:keyword',
            'dcat:landingPage'                  => 'dcat:landingPage',
            'prov:qualifiedAttribution'         => 'prov:qualifiedAttribution',
            'license_id'                        => 'dct:license',
            'dct:rights'                        => 'dct:rights',
            'has_policy'                        => 'odrl:hasPolicy',
            'dct:isReferencedBy'                => 'dct:isReferencedBy',
            'frequency'                         => 'dct:accrualPeriodicity',
            'dcat:spatialResolutionInMeters'    => 'dcat:spatialResolutionInMeters',
            'temporal_start'                    => 'dct:temporal.dcat:startDate',
            'temporal_end'                      => 'dct:temporal.dcat:endDate',
            'dct:temporal.time:hasBeginning'    => 'dct:temporal.time:hasBeginning',
            'dct:temporal.time:hasEnd'          => 'dct:temporal.time:hasEnd',
            'dcat:temporalResolution'           => 'dcat:temporalResolution',
            'prov:wasGeneratedBy'               => 'prov:wasGeneratedBy',
        ];
    }

    /**
     * Generate a link for the dataset.
     *
     * @param array<string, mixed> $model The dataset
     *
     * @return string The link
     */
    private function getLinkFromModel(array $model): string
    {
        $idOrName = array_key_exists('name', $model) ? $model['name'] : $model['identifier'];

        return route('api.dcat2.datasets.show', ['id' => $idOrName]);
    }
}
