<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\DCAT2\Serializers;

use App\Services\ObjectMapper;
use XpertSelect\JsonApi\Serializer\BaseSerializer;

/**
 * Class DataServiceSerializer.
 *
 * Serializers a Dexes DataService into JSON API resources
 */
class DataServiceSerializer extends BaseSerializer
{
    /**
     * DataServiceSerializer constructor.
     *
     * @param ObjectMapper $mapper The object mapper
     */
    public function __construct(private readonly ObjectMapper $mapper)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return 'dataservice';
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(array $model): array
    {
        return $this->mapper->map($model, $this->dataServiceMapping());
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier(array $model): string
    {
        return $model['name'];
    }

    /**
     * {@inheritdoc}
     */
    public function getLinks(array $model): array
    {
        return [
            'self' => $this->getLinkFromModel($model),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMeta(array $model): array
    {
        $metadataFields = [
            'dataspace_uri',
            'consumer_name',
            'consumer_checksum',
            'name',
        ];

        $meta = [];

        foreach ($metadataFields as $field) {
            if (array_key_exists($field, $model)) {
                $meta[$field] = $model[$field];
            }
        }

        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function getRelationships(array $model): array
    {
        return [];
    }

    /**
     * Retrieve the dataservice mapping to pass to the object mapper.
     *
     * @return array<string, string> The mapping to use
     */
    private function dataServiceMapping(): array
    {
        return [
            'access_rights'               => 'dct:accessRights',
            'conforms_to'                 => 'dct:conformsTo',
            'contact_point.name'          => 'dcat:contactPoint.vcard:fn',
            'contact_point.title'         => 'dcat:contactPoint.vcard:hasName',
            'contact_point.address'       => 'dcat:contactPoint.vcard:hasAddress',
            'contact_point.email'         => 'dcat:contactPoint.vcard:hasEmail',
            'contact_point.website'       => 'dcat:contactPoint.vcard:hasURL',
            'contact_point.phone'         => 'dcat:contactPoint.vcard:hasTelephone',
            'creator'                     => 'dct:creator',
            'description'                 => 'dct:description',
            'title'                       => 'dct:title',
            'release_date'                => 'dct:issued',
            'modification_date'           => 'dct:modified',
            'resource_language'           => 'dct:language',
            'publisher'                   => 'dct:publisher',
            'identifier'                  => 'dct:identifier',
            'theme'                       => 'dcat:theme',
            'type'                        => 'dct:type',
            'relation'                    => 'dct:relation',
            'qualified_relation'          => 'dcat:qualifiedRelation',
            'keyword'                     => 'dcat:keyword',
            'landing_page'                => 'dcat:landingPage',
            'qualified_attribution'       => 'prov:qualifiedAttribution',
            'license'                     => 'dct:license',
            'rights'                      => 'dct:rights',
            'has_policy'                  => 'odrl:hasPolicy',
            'is_referenced_by'            => 'dct:isReferencedBy',
            'endpoint_url'                => 'dcat:endpointURL',
            'endpoint_description'        => 'dcat:endpointDescription',
            'serves_dataset'              => 'dcat:servesDataset',
        ];
    }

    /**
     * Generate a link for the data service.
     *
     * @param array<string, mixed> $model The data service
     *
     * @return string The link
     */
    private function getLinkFromModel(array $model): string
    {
        $idOrName = array_key_exists('name', $model) ? $model['name'] : $model['identifier'];

        return route('api.dcat2.dataservices.show', ['id' => $idOrName]);
    }
}
