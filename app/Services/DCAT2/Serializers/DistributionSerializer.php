<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\DCAT2\Serializers;

use App\Services\ObjectMapper;
use XpertSelect\JsonApi\Serializer\BaseSerializer;

/**
 * Class DistributionSerializer.
 *
 * This transforms a dexes distribution to a DCAT2 JSON API Resource
 */
class DistributionSerializer extends BaseSerializer
{
    /**
     * DistributionSerializer constructor.
     *
     * @param ObjectMapper $mapper The object mapping instance to use
     */
    public function __construct(private readonly ObjectMapper $mapper)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return 'distribution';
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(array $model): array
    {
        return $this->mapper->map($model, $this->distributionMapping());
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier(array $model): string
    {
        return $model['id'];
    }

    /**
     * {@inheritdoc}
     */
    public function getLinks(array $model): array
    {
        return [
            'self' => sprintf('%s/api/dcat2/datasets/%s/distributions/%s',
                config('app.url'),
                $model['package_name'],
                $model['id']
            ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMeta(array $model): array
    {
        $metadataFields = [
            'metadata_language',
            'name',
        ];

        $meta = [];

        foreach ($metadataFields as $field) {
            if (array_key_exists($field, $model)) {
                $meta[$field] = $model[$field];
            }
        }

        return $meta;
    }

    /**
     * Retrieve the distribution mappings to pass to the object mapper.
     *
     * @return array<string, string> The mappings to use
     */
    private function distributionMapping(): array
    {
        return [
            'id'                             => 'id',
            'url'                            => 'dcat:accessURL',
            'download_url'                   => 'dcat:downloadURL',
            'documentation'                  => 'foaf:page',
            'linked_schemas'                 => 'dct:conformsTo',
            'name'                           => 'dct:title',
            'description'                    => 'dct:description',
            'status'                         => 'adms:status',
            'language'                       => 'dct:language',
            'license_id'                     => 'dct:license',
            'rights'                         => 'dct:rights',
            'format'                         => 'dct:format',
            'media_type'                     => 'dcat:mediaType',
            'size'                           => 'dcat:byteSize',
            'hash'                           => 'spdx:checksum.spdx:checksumValue',
            'hash_algorithm'                 => 'spdx:checksum.spdx:algorithm',
            'release_date'                   => 'dct:issued',
            'modification_date'              => 'dct:modified',
            'distribution_type'              => 'donl:distributionType',
            'has_policy'                     => 'odrl:hasPolicy',
            'dct:accessRights'               => 'dct:accessRights',
            'dcat:accessService'             => 'dcat:accessService',
            'dcat:spatialResolutionInMeters' => 'dcat:spatialResolutionInMeters',
            'dcat:temporalResolution'        => 'dcat:temporalResolution',
            'dcat:compressFormat'            => 'dcat:compressFormat',
            'dcat:packageFormat'             => 'dcat:packageFormat',
        ];
    }
}
