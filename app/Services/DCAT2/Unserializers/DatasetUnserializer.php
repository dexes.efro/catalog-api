<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\DCAT2\Unserializers;

use App\Services\ObjectMapper;

/**
 * Class DatasetUnserializer.
 *
 * This transforms a JSON API DCAT 2 Dataset to a dexes dataset.
 */
class DatasetUnserializer
{
    /**
     * DatasetUnserializer constructor.
     *
     * @param ObjectMapper $mapper The object mapper
     */
    public function __construct(private readonly ObjectMapper $mapper)
    {
    }

    /**
     * Transform a JSON API DCAT 2 Dataset to a dexes dataset.
     *
     * @param array<string, mixed> $model The JSON API DCAT 2 Dataset
     *
     * @return array<string, mixed> The dexes-dataset
     */
    public function unserialize(array $model): array
    {
        return $this->mapper->map($model, $this->mapping());
    }

    /**
     * Retrieve the dataset mapping to pass to the object mapper.
     *
     * @return array<string, string> The mapping to use
     */
    private function mapping(): array
    {
        return [
            'attributes.dct:accessRights'                     => 'access_rights',
            'attributes.dct:conformsTo'                       => 'dct:conformsTo',
            'attributes.dcat:contactPoint.vcard:fn'           => 'contact_point_name',
            'attributes.dcat:contactPoint.vcard:hasEmail'     => 'contact_point_email',
            'attributes.dcat:contactPoint.vcard:hasName'      => 'contact_point_title',
            'attributes.dcat:contactPoint.vcard:hasAddress'   => 'contact_point_address',
            'attributes.dcat:contactPoint.vcard:hasURL'       => 'contact_point_website',
            'attributes.dcat:contactPoint.vcard:hasTelephone' => 'contact_point_phone',
            'attributes.dct:creator'                          => 'authority',
            'attributes.dct:description'                      => 'notes',
            'attributes.dct:title'                            => 'title',
            'attributes.dct:issued'                           => 'dct:issued',
            'attributes.dct:modified'                         => 'modified',
            'attributes.dct:language'                         => 'language',
            'attributes.dct:publisher'                        => 'publisher',
            'attributes.dct:identifier'                       => 'identifier',
            'attributes.dcat:theme'                           => 'theme',
            'attributes.dct:type'                             => 'dct:type',
            'attributes.dct:relation'                         => 'related_resource',
            'attributes.dcat:qualifiedRelation'               => 'dcat:qualifiedRelation',
            'attributes.dcat:keyword'                         => 'tags',
            'attributes.dcat:landingPage'                     => 'dcat:landingPage',
            'attributes.prov:qualifiedAttribution'            => 'prov:qualifiedAttribution',
            'attributes.dct:license'                          => 'license_id',
            'attributes.dct:rights'                           => 'dct:rights',
            'attributes.odrl:hasPolicy'                       => 'has_policy',
            'attributes.dct:isReferencedBy'                   => 'dct:isReferencedBy',
            'attributes.dct:accrualPeriodicity'               => 'frequency',
            'attribtes.dct:spatial.locn:geometry'             => 'dct:spatial.locn:geometry',
            'attributes.dct:spatial.dcat:bbox'                => 'dct:spatial.dcat:bbox',
            'attributes.dct:spatial.dcat:centroid'            => 'dct:spatial.dcat:centroid',
            'attributes.dcat:spatialResolutionInMeters'       => 'dcat:spatialResolutionInMeters',
            'attributes.dct:temporal.dcat:startDate'          => 'temporal_start',
            'attributes.dct:temporal.dcat:endDate'            => 'temporal_end',
            'attributes.dct:temporal.time:hasBeginning'       => 'dct:temporal.time:hasBeginning',
            'attributes.dct:temporal.time:hasEnd'             => 'dct:temporal.time:hasEnd',
            'attributes.dcat:temporalResolution'              => 'dcat:temporalResolution',
            'attributes.prov:wasGeneratedBy'                  => 'prov:wasGeneratedBy',
            'meta.consumer_name'                              => 'consumer_name',
            'meta.consumer_checksum'                          => 'consumer_checksum',
            'meta.dataspace_uri'                              => 'dataspace_uri',
            'meta.metadata_language'                          => 'metadata_language',
            'meta.name'                                       => 'name',
            'meta.dataset_status'                             => 'dataset_status',
            'meta.high_value'                                 => 'high_value',
            'meta.basis_register'                             => 'basis_register',
            'meta.source_catalog'                             => 'source_catalog',
        ];
    }
}
