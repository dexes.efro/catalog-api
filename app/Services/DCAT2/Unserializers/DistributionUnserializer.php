<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\DCAT2\Unserializers;

use App\Services\ObjectMapper;

/**
 * Class DistributionUnserializer.
 *
 * This transforms a DCAT 2 Distribution to a dexes distribution.
 */
class DistributionUnserializer
{
    /**
     * DistributionUnserializer constuctor.
     *
     * @param ObjectMapper $mapper The object mapper
     */
    public function __construct(private readonly ObjectMapper $mapper)
    {
    }

    /**
     * Transform a JSON API DCAT 2 Distribution to a dexes distribution.
     *
     * @param array<string, mixed> $model The JSON API DCAT 2 Distribution
     *
     * @return array<string, mixed> The dexes-distribution
     */
    public function unserialize(array $model): array
    {
        return $this->mapper->map($model, $this->mapping());
    }

    /**
     * This returns a mapping for transforming DCAT 2 distributions to dexes distributions.
     *
     * @return array<string, string> The mapping
     */
    public function mapping(): array
    {
        return [
            'attributes.id'                              => 'id',
            'attributes.dcat:accessURL'                  => 'url',
            'attributes.dcat:downloadURL'                => 'download_url',
            'attributes.foaf:page'                       => 'documentation',
            'attributes.dct:conformsTo'                  => 'linked_schemas',
            'attributes.dct:title'                       => 'name',
            'attributes.dct:description'                 => 'description',
            'attributes.adms:status'                     => 'status',
            'attributes.dct:language'                    => 'language',
            'attributes.dct:license'                     => 'license_id',
            'attributes.dct:rights'                      => 'rights',
            'attributes.dct:format'                      => 'format',
            'attributes.dcat:mediaType'                  => 'media_type',
            'attributes.dcat:byteSize'                   => 'size',
            'attributes.spdx:checksum.spd:checksumValue' => 'hash',
            'attributes.spdx:checksum.spd:algorithm'     => 'hash_algorithm',
            'attributes.dct:issued'                      => 'release_date',
            'attributes.dct:modified'                    => 'modification_date',
            'attributes.donl:distributionType'           => 'distribution_type',
            'attributes.odrl:hasPolicy'                  => 'has_policy',
            'attributes.dct:accessRights'                => 'dct:accessRights',
            'attributes.dcat:accessService'              => 'dcat:accessService',
            'attributes.dcat:spatialResolutionInMeters'  => 'dcat:spatialResolutionInMeters',
            'attributes.dcat:temporalResolution'         => 'dcat:temporalResolution',
            'attributes.dcat:compressFormat'             => 'dcat:compressFormat',
            'attributes.dcat:packageFormat'              => 'dcat:packageFormat',
        ];
    }
}
