<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\DCAT2\Unserializers;

use App\Services\ObjectMapper;

/**
 * Class DataServiceUnserializer.
 *
 * This transforms a JSON API DCAT 2 DataService to a dexes dataservice.
 */
class DataServiceUnserializer
{
    /**
     * DataServiceUnserializer constructor.
     *
     * @param ObjectMapper $mapper The object mapper
     */
    public function __construct(private readonly ObjectMapper $mapper)
    {
    }

    /**
     * Transform a JSON API DCAT 2 DataService to a dexes data service.
     *
     * @param array<string, mixed> $model The JSON API DCAT 2 DataService
     *
     * @return array<string, mixed> The dexes-dataservice
     */
    public function unserialize(array $model): array
    {
        return $this->mapper->map($model, $this->mapping());
    }

    /**
     * @return string[]
     */
    private function mapping(): array
    {
        return [
            'attributes.dct:accessRights'                     => 'access_rights',
            'attributes.dct:conformsTo'                       => 'conforms_to',
            'attributes.dcat:contactPoint.vcard:fn'           => 'contact_point.name',
            'attributes.dcat:contactPoint.vcard:hasName'      => 'contact_point.title',
            'attributes.dcat:contactPoint.vcard:hasAddress'   => 'contact_point.address',
            'attributes.dcat:contactPoint.vcard:hasEmail'     => 'contact_point.email',
            'attributes.dcat:contactPoint.vcard:hasURL'       => 'contact_point.website',
            'attributes.dcat:contactPoint.vcard:hasTelephone' => 'contact_point.phone',
            'attributes.dct:creator'                          => 'creator',
            'attributes.dct:description'                      => 'description',
            'attributes.dct:title'                            => 'title',
            'attributes.dct:issued'                           => 'release_date',
            'attributes.dct:modified'                         => 'modification_date',
            'attributes.dct:language'                         => 'resource_language',
            'attributes.dct:publisher'                        => 'publisher',
            'attributes.dct:identifier'                       => 'identifier',
            'attributes.dcat:theme'                           => 'theme',
            'attributes.dct:type'                             => 'type',
            'attributes.dct:relation'                         => 'relation',
            'attributes.dcat:qualifiedRelation'               => 'qualified_relation',
            'attributes.dcat:keyword'                         => 'keyword',
            'attributes.dcat:landingPage'                     => 'landing_page',
            'attributes.prov:qualifiedAttribution'            => 'qualified_attribution',
            'attributes.dct:license'                          => 'license',
            'attributes.dct:rights'                           => 'rights',
            'attributes.odrl:hasPolicy'                       => 'has_policy',
            'attributes.dct:isReferencedBy'                   => 'is_referenced_by',
            'attributes.dcat:endpointURL'                     => 'endpoint_url',
            'attributes.dcat:endpointDescription'             => 'endpoint_description',
            'attributes.dcat:servesDataset'                   => 'serves_dataset',
            'attributes.identifier'                           => 'identifier',
            'meta.consumer_name'                              => 'consumer_name',
            'meta.consumer_checksum'                          => 'consumer_checksum',
            'meta.dataspace_uri'                              => 'dataspace_uri',
            'meta.name'                                       => 'name',
        ];
    }
}
