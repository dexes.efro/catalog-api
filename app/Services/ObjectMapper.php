<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services;

use Illuminate\Support\Arr;

/**
 * Class ObjectMapper.
 *
 * Maps source arrays into new arrays based on provided mappings.
 */
final class ObjectMapper
{
    /**
     * Gets a value of a given array using dot notation.
     *
     * @param string               $key   The key using dot notation
     * @param array<string, mixed> $array The array to get a value from
     *
     * @return mixed The value if it exists else null
     */
    public static function getWithDotNotation(string $key, array $array): mixed
    {
        $key = explode('.', $key, 2);

        if (!array_key_exists($key[0], $array)) {
            return null;
        }

        if (count($key) > 1) {
            if (!is_array($array[$key[0]])) {
                return null;
            }

            return self::getWithDotNotation($key[1], $array[$key[0]]);
        }

        return $array[$key[0]] ?? null;
    }

    /**
     * Sets a value using dot notation and return the resulting array.
     *
     * @param string               $key   The key using dot notation
     * @param mixed                $value The value to set
     * @param array<string, mixed> $array The array to set the value in
     *
     * @return array<string, mixed> The resulting array
     */
    public static function setWithDotNotation(string $key, mixed $value, array $array): array
    {
        $key = explode('.', $key, 2);

        if (count($key) > 1) {
            if (!array_key_exists($key[0], $array)) {
                $array[$key[0]] = [];
            } elseif (!is_array($array[$key[0]])) {
                return $array;
            }

            $array[$key[0]] = self::setWithDotNotation($key[1], $value, $array[$key[0]]);
        } else {
            $array[$key[0]] = $value;
        }

        return $array;
    }

    /**
     * Maps a given array based on the given mappings.
     *
     * @param array<string, mixed>  $object  The array to map
     * @param array<string, string> $mapping The mapping to apply
     *
     * @return array<string, mixed> The mapped array
     */
    public function map(array $object, array $mapping): array
    {
        $result = [];

        foreach ($mapping as $oldKey => $newKey) {
            if (null === $value = self::getWithDotNotation($oldKey, $object)) {
                // only skip if the oldKey didn't exist in the object
                if (!Arr::has($object, $oldKey)) {
                    continue;
                }
            }

            $result = self::setWithDotNotation($newKey, $value, $result);
        }

        return $result;
    }
}
