<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Lists;

/**
 * Interface ListItemInterface.
 */
interface ListItemInterface
{
    /**
     * Return the item in a format that can be used for a xs-list.
     *
     * @return array<string, array<string, array<string, string>>>
     */
    public function getListItem(): array;
}
