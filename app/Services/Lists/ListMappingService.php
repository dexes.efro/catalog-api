<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Lists;

use Illuminate\Support\Collection;

/**
 * Trait ListMappingService.
 */
trait ListMappingService
{
    /**
     * Map a collection of objects to an array of ListItems. If an object in the objects collection
     * doesn't implement the ListItemInterface it will be skipped.
     *
     * @param Collection<ListItemInterface> $objects The collection to map
     *
     * @return array<string, mixed> The mapped array
     */
    public function map(Collection $objects): array
    {
        return $objects->map(function($object) {
            if ($object instanceof ListItemInterface) {
                return $object->getListItem();
            }

            return false;
        })->filter(function($object) {
            return $object;
        })->collapse()->toArray();
    }
}
