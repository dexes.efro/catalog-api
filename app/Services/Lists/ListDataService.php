<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Lists;

use App\Repositories\DataspaceRepository;
use App\Repositories\RuleFrameworkRepository;
use App\Services\Lists\ListClient\ListClient;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * Class ListDataService.
 *
 * Laravel service for aggregating list data.
 */
final class ListDataService
{
    use ListMappingService;

    public const ORGANIZATION_LIST_NAME = 'DONL:Organization';

    public const ORGANIZATION_EXPAND_LIST_NAME = 'EXTRA:Organizations';

    public const LICENSE_LIST_NAME = 'DONL:License';

    public const LICENSE_EXPAND_LIST_NAME = 'EXTRA:License';

    public const POLICIES_LIST_NAME = 'DEXES:Policies';

    public const CATALOGS_LIST_NAME = 'DONL:Catalogs';

    public const CATALOGS_EXPAND_LIST_NAME = 'EXTRA:Catalogs';

    /**
     * ListDataService Constructor.
     *
     * @param ListClient              $listClient              The list Client
     * @param DataspaceRepository     $dataspaceRepository     The Dataspace repository
     * @param RuleFrameworkRepository $ruleFrameworkRepository The RuleFramework repository
     */
    public function __construct(private readonly ListClient $listClient,
                                private readonly DataspaceRepository $dataspaceRepository,
                                private readonly RuleFrameworkRepository $ruleFrameworkRepository)
    {
    }

    /**
     * Get the expanded Catalog list.
     *
     * @return array<string, mixed>
     */
    public function catalogsList(): array
    {
        return $this->mergeLists([self::CATALOGS_LIST_NAME, 'DEXES:Dataspaces']);
    }

    /**
     * Get the expanded Organization list.
     *
     * @return array<string, mixed>
     */
    public function organizationsList(): array
    {
        return $this->mergeLists([self::ORGANIZATION_LIST_NAME, self::ORGANIZATION_EXPAND_LIST_NAME]);
    }

    /**
     * Get the expanded license list.
     *
     * @return array<string, mixed>
     */
    public function licensesList(): array
    {
        return $this->mergeLists([self::LICENSE_LIST_NAME, self::LICENSE_EXPAND_LIST_NAME]);
    }

    /**
     * Get the policies list.
     *
     * @return array<string, mixed>
     */
    public function policiesList(): array
    {
        return $this->listClient->list(self::POLICIES_LIST_NAME)->getData();
    }

    /**
     * Get the ruleframework list.
     *
     * @throws StorageRecordStorageException
     *
     * @return array<string, mixed>
     */
    public function ruleFrameworksList(): array
    {
        $ruleframeworks = $this->ruleFrameworkRepository->all();

        return $this->map($ruleframeworks);
    }

    /**
     * Get the dataspaces list.
     *
     * @throws StorageRecordStorageException
     *
     * @return array<string, mixed>
     */
    public function dataspacesList(): array
    {
        $dataspaces = $this->dataspaceRepository->all();

        return $this->map($dataspaces);
    }

    /**
     * Merge lists together.
     *
     * @param array<int, string> $lists An array with names of lists to merge together
     *
     * @return array<string, array<string, array<string, string>>> The merged list
     */
    private function mergeLists(array $lists): array
    {
        $mergedLists = [];

        foreach ($lists as $name) {
            $mergedLists = array_merge($mergedLists, $this->listClient->list($name)->getData());
        }

        return $mergedLists;
    }
}
