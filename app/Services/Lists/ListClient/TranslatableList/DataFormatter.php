<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Lists\ListClient\TranslatableList;

use RuntimeException;

/**
 * Class DataFormatter.
 *
 * Basic implementation of the DataFormatterInterface.
 */
final class DataFormatter
{
    /**
     * In-memory cache of formats.
     *
     * @var array<string, mixed>
     */
    private static array $CACHE = [
        'flat'    => [],
        'sorted'  => [],
        'grouped' => [],
        'nested'  => [],
    ];

    /**
     * Flatten the contents of TranslatableListInterface::getData() such that it becomes an
     * {identifier} => {display label} array.
     *
     * @param TranslatableList $list The list to flatten its contents of
     *
     * @return array<string, string> The {identifier} => {display label} list data
     */
    public function flatten(TranslatableList $list): array
    {
        $md5       = $this->createHashForData($list->getData());
        $flattened = [];
        $store     = 'flat';

        if ($this->inCacheStore($store, $md5)) {
            return self::$CACHE[$store][$md5];
        }

        foreach ($list->getData() as $identifier => $contents) {
            if (!array_key_exists('labels', $contents)) {
                continue;
            }

            if (!array_key_exists($list->getLanguageCode(), $contents['labels'])) {
                continue;
            }

            $flattened[$identifier] = $contents['labels'][$list->getLanguageCode()];
        }

        self::$CACHE[$store][$md5] = $flattened;

        return $flattened;
    }

    /**
     * Sort the output of TranslatableListInterface::getData() alphabetically.
     *
     * @param TranslatableList $list The list to sort its contents of
     *
     * @return array<string, mixed> The sorted list contents
     */
    public function sorted(TranslatableList $list): array
    {
        $data     = $list->getData();
        $language = $list->getLanguageCode();
        $md5      = $this->createHashForData($data);
        $store    = 'sorted';

        if ($this->inCacheStore($store, $md5)) {
            return self::$CACHE[$store][$md5];
        }

        uasort($data, function($a, $b) use ($language) {
            foreach ([$a, $b] as $element) {
                if (!array_key_exists('labels', $element)) {
                    return 0;
                }

                if (!array_key_exists($language, $element['labels'])) {
                    return 0;
                }
            }

            return strcmp($a['labels'][$language], $b['labels'][$language]);
        });

        self::$CACHE[$store][$md5] = $data;

        return $data;
    }

    /**
     * Group the output of TranslatableListInterface::getData() based on the field present in the
     * items of the list.
     *
     * @param TranslatableList $list       The list to group its contents of
     * @param string           $groupField The field to group by
     *
     * @return array<string, mixed> The grouped list contents
     */
    public function grouped(TranslatableList $list, string $groupField): array
    {
        $data  = $list->getData();
        $md5   = $this->createHashForData($data);
        $store = 'grouped';

        if ($this->inCacheStore($store, $md5)) {
            return self::$CACHE[$store][$md5];
        }

        $groupedData = [];

        foreach ($data as $item_id => $value) {
            if (!array_key_exists($groupField, $value)) {
                continue;
            }

            if (null === $value[$groupField]) {
                $value[$groupField] = 'null';
            }

            if (!array_key_exists($value[$groupField], $groupedData)) {
                $groupedData[strval($value[$groupField])] = [];
            }

            $groupedData[strval($value[$groupField])][$item_id] = $value;
        }

        self::$CACHE[$store][$md5] = $groupedData;

        return $groupedData;
    }

    /**
     * Nest the output of TranslatableListInterface::getData() based on a field  representing the
     * parent element of an item in the list.
     *
     * @param TranslatableList $list        The list to nest its contents of
     * @param string           $parentField The field containing the reference to the
     *                                      parent field
     * @param string           $childField  The field containing the nested item
     * @param bool             $sorted      Whether to also sort the list contents
     *                                      alphabetically
     *
     * @return array<string, mixed> The nested list contents
     */
    public function nested(TranslatableList $list, string $parentField = 'parent',
                           string $childField = 'children', bool $sorted = false): array
    {
        $data  = $sorted ? $this->sorted($list) : $list->getData();
        $md5   = $this->createHashForData($data);
        $store = 'nested';

        if ($this->inCacheStore($store, $md5)) {
            return self::$CACHE[$store][$md5];
        }

        foreach ($data as $item_id => $value) {
            if (!array_key_exists($parentField, $value)) {
                continue;
            }

            if (null === $value[$parentField]) {
                continue;
            }

            if (!array_key_exists($value[$parentField], $data)) {
                continue;
            }

            if (!array_key_exists($childField, $data[$value[$parentField]])) {
                $data[strval($value[$parentField])][$childField] = [];
            }

            $data[strval($value[$parentField])][$childField][$item_id] = &$data[$item_id];
        }

        $output = array_filter($data, function($value) use ($parentField) {
            return null === $value[$parentField];
        });

        self::$CACHE[$store][$md5] = $output;

        return $output;
    }

    /**
     * Creates a MD5 hash of a given array of data.
     *
     * @param array<int|string, mixed> $data The data to generate the hash for
     *
     * @return string The MD5 hash of the array
     */
    private function createHashForData(array $data): string
    {
        $json = json_encode($data);

        if (JSON_ERROR_NONE !== json_last_error() || false === $json) {
            throw new RuntimeException('Failed to JSON encode list data');
        }

        return md5($json);
    }

    /**
     * Checks if a given checksum is present in the in-memory cache store.
     *
     * @param string $store    The name of the cache store
     * @param string $checksum The checksum to check
     *
     * @return bool Whether the checksum is present in the store
     */
    private function inCacheStore(string $store, string $checksum): bool
    {
        if (!array_key_exists($store, self::$CACHE)) {
            return false;
        }

        return array_key_exists($checksum, self::$CACHE[$store]);
    }
}
