<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Lists\ListClient\TranslatableList;

/**
 * Class TranslatableList.
 *
 * Primary implementation of the TranslatableListInterface interface.
 */
final class TranslatableList
{
    /**
     * TranslatableListInterface constructor.
     *
     * @param string                                 $name     The name of the list
     * @param array<string, mixed>                   $data     The raw data of the list
     * @param string                                 $language The ISO 649-1 language code used for translations
     * @param string|array<int, class-string|string> $source   The source of this list's data
     * @param string[]                               $aliases  The name aliases of this list
     */
    public function __construct(private readonly string $name, private readonly array $data,
                                private readonly string $language, private readonly string|array $source,
                                private readonly array $aliases = [])
    {
    }

    /**
     * Return the ISO 639-1 language code that will be used for `identifier` to `label`
     * translations.
     *
     * @return string The ISO 639-1 language code
     */
    public function getLanguageCode(): string
    {
        return $this->language;
    }

    /**
     * Return the name of this list.
     *
     * @return string The name of the list
     */
    public function getTitle(): string
    {
        return $this->name;
    }

    /**
     * Return the source of this list's data.
     *
     * @return string The source of the data
     */
    public function getSource(): string
    {
        if (is_array($this->source)) {
            return $this->source[1];
        }

        return $this->source;
    }

    /**
     * Return all the name aliases for this list.
     *
     * @return string[] The list aliases
     */
    public function getAliases(): array
    {
        return $this->aliases;
    }

    /**
     * Return the raw data of this list.
     *
     * @return array<string, mixed> The list data
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Return the size of the data.
     *
     * @return int The size of the data
     */
    public function size(): int
    {
        return count($this->data);
    }

    /**
     * Translates a given label to its matching identifier if such a translation can be made.
     * Otherwise, the original label will be returned.
     *
     * @param string $label The label to translate to an identifier
     *
     * @return string the possibly translated label
     */
    public function labelToIdentifier(string $label): string
    {
        foreach ($this->data as $identifier => $contents) {
            if (!array_key_exists('labels', $contents)) {
                continue;
            }

            if (!array_key_exists($this->language, $contents['labels'])) {
                continue;
            }

            if ($contents['labels'][$this->language] === $label) {
                return $identifier;
            }
        }

        return $label;
    }

    /**
     * Translates a given identifier to its matching display label based on the configured ISO 639-1
     * language code. If no translation could be made the original identifier will be returned.
     *
     * @param string $identifier The identifier to translate to a display label
     *
     * @return string The possibly translated identifier
     */
    public function identifierToLabel(string $identifier): string
    {
        if (array_key_exists($identifier, $this->data)) {
            $item = $this->data[$identifier];

            if (!array_key_exists('labels', $item)) {
                return $identifier;
            }

            if (array_key_exists($this->language, $item['labels'])) {
                return $item['labels'][$this->language];
            }
        }

        return $identifier;
    }

    /**
     * Checks if an entry identified by the given identifier has a given field and
     * a non-empty value for that field.
     *
     * @param string $identifier The identifier to translate to a display label
     * @param string $field      The field to check for
     *
     * @return bool Whether the given entry has a non-empty field
     */
    public function entryHasField(string $identifier, string $field): bool
    {
        if (!array_key_exists($identifier, $this->data)) {
            return false;
        }

        $entry = $this->data[$identifier];

        if (!array_key_exists($field, $entry)) {
            return false;
        }

        return !empty($entry[$field]);
    }
}
