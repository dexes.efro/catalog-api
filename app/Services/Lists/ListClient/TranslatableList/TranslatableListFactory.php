<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Lists\ListClient\TranslatableList;

use RuntimeException;

/**
 * Class TranslatableListFactory.
 *
 * Main implementation of the TranslatableListFactoryInterface.
 */
final class TranslatableListFactory
{
    /**
     * A `key => value` array where key is the Drupal language code and value equals the equivalent
     * ISO 639-1 language.
     *
     * @var array<string, string>
     */
    public const LANGUAGE_MAP = [
        'nl' => 'nl-NL',
        'en' => 'en-US',
    ];

    /**
     * The in-memory cache of the created instances.
     *
     * @var array<string, TranslatableList>
     */
    private array $memoryCache;

    /**
     * TranslatableListFactory constructor.
     *
     * @param string     $languageId The ISO 639-1 language code of the active language
     * @param DataLoader $dataLoader The DataLoaderInterface implementation for loading the
     *                               list data
     */
    public function __construct(private readonly string $languageId,
                                private readonly DataLoader $dataLoader)
    {
        $this->memoryCache = [];
    }

    /**
     * Creates a dummy list that doesn't hold any translations.
     *
     * @param string   $name  The name of the list
     * @param string[] $alias The aliases of the name
     *
     * @return TranslatableList The dummy list
     */
    public function dummy(string $name, array $alias = []): TranslatableList
    {
        return new TranslatableList($name, [], $this->languageId, '', $alias);
    }

    /**
     *  Create a TranslatableListInterface implementation based on the given data.
     *  Created instances will be cached in memory for the duration of the request and subsequent
     *  requests for the same list will return the same instance.
     *
     * @param string                                 $name   The name of the list
     * @param string|array<int, class-string|string> $source The source containing the data of the list
     * @param string[]                               $alias  The aliases of the name
     *
     * @return TranslatableList The created list
     */
    public function createTranslatableList(string $name, string|array $source,
                                           array $alias): TranslatableList
    {
        if (!empty($this->memoryCache[$name])) {
            return $this->memoryCache[$name];
        }

        $listData = '/dev/null' !== $source
            ? $this->dataLoader->loadData($source)
            : [];

        $list = new TranslatableList($name, $listData, $this->languageId, $source, $alias);

        $this->memoryCache[$name] = $list;

        foreach ($list->getAliases() as $alias) {
            if (array_key_exists($alias, $this->memoryCache)) {
                throw new RuntimeException(
                    'List alias conflicts with name or alias from another list'
                );
            }

            $this->memoryCache[$alias] = $list;
        }

        return $this->memoryCache[$name];
    }
}
