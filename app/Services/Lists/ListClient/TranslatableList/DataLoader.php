<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Lists\ListClient\TranslatableList;

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use RuntimeException;

/**
 * Class DataLoader.
 *
 * Basic implementation of the DataLoaderInterface allowing to load data from files and URLs.
 */
final class DataLoader
{
    /**
     * The cache duration in seconds for how long the contents of a list should be stored in the
     * cache.
     */
    public const CACHE_DURATION = 60 * 60;

    /**
     * DataLoader constructor.
     *
     * @param ClientInterface $client The ClientInterface to use for online requests
     */
    public function __construct(private readonly ClientInterface $client)
    {
    }

    /**
     * Load the data from the given source.
     *
     * @param string|array<int, class-string|string> $source The source containing the data, may be
     *                                                       a filepath, a URL, a function or an
     *                                                       array with <class, method>
     *
     * @return array<string, mixed> The loaded data
     */
    public function loadData(string|array $source): array
    {
        $key = is_array($source) ? $source[1] : $source;
        $cid = 'xs_lists.list.' . $key;

        // @phpstan-ignore-next-line
        if ($cached = cache($cid)) {
            return $cached;
        }

        if ($this->canBeCalled($source)) {
            $data = $this->loadFromCallable($source);
        } elseif ($this->isFile($source)) {
            $data = $this->loadJSONFile($source);
        } elseif ($this->isUri($source)) {
            $data = $this->loadJSONURL($source);
        } else {
            throw new RuntimeException('');
        }

        if (!empty($data)) {
            // @phpstan-ignore-next-line
            cache([$cid => $data], time() + self::CACHE_DURATION);
        }

        return $data;
    }

    /**
     * Loads the JSON contents of the given file.
     *
     * @param array<int, string|class-string>|string $file The file to load
     *
     * @return array<string, mixed> The JSON contents of the file, or an empty array on any error
     */
    private function loadJSONFile(array|string $file): array
    {
        if (!is_string($file)) {
            Log::error('File must be a string.');

            return [];
        }

        if (!is_readable($file)) {
            Log::error('Cannot read file ' . $file);

            return [];
        }

        $fileContents = file_get_contents($file);

        if (false === $fileContents) {
            Log::error('Failed to read file ' . $file);

            return [];
        }

        $json = json_decode($fileContents, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            Log::error('JSON decoding error for file' . $file);

            return [];
        }

        return $json;
    }

    /**
     * Loads the JSON contents behind a given URL.
     *
     * @param array<int, string|class-string>|string $url The URL to load
     *
     * @return array<string, mixed> The JSON contents behind the URL, or an empty array on any error
     */
    private function loadJSONURL(array|string $url): array
    {
        if (!is_string($url)) {
            Log::error('Url must be a string.');

            return [];
        }

        try {
            $response = $this->client->sendRequest(new Request('GET', $url));
            $json     = json_decode($response->getBody()->getContents(), true);

            if (JSON_ERROR_NONE !== json_last_error()) {
                Log::error('JSON decoding error for ' . $url);

                return [];
            }

            return $json;
        } catch (ClientExceptionInterface $e) {
            Log::error(sprintf('HTTP request error (%s); %s',
                $url, $e->getMessage()
            ));

            return [];
        }
    }

    /**
     * Checks whether the source is either a method or a function.
     *
     * @param array<int, string|class-string>|string $source
     */
    private function canBeCalled(array|string $source): bool
    {
        if (is_array($source) && method_exists($source[0], $source[1])) {
            return true;
        }

        return is_string($source) && function_exists($source);
    }

    /**
     * Loads the data from a callable method or function.
     *
     * @param array<int, string|class-string>|string $source
     *
     * @return array<string, mixed> The data or an empty array
     */
    private function loadFromCallable(array|string $source): array
    {
        if (is_array($source) && method_exists($source[0], $source[1])) {
            return app()->call($source[0] . '@' . $source[1]);
        }
        if (is_string($source) && function_exists($source)) {
            return $source();
        }

        return [];
    }

    /**
     * Checks whether the source is a file.
     *
     * @param array<int, string|class-string>|string $source
     */
    private function isFile(array|string $source): bool
    {
        return is_string($source) && is_file($source);
    }

    /**
     * Checks whether the source is a valid uri.
     *
     * @param array<int, string|class-string>|string $source
     */
    private function isUri(array|string $source): bool
    {
        return is_string($source) && filter_var($source, FILTER_VALIDATE_URL);
    }
}
