<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Lists\ListClient;

use App\Services\Lists\ListClient\TranslatableList\DataFormatter;
use App\Services\Lists\ListClient\TranslatableList\TranslatableList;
use App\Services\Lists\ListClient\TranslatableList\TranslatableListFactory;
use Illuminate\Support\Facades\Log;

/**
 * Class ListClient.
 *
 * Main implementation of the ListClientInterface.
 */
final class ListClient
{
    /**
     * The configured lists which are accessible.
     *
     * @var array<string, mixed>
     */
    private array $knownLists;

    /**
     * The names of the available lists (without any aliases).
     *
     * @var string[]
     */
    private array $originalNames;

    /**
     * ListClient constructor.
     *
     * @param TranslatableListFactory $listFactory The factory for creating list
     *                                             implementations
     * @param DataFormatter           $formatter   The list formatter to format list
     *                                             contents
     * @param array<string, mixed>    $knownLists  The configured lists which should be
     *                                             accessible
     */
    public function __construct(private readonly TranslatableListFactory $listFactory,
                                private readonly DataFormatter $formatter,
                                array $knownLists)
    {
        $this->buildKnownLists($knownLists);
    }

    /**
     * Retrieve a list specified by its name.
     *
     * @param string $name The name of the list to retrieve
     *
     * @return TranslatableList The requested list
     */
    public function list(string $name): TranslatableList
    {
        if (!array_key_exists($name, $this->knownLists)) {
            Log::warning(sprintf('Unknown list "%s" requested', $name));

            return $this->listFactory->dummy($name);
        }

        $list = $this->knownLists[$name];

        return $this->listFactory->createTranslatableList(
            $list['name'],
            $list['source'],
            $list['aliases']
        );
    }

    /**
     * Retrieve the original names of the configured lists without any aliases.
     *
     * @return string[] The original names
     */
    public function names(): array
    {
        return $this->originalNames;
    }

    /**
     * Retrieve the formatter to apply various formatting options to the contents
     * of a translatable list.
     *
     * @return DataFormatter The formatter to use
     */
    public function formatter(): DataFormatter
    {
        return $this->formatter;
    }

    /**
     * Builds an array of known lists based on the configured lists and their
     * aliases.
     *
     * @param array<string, mixed> $knownLists The configured lists which should be accessible
     */
    private function buildKnownLists(array $knownLists): void
    {
        $names = [];
        $lists = [];

        foreach ($knownLists as $name => $data) {
            $names[]      = $name;
            $lists[$name] = $data;

            foreach ($data['aliases'] as $alias) {
                $lists[strval($alias)] = $data;
            }
        }

        $this->originalNames = $names;
        $this->knownLists    = $lists;
    }
}
