<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Suggest;

use RuntimeException;
use XpertSelect\SolariumClient\Services\Suggest\SuggestionDocumentInterface;

/**
 * Class DCATTitleSuggestionDocument.
 *
 * Represents the implementation of the SuggestionDocumentInterface specific for giving title
 * suggestions for DCAT documents.
 */
class DCATTitleSuggestionDocument implements SuggestionDocumentInterface
{
    /**
     * The type of document this suggestion refers to.
     */
    private string $type;

    /**
     * The DCAT identifier.
     */
    private string $identifier;

    /**
     * The system name of the DCAT object.
     */
    private string $systemName;

    /**
     * The title of the object.
     */
    private string $title;

    /**
     * The languages for which this suggestion should be active.
     *
     * @var string[]
     */
    private array $languages;

    /**
     * Creates a DCATTitleSuggestionDocument based on the given dataset array.
     *
     * @param array<string, mixed> $dataset The dataset to base the title suggestion on
     *
     * @return SuggestionDocumentInterface The created suggestion document
     */
    public static function fromDataset(array $dataset): SuggestionDocumentInterface
    {
        $keys = ['identifier', 'name', 'title'];

        foreach ($keys as $key) {
            if (empty($dataset[$key])) {
                throw new RuntimeException(sprintf(
                    'Missing key [%s] in the dataset object; unable to create title suggestion',
                    $key
                ));
            }
        }

        return new DCATTitleSuggestionDocument(
            $dataset['identifier'],
            $dataset['name'],
            $dataset['title'],
            'dataset'
        );
    }

    /**
     * Creates a DCATTitleSuggestionDocument based on the given DataService array.
     *
     * @param array<string, mixed> $dataService The DataService to base the title suggestion on
     *
     * @return SuggestionDocumentInterface The created suggestion document
     */
    public static function fromDataService(array $dataService): SuggestionDocumentInterface
    {
        $keys = ['identifier', 'name', 'title'];

        foreach ($keys as $key) {
            if (empty($dataService[$key])) {
                throw new RuntimeException(sprintf(
                    'Missing key [%s] in the DataService object; unable to create title suggestion',
                    $key
                ));
            }
        }

        return new DCATTitleSuggestionDocument(
            $dataService['identifier'],
            $dataService['name'],
            $dataService['title'],
            'dataservice'
        );
    }

    /**
     * DCATTitleSuggestionDocument constructor.
     *
     * @param string $identifier The DCAT identifier
     * @param string $systemName The system name
     * @param string $title      The title
     * @param string $type       The type of the TitleSuggestion
     */
    public function __construct(string $identifier, string $systemName, string $title, string $type)
    {
        $this->type       = $type;
        $this->identifier = $identifier;
        $this->systemName = $systemName;
        $this->title      = $title;

        // DCAT title suggestions should be given regardless of the active language of the portal.
        // So the languages' property is hard-coded to contain all currently supported languages.
        $this->languages  = ['nl', 'en'];
    }

    /**
     * {@inheritdoc}
     */
    public function getWeight(): int
    {
        // Weight should be calculated by the TitleGenerationService. See: hasCustomWeightValue()

        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public function setWeight(int $weight): SuggestionDocumentInterface
    {
        // Weight should be calculated by the TitleGenerationService. See: hasCustomWeightValue()

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchURI(): string
    {
        return $this->identifier;
    }

    /**
     * {@inheritdoc}
     */
    public function setSearchURI(string $searchURI): SuggestionDocumentInterface
    {
        $this->identifier = $searchURI;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSystemName(): string
    {
        return $this->systemName;
    }

    /**
     * {@inheritdoc}
     */
    public function setSystemName(string $name): SuggestionDocumentInterface
    {
        $this->systemName = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function setType(string $type): SuggestionDocumentInterface
    {
        $this->type = $type;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * {@inheritdoc}
     */
    public function setLanguages(array $languages): SuggestionDocumentInterface
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasCustomWeightValue(): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function asSolariumUpdateDocument(): array
    {
        return [
            'payload'       => $this->systemName,
            'dataset'       => $this->title,
            'type'          => $this->type,
            'language'      => $this->languages,
            'in_context_of' => 'self',
        ];
    }
}
