<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Ishare;

use App\Exceptions\IshareException;
use App\Services\Ishare\Satellite\SatelliteClient;
use Illuminate\Support\Facades\Cache;

/**
 * Class PartiesService.
 */
class PartiesService
{
    private const allCacheKey = 'all-parties';

    /**
     * PartiesService Constructor.
     *
     * @param SatelliteClient $satelliteClient The client for communication with the iShare satellite
     */
    public function __construct(private readonly SatelliteClient $satelliteClient)
    {
    }

    /**
     * Get all Parties from the satellite.
     *
     * @throws IshareException
     *
     * @return array<int, mixed>
     */
    public function all(): array
    {
        if (Cache::has(self::allCacheKey)) {
            return cache()->get(self::allCacheKey);
        }

        $page    = 1;
        $parties = [];

        do {
            $response = $this->satelliteClient->getParties(page: $page);
            $total    = $response['total_count'];

            foreach ($response['data'] as $party) {
                $parties[] = $party;
            }
        } while (count($parties) < $total);

        $this->satelliteClient->revokeAccessToken();

        cache()->set(self::allCacheKey, $parties, 3600);

        return $parties;
    }
}
