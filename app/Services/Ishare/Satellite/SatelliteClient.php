<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services\Ishare\Satellite;

use App\Exceptions\IshareException;
use App\Services\JWT\JWT;
use Firebase\JWT\JWT as FirebaseJWT;
use Illuminate\Support\Facades\Http;

/**
 * Class SatelliteClient.
 */
class SatelliteClient
{
    private ?string $accessToken = null;

    /**
     * SatelliteClient constructor.
     *
     * @param ?string            $satelliteID  The EORI number of the iShare satellite
     * @param string             $satelliteURL The URL of the iShare satellite
     * @param ?string            $clientId     The EORI number of this application
     * @param array<int, string> $x5c          Self-contained JWT with token signing certificate details
     * @param string             $privateKey   The private key
     */
    public function __construct(private readonly ?string $satelliteID,
                                private string $satelliteURL,
                                private readonly ?string $clientId,
                                private readonly array $x5c,
                                private readonly string $privateKey)
    {
        if (str_ends_with($this->satelliteURL, '/')) {
            $this->satelliteURL = rtrim($this->satelliteURL, '/');
        }
    }

    /**
     * Execute a GET request to the /parties endpoint on a iShare satellite.
     *
     * @param null|string $name          Then name of the party
     * @param null|string $eori          The EORI number of the party
     * @param bool        $certifiedOnly Whether only certified parties should be fetched
     * @param bool        $activeOnly    Whether only active parties should be fetched
     * @param int         $page          The page number
     *
     * @throws IshareException
     *
     * @return array<string, mixed> The parties
     */
    public function getParties(?string $name = null,
                               ?string $eori = null,
                               bool $certifiedOnly = true,
                               bool $activeOnly = true,
                               int $page = 1): array
    {
        $accessToken = $this->getAccessToken();

        $headers = ['Authorization' => 'Bearer ' . $accessToken];

        $parameters = [
            'certified_only' => $certifiedOnly,
            'active_only'    => $activeOnly,
            'page'           => $page,
        ];

        if (!is_null($name)) {
            $parameters['name'] = $name;
        }
        if (!is_null($eori)) {
            $parameters['eori'] = $eori;
        }

        $url = $this->buildUrl('/parties', $parameters);

        $response_token_encoded = Http::withHeaders($headers)
            ->get($url)
            ->json('parties_token');

        $payload = JWT::decode($response_token_encoded);

        return (array) $payload->parties_info;
    }

    /**
     * Set the accessToken to NULL.
     */
    public function revokeAccessToken(): void
    {
        $this->accessToken = null;
    }

    /**
     * Create the JWT Encoded clientAssertion and execute a POST request to the /connect/token
     * endpoint of the iShare Satellite to receive an access token.
     *
     * @throws IshareException
     *
     * @return string The access token
     */
    private function getAccessToken(): string
    {
        if (!is_null($this->accessToken)) {
            return $this->accessToken;
        }

        $header = [
            'typ' => 'JWT',
            'x5c' => $this->x5c,
        ];

        $payload = [
            'iss' => $this->clientId,
            'sub' => $this->clientId,
            'aud' => $this->satelliteID,
            'jti' => uuid_create(UUID_TYPE_TIME),
            'iat' => time(),
            'exp' => time() + 30,
        ];

        // @phpstan-ignore-next-line
        $clientAssertion = FirebaseJWT::encode($payload, $this->privateKey, 'RS256', 'foo', $header);

        $header = [
            'Content-type' => 'application/x-www-form-urlencoded',
        ];

        $params = [
            'grant_type'            => 'client_credentials',
            'scope'                 => 'iSHARE',
            'client_id'             => $this->clientId,
            'client_assertion_type' => 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
            'client_assertion'      => $clientAssertion,
        ];

        $url = $this->buildUrl('/connect/token', $params);

        $response_json = Http::withHeaders($header)
            ->post($url)
            ->json();

        if (!array_key_exists('access_token', $response_json)) {
            $message = array_key_exists('message', $response_json) ? $response_json['message'] : '';

            throw new IshareException('Something went wrong while trying to get the access_token: ' . $message);
        }

        $this->accessToken = $response_json['access_token'];

        return $this->accessToken;
    }

    /**
     * Build the url for the request.
     *
     * @param string               $path       The path
     * @param array<string, mixed> $parameters The query parameters
     *
     * @return string The url
     */
    private function buildUrl(string $path, array $parameters): string
    {
        return sprintf('%s%s?%s', $this->satelliteURL, $path, http_build_query($parameters));
    }
}
