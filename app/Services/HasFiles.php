<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Trait HasFiles.
 */
trait HasFiles
{
    /**
     * Upload a file and return the storage url, or null if something went wrong.
     *
     * @param array<string, mixed> $requestBody The HTTP Request body
     * @param string               $destination Where to save the file on the server
     * @param string               $field       The name of field where the uploaded file can be found
     *
     * @return null|string The storage url
     */
    public function uploadFile(array $requestBody, string $destination, string $field): ?string
    {
        /** @var UploadedFile $file */
        $file = $requestBody[$field];

        $img_path = $file->storePubliclyAs(
            'public/images/' . $destination,
            $file->getClientOriginalName()
        );

        if (!$img_path) {
            return null;
        }

        return Storage::url($img_path);
    }
}
