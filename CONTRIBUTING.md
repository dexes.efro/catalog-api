# Contributing guide

## Git

TODO:

## Code quality

- Ensure the code passes `PHPStan`'s analysis.
  - To check for quality issues: `./resources/dev-tools/phpstan.phar analyse`
- `PHPCPD` is used to detect duplicate code blocks, ensure there aren't any.
  - To check for duplicate code locally: `./resources/dev-tools/phpcpd.phar --min-lines=25 --fuzzy app`
- Both Composer and NPM dependencies are scanned for vulnerabilities. No vulnerabilities are allowed.
  - To check for Composer vulnerabilities: `./resources/dev-tools/local-php-security-checker`
  - To check for NPM vulnerabilities: `npm audit`

## Code style

- Respect the `.editorconfig` file.
- All written code must pass `PHP CS Fixer` analysis. 
  - To check to code for adherence: `./resources/dev-tools/php-cs-fixer.phar fix --dry-run`
  - To automatically resolve all issues: `./resources/dev-tools/php-cs-fixer.phar fix`

## Testing

TODO:
