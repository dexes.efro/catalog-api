<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

return [
    [
        'name'     => 'admin',
        'email'    => 'support@textinfo.nl',
        'password' => 'password',
    ],
];
