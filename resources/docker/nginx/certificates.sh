#!/bin/sh


set -eu


TARGET_DIRECTORY=/var/nginx-certificates


if [ -f "${TARGET_DIRECTORY}/private.key" ]; then
    return 0
fi


if [ -f "${TARGET_DIRECTORY}/public.crt" ]; then
    return 0
fi


openssl req \
    -new \
    -newkey rsa:4096 \
    -days 3650 \
    -nodes \
    -x509 \
    -subj "/C=NL/ST=Gelderland/L=Nijmegen/O=Textinfo B.V./CN=api.dexes.local" \
    -addext "subjectAltName=DNS:*.api.dexes.local" \
    -keyout "${TARGET_DIRECTORY}/private.key" \
    -out "${TARGET_DIRECTORY}/public.crt"
