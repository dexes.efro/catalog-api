#!/bin/sh


set -eu


if [ ! "$(command -v npm)" ]; then
    echo "command npm is unavailable" && return 1
fi


if [ -f ./package-lock.json ]; then
    npm ci
else
    npm install
fi


exec npm run watch
