#!/bin/sh


set -eu


wait-for-it dexes-api-mariadb:3306 -s -t 60
wait-for-it dexes-api-mongodb:27017 -s -t 60


phive install --trust-gpg-keys E82B2FB314E9906E,CF1A108D0E7AE720,4AA394086372C20A,51C67305FFC2E5C0
composer install


php artisan storage:link
php artisan migrate --force
php artisan db:seed --force


exec php-fpm
