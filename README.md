# Dexes / Catalog API

[gitlab.com/dexes.efro/catalog-api](https://gitlab.com/dexes.efro/catalog-api)

## License

View the `LICENSE.md` file for licensing details.

## Getting started

To quickly get this project up and running, follow these steps (running `composer install` or `npm ci` is not required, the Docker containers will run these commands if required):

1. Open the `.env` file and update the `DB_PASSWORD` key to use a password of choice.
2. Open the `.env` file and update the `MONGODB_PASSWORD` key to use a password of choice.
3. Run `docker-compose up --quiet-pull --detach`.

The project is now up and running.
