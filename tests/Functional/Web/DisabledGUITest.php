<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\Web;

use Tests\TestCase;

/**
 * @internal
 */
class DisabledGUITest extends TestCase
{
    public function testHomepageRedirectsToSwaggerWhenGUIIsDisabled(): void
    {
        config(['xs-framework.enable_gui' => false]);

        $this->get('/')
            ->assertRedirect(config('l5-swagger.documentations.default.routes.api'));
    }
}
