<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\System;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;

/**
 * @internal
 */
class ServiceProviderTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutEvents;

    public function testWebRoutesDoNotHaveRateLimitingEnabled(): void
    {
        Route::get('/foo', function() {
            return 'Hello';
        })->middleware('web');

        $this->get('/foo')
            ->assertHeaderMissing('X-RateLimit-Limit')
            ->assertHeaderMissing('X-RateLimit-Remaining');
    }

    public function testApiRoutesHaveRateLimitingEnabled(): void
    {
        Route::get('/api/foo', function() {
            return 'Hello';
        })->middleware('api');

        $this->get('/api/foo')
            ->assertHeader('X-RateLimit-Limit')
            ->assertHeader('X-RateLimit-Remaining');
    }
}
