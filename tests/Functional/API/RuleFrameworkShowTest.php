<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\RuleFrameworkController;
use App\Models\RuleFramework;
use App\Repositories\RuleFrameworkRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * @internal
 */
class RuleFrameworkShowTest extends TestCase
{
    use WithoutMiddleware;

    public function testRetrieveHTTP404WhenRuleFrameworkDoesNotExist(): void
    {
        $this->app->when(RuleFrameworkController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(RuleFrameworkRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andThrow(StorageRecordNotFoundException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-ruleframework');
                });
            });

        $this->call('GET', 'api/dexes-ruleframeworks/foo')
            ->assertStatus(404);
    }

    public function testRetrieveHTTP500WhenFrameworkExistsButCouldNotBeLoaded(): void
    {
        $this->app->when(RuleFrameworkController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(RuleFrameworkRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andThrow(StorageRecordStorageException::class);
                });
            });

        $this->call('GET', 'api/dexes-ruleframeworks/foo')
            ->assertStatus(500);
    }

    public function testRetrieveDatasetWithHTTP200WhenFrameworkExistsAndWasLoaded(): void
    {
        $dataset        = new RuleFramework();
        $dataset->name  = 'dexes';

        $this->app->when(RuleFrameworkController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() use ($dataset) {
                return Mockery::mock(RuleFrameworkRepository::class, function(MockInterface $mock) use ($dataset) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andReturn($dataset);
                });
            });

        $this->call('GET', 'api/dexes-ruleframeworks/foo')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'dexes',
            ]);
    }

    public function testRetrieveDatasetsWithHTTP200WhenNoFrameworksExists(): void
    {
        $this->app->when(RuleFrameworkController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(RuleFrameworkRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('paginate')
                        ->withAnyArgs()
                        ->andReturn(new Collection());

                    $mock->shouldReceive('count')
                        ->andReturn(0);
                });
            });

        $this->call('GET', 'api/dexes-ruleframeworks')
            ->assertStatus(200);
    }

    public function testRetrieveDatasetsWithHTTP500WhenStorageIsUnavailable(): void
    {
        $this->app->when(RuleFrameworkController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(RuleFrameworkRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('paginate')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordStorageException::class);
                });
            });

        $this->call('GET', 'api/dexes-ruleframeworks')
            ->assertStatus(500);
    }
}
