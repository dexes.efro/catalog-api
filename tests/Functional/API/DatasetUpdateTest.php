<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\DatasetController;
use App\Repositories\DatasetRepository;
use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\TranslatableList;
use Mockery;
use Mockery\MockInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordExistsException;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * @internal
 */
class DatasetUpdateTest extends DatasetTestCase
{
    public function testPutDatasetValidationErrorIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                $mock->shouldReceive('list')
                    ->withAnyArgs()
                    ->andReturn(new TranslatableList('', [], '', ''));
            });
        });

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('update')
                        ->with('foo')
                        ->andThrow(StorageRecordExistsException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $response = $this->putJson('api/dexes-datasets/foo');

        $response
            ->assertStatus(400)
            ->assertSee('The title field is required.')
            ->assertSee('The notes field is required')
            ->assertSee('The theme field is required.')
            ->assertSee('The identifier field is required')
            ->assertSee('The authority field is required')
            ->assertSee('The publisher field is required')
            ->assertSee('The contact point name field is required')
            ->assertSee('The metadata language field is required')
            ->assertSee('The language field is required')
            ->assertSee('The license id field is required')
            ->assertSee('The modified field is required')
            ->assertSee('The resources field must be present');
    }

    public function testPutDatasetNotExistsErrorIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('update')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordNotFoundException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $response = $this->putJson('api/dexes-datasets/foo', $this->getValidDatasetObject());

        $response->assertStatus(409);
    }

    public function testPutDatasetStorageRecordExceptionIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('update')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $response = $this->putJson('api/dexes-datasets/foo', $this->getValidDatasetObject());

        $response->assertStatus(500);
    }

    public function testPutDatasetNoExceptionIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('update')
                        ->withAnyArgs()
                        ->andReturn($this->getValidDatasetObject());
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $response = $this->putJson('api/dexes-datasets/foo', $this->getValidDatasetObject());

        $response->assertStatus(201);
    }
}
