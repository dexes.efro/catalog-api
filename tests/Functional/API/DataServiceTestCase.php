<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

/**
 * @internal
 */
abstract class DataServiceTestCase extends TestCase
{
    use WithoutMiddleware;

    /**
     * Get a valid DataService object that can be used in unit tests and won't give any
     * validation errors.
     */
    public function getValidDataServiceObject(): array
    {
        return [
            'title'          => 'some title',
            'contact_point'  => [
                'name'  => 'bob',
                'email' => 'test@email.com',
            ],
            'identifier'     => '123abc',
            'publisher'      => 'https://standaarden.overheid.nl/owms/terms/Amsterdam',
            'endpoint_url'   => 'some.api.com/api/v1',
            'dataspace_uri'  => 'https://open.dexspace.nl',
        ];
    }

    protected function loadXsLists(): array
    {
        return include __DIR__ . '/../test-xs-lists.php';
    }
}
