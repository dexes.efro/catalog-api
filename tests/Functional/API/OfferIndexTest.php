<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use Database\Seeders\OfferTestSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

/**
 * @internal
 */
class OfferIndexTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseMigrations;

    public function testRetrieveEmptyListWithHTTP200WhenNoOffersExist(): void
    {
        $this->call('GET', 'api/dexes-offers')
            ->assertStatus(200)
            ->assertExactJson([
                'meta'    => [
                    'start' => 0,
                    'rows'  => 100,
                    'total' => 0,
                ],
                'results' => [],
            ]);
    }

    public function testStartQueryParameterIsRespected(): void
    {
        $this->call('GET', 'api/dexes-offers?start=10')
            ->assertStatus(200)
            ->assertExactJson([
                'meta'    => [
                    'start' => 10,
                    'rows'  => 100,
                    'total' => 0,
                ],
                'results' => [],
            ]);
    }

    public function testRowsQueryParameterIsRespected(): void
    {
        $this->call('GET', 'api/dexes-offers?rows=10')
            ->assertStatus(200)
            ->assertExactJson([
                'meta'    => [
                    'start' => 0,
                    'rows'  => 10,
                    'total' => 0,
                ],
                'results' => [],
            ]);
    }

    public function testTotalValueIsAccurate(): void
    {
        $this->seed(OfferTestSeeder::class);

        $this->call('GET', 'api/dexes-offers')
            ->assertStatus(200)
            ->assertJsonFragment([
                'start' => 0,
                'rows'  => 100,
                'total' => OfferTestSeeder::SEED_COUNT,
            ])
            ->assertJsonCount(OfferTestSeeder::SEED_COUNT, 'results');
    }
}
