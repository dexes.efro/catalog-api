<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\DatasetController;
use App\Repositories\DatasetRepository;
use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\TranslatableList;
use Mockery;
use Mockery\MockInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordExistsException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * @internal
 */
class DatasetCreateTest extends DatasetTestCase
{
    public function testCreateDatasetValidationErrorIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                $mock->shouldReceive('list')
                    ->withAnyArgs()
                    ->andReturn(new TranslatableList('', [], '', ''));
            });
        });

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('create')
                        ->with('foo')
                        ->andThrow(StorageRecordExistsException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $response = $this->postJson('api/dexes-datasets', [
        ]);

        $response
            ->assertStatus(400)
            ->assertSee('The title field is required.')
            ->assertSee('The notes field is required')
            ->assertSee('The theme field is required.')
            ->assertSee('The identifier field is required')
            ->assertSee('The authority field is required')
            ->assertSee('The publisher field is required')
            ->assertSee('The contact point name field is required')
            ->assertSee('The metadata language field is required')
            ->assertSee('The language field is required')
            ->assertSee('The license id field is required')
            ->assertSee('The modified field is required')
            ->assertSee('The resources field must be present')
            ->assertSee('The dataspace uri field is required');
    }

    public function testCreateDatasetExistsExceptionIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('create')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordExistsException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $response = $this->postJson('api/dexes-datasets', $this->getValidDatasetObject());

        $response->assertStatus(409);
    }

    public function testCreateDatasetStorageExceptionIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('create')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $response = $this->postJson('api/dexes-datasets', $this->getValidDatasetObject());

        $response->assertStatus(500);
    }

    public function testCreateDatasetDatasetCreated(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('create')
                        ->withAnyArgs()
                        ->andReturn($this->getValidDatasetObject());
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $response = $this->postJson('api/dexes-datasets', $this->getValidDatasetObject());

        $response->assertStatus(201)
            ->assertSee('example@email.com');
    }
}
