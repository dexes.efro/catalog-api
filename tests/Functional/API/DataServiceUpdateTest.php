<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\DataServiceController;
use App\Repositories\DataServiceRepository;
use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\TranslatableList;
use Mockery;
use Mockery\MockInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordExistsException;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * @internal
 */
class DataServiceUpdateTest extends DataServiceTestCase
{
    public function testPutDataServiceValidationErrorIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                $mock->shouldReceive('list')
                    ->withAnyArgs()
                    ->andReturn(new TranslatableList('', [], '', ''));
            });
        });

        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('update')
                        ->with('foo')
                        ->andThrow(StorageRecordExistsException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $response = $this->putJson('api/dexes-dataservices/foo', [
        ]);

        $response
            ->assertStatus(400)
            ->assertSee('The contact point.name field is required')
            ->assertSee('The contact point.email field is required when none of contact point.website \/ contact point.phone are present.')
            ->assertSee('The contact point.website field is required when none of contact point.email \/ contact point.phone are present.')
            ->assertSee('The contact point.phone field is required when none of contact point.email \/ contact point.website are present.')
            ->assertSee('The identifier field is required.')
            ->assertSee('The publisher field is required.')
            ->assertSee('The title field is required.')
            ->assertSee('The endpoint url field is required.');
    }

    public function testPutDataServiceNotExistsErrorIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('update')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordNotFoundException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $response = $this->putJson('api/dexes-dataservices/foo', $this->getValidDataServiceObject());

        $response->assertStatus(409);
    }

    public function testPutDataServiceStorageRecordExceptionIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('update')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $response = $this->putJson('api/dexes-dataservices/foo', $this->getValidDataServiceObject());

        $response->assertStatus(500);
    }

    public function testPutDataServiceNoExceptionIsThrown(): void
    {
        $this->app->bind(ListClient::class, function() {
            return Mockery::mock(ListClient::class, function(MockInterface $mock) {
                foreach ($this->loadXsLists() as $key => $item) {
                    $list = new TranslatableList($key, $item['data'], $item['language'], $item['source'], $item['aliases']);

                    $mock->shouldReceive('list')
                        ->with($key)
                        ->andReturn($list);
                }
            });
        });

        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('update')
                        ->withAnyArgs()
                        ->andReturn($this->getValidDataServiceObject());
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $response = $this->putJson('api/dexes-dataservices/foo', $this->getValidDataServiceObject());

        $response->assertStatus(201);
    }
}
