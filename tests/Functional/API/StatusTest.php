<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

/**
 * @internal
 */
class StatusTest extends TestCase
{
    use DatabaseMigrations;

    public function testStatusContainsTheApplicationName(): void
    {
        config(['app.name' => 'LoremIpsum']);

        $this->call('GET', '/api/status')
            ->assertStatus(200)
            ->assertJsonFragment([
                'application' => 'LoremIpsum',
            ]);
    }

    public function testStatusIndicatesIfTheInterfaceIsEnabled(): void
    {
        config(['xs-framework.enable_gui' => true]);

        $this->call('GET', '/api/status')
            ->assertStatus(200)
            ->assertJsonFragment([
                'interface' => 'enabled',
            ]);

        config(['xs-framework.enable_gui' => false]);

        $this->call('GET', '/api/status')
            ->assertStatus(200)
            ->assertJsonFragment([
                'interface' => 'disabled',
            ]);
    }

    public function testStatusContainsNoContentTypesWhenConfigIsEmpty(): void
    {
        config(['xs-content-types' => []]);

        $this->call('GET', '/api/status')
            ->assertStatus(200)
            ->assertJsonFragment([
                'content-types' => [],
            ]);
    }
}
