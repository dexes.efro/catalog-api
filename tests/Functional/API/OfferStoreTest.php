<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

/**
 * @internal
 */
class OfferStoreTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseMigrations;

    public function testValidationErrorIsThrownForMissingFields(): void
    {
        $response = $this->postJson('api/dexes-offers');

        $response->assertStatus(400)
            ->assertSee('The id field is required.')
            ->assertSee('The url field is required.')
            ->assertSee('The data owner field is required.')
            ->assertSee('The resource field is required.')
            ->assertSee('The created at field is required.');
    }

    public function testValidationErrorIsThrownForEmptyFields(): void
    {
        $response = $this->postJson('api/dexes-offers', [
            'id'         => '',
            'url'        => '',
            'dataOwner'  => '',
            'resource'   => '',
            'permitted'  => '',
            'scope'      => '',
            'createdAt'  => '',
        ]);

        $response->assertStatus(400)
            ->assertSee('The id field is required.')
            ->assertSee('The url field is required.')
            ->assertSee('The data owner field is required.')
            ->assertSee('The resource field is required.')
            ->assertSee('The created at field is required.');
    }

    public function testValidationErrorIsThrownWhenConditionsIsNotAnArray(): void
    {
        $response = $this->postJson('api/dexes-offers', [
            'conditions' => 'lorem',
        ]);

        $response->assertStatus(400)
            ->assertSee('The conditions must be an array.');

        $response = $this->postJson('api/dexes-offers', [
            'conditions' => [],
        ]);

        $response->assertStatus(400)
            ->assertDontSee('The conditions must be an array.');
    }
}
