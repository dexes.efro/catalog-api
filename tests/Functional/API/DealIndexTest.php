<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\DealController;
use App\Repositories\DealRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;

/**
 * @internal
 */
class DealIndexTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseMigrations;

    public function testRetrieveEmptyListWithHTTP200WhenNoDealsExist(): void
    {
        $this->app->when(DealController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DealRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('paginate')
                        ->andReturn(new Collection([]));
                    $mock->shouldReceive('count')
                        ->andReturn(0);
                });
            });

        $this->call('GET', 'api/dexes-deals')
            ->assertStatus(200)
            ->assertExactJson([
                'meta'    => [
                    'start' => 0,
                    'rows'  => 100,
                    'total' => 0,
                ],
                'results' => [],
            ]);
    }

    public function testStartQueryParameterIsRespected(): void
    {
        $this->app->when(DealController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DealRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('paginate')
                        ->andReturn(new Collection([]));
                    $mock->shouldReceive('count')
                        ->andReturn(0);
                });
            });

        $this->call('GET', 'api/dexes-deals?start=10')
            ->assertStatus(200)
            ->assertExactJson([
                'meta'    => [
                    'start' => 10,
                    'rows'  => 100,
                    'total' => 0,
                ],
                'results' => [],
            ]);
    }

    public function testRowsQueryParameterIsRespected(): void
    {
        $this->call('GET', 'api/dexes-deals?rows=10')
            ->assertStatus(200)
            ->assertExactJson([
                'meta'    => [
                    'start' => 0,
                    'rows'  => 10,
                    'total' => 0,
                ],
                'results' => [],
            ]);
    }
}
