<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\DatasetController;
use App\Repositories\DatasetRepository;
use Mockery;
use Mockery\MockInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * @internal
 */
class DatasetDestroyTest extends DatasetTestCase
{
    public function testRetrieveHTTP404WhenDeletingADatasetThatDoesNotExist(): void
    {
        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('delete')
                        ->with('foo')
                        ->andThrow(StorageRecordNotFoundException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $this->call('DELETE', 'api/dexes-datasets/foo')
            ->assertStatus(409);
    }

    public function testRetrieveHTTP500WhenDatasetExistsButCouldNotBeLoaded(): void
    {
        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('delete')
                        ->with('foo')
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $this->call('DELETE', 'api/dexes-datasets/foo')
            ->assertStatus(500);
    }

    public function testRetrieveHTTP204(): void
    {
        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('delete')
                        ->with('foo')
                        ->andReturnTrue();
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $this->call('DELETE', 'api/dexes-datasets/foo')
            ->assertNoContent();
    }
}
