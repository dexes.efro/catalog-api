<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\DataServiceController;
use App\Models\DataService;
use App\Repositories\DataServiceRepository;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * @internal
 */
class DataServiceShowTest extends DataServiceTestCase
{
    public function testRetrieveHTTP404WhenDatasetDoesNotExist(): void
    {
        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andThrow(StorageRecordNotFoundException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $this->call('GET', 'api/dexes-dataservices/foo')
            ->assertStatus(404);
    }

    public function testRetrieveHTTP500WhenDatasetExistsButCouldNotBeLoaded(): void
    {
        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $this->call('GET', 'api/dexes-dataservices/foo')
            ->assertStatus(500);
    }

    public function testRetrieveDatasetWithHTTP200WhenDatasetExistsAndWasLoaded(): void
    {
        $dataset        = new DataService();
        $dataset->title = 'title';

        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() use ($dataset) {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) use ($dataset) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andReturn($dataset);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $this->call('GET', 'api/dexes-dataservices/foo')
            ->assertStatus(200)
            ->assertJsonFragment([
                'title' => 'title',
            ]);
    }

    public function testRetrieveDatasetsWithHTTP200WhenNoDatasetsExists(): void
    {
        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('paginate')
                        ->withAnyArgs()
                        ->andReturn(new Collection());
                    $mock->shouldReceive('count')
                        ->andReturn(0);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $this->call('GET', 'api/dexes-dataservices')
            ->assertStatus(200);
    }

    public function testRetrieveDatasetsWithHTTP500WhenStorageIsUnavailable(): void
    {
        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('paginate')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $this->call('GET', 'api/dexes-dataservices')
            ->assertStatus(500);
    }
}
