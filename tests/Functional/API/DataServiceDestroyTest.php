<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\DataServiceController;
use App\Repositories\DataServiceRepository;
use Mockery;
use Mockery\MockInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * @internal
 */
class DataServiceDestroyTest extends DataServiceTestCase
{
    public function testRetrieveHTTP404WhenDeletingADataServiceThatDoesNotExist(): void
    {
        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('delete')
                        ->with('foo')
                        ->andThrow(StorageRecordNotFoundException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $this->call('DELETE', 'api/dexes-dataservices/foo')
            ->assertStatus(409);
    }

    public function testRetrieveHTTP500WhenDataServiceExistsButCouldNotBeLoaded(): void
    {
        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('delete')
                        ->with('foo')
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $this->call('DELETE', 'api/dexes-dataservices/foo')
            ->assertStatus(500);
    }

    public function testRetrieveHTTP204(): void
    {
        $this->app->when(DataServiceController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DataServiceRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('delete')
                        ->with('foo')
                        ->andReturnTrue();
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataservice');
                });
            });

        $this->call('DELETE', 'api/dexes-dataservices/foo')
            ->assertNoContent();
    }
}
