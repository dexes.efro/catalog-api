<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

/**
 * @internal
 */
abstract class DatasetTestCase extends TestCase
{
    use WithoutMiddleware;

    /**
     * Get a valid Dataset object that can be used in unit tests and won't give any validation
     * errors.
     */
    protected function getValidDatasetObject(): array
    {
        return [
            'title'               => 'some title',
            'notes'               => 'notes',
            'theme'               => [
                'https://standaarden.overheid.nl/owms/terms/Afval_(thema)',
            ],
            'identifier'          => 'identifier',
            'authority'           => 'https://standaarden.overheid.nl/owms/terms/Amsterdam',
            'publisher'           => 'https://standaarden.overheid.nl/owms/terms/Amsterdam',
            'contact_point_name'  => 'name',
            'contact_point_email' => 'example@email.com',
            'metadata_language'   => 'https://publications.europa.eu/resource/authority/language/NLD',
            'language'            => [
                'https://publications.europa.eu/resource/authority/language/NLD',
            ],
            'license_id'          => 'https://creativecommons.org/publicdomain/zero/1.0/deed.nl',
            'modified'            => '1111-11-11T00:00:00',
            'resources'           => [
                [
                    'title'             => 'title',
                    'url'               => 'https://example.com',
                    'name'              => 'title',
                    'description'       => 'title',
                    'metadata_language' => 'https://publications.europa.eu/resource/authority/language/NLD',
                    'language'          => [
                        'https://publications.europa.eu/resource/authority/language/NLD',
                    ],
                    'license_id'        => 'https://creativecommons.org/publicdomain/zero/1.0/deed.nl',
                    'format'            => 'https://publications.europa.eu/resource/authority/file-type/MAP_PRVW',
                ],
            ],
            'dataspace_uri'       => 'https://open.dexspace.nl',
        ];
    }

    protected function loadXsLists(): array
    {
        return include __DIR__ . '/../test-xs-lists.php';
    }
}
