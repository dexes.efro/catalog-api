<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\API;

use App\Http\Controllers\API\DatasetController;
use App\Models\Dataset;
use App\Repositories\DatasetRepository;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;
use XpertSelect\Framework\Contracts\Repositories\RepositoryInterface;
use XpertSelect\Framework\Exceptions\StorageRecordNotFoundException;
use XpertSelect\Framework\Exceptions\StorageRecordStorageException;

/**
 * @internal
 */
class DatasetShowTest extends DatasetTestCase
{
    public function testRetrieveHTTP404WhenDatasetDoesNotExist(): void
    {
        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andThrow(StorageRecordNotFoundException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $this->call('GET', 'api/dexes-datasets/foo')
            ->assertStatus(404);
    }

    public function testRetrieveHTTP500WhenDatasetExistsButCouldNotBeLoaded(): void
    {
        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $this->call('GET', 'api/dexes-datasets/foo')
            ->assertStatus(500);
    }

    public function testRetrieveDatasetWithHTTP200WhenDatasetExistsAndWasLoaded(): void
    {
        $dataset        = new Dataset();
        $dataset->title = 'title';

        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() use ($dataset) {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) use ($dataset) {
                    $mock->shouldReceive('find')
                        ->with('foo')
                        ->andReturn($dataset);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $this->call('GET', 'api/dexes-datasets/foo')
            ->assertStatus(200)
            ->assertJsonFragment([
                'title' => 'title',
            ]);
    }

    public function testRetrieveDatasetsWithHTTP200WhenNoDatasetsExists(): void
    {
        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('paginate')
                        ->withAnyArgs()
                        ->andReturn(new Collection());
                    $mock->shouldReceive('count')
                        ->andReturn(0);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $this->call('GET', 'api/dexes-datasets')
            ->assertStatus(200);
    }

    public function testRetrieveDatasetsWithHTTP500WhenStorageIsUnavailable(): void
    {
        $this->app->when(DatasetController::class)
            ->needs(RepositoryInterface::class)
            ->give(function() {
                return Mockery::mock(DatasetRepository::class, function(MockInterface $mock) {
                    $mock->shouldReceive('paginate')
                        ->withAnyArgs()
                        ->andThrow(StorageRecordStorageException::class);
                    $mock->shouldReceive('getName')
                        ->andReturn('dexes-dataset');
                });
            });

        $this->call('GET', 'api/dexes-datasets')
            ->assertStatus(500);
    }
}
