<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\Console;

use Tests\TestCase;

/**
 * @internal
 */
class TranslatableListsListTest extends TestCase
{
    public function testCommandShowsEmptyTableWhenNoListsAreConfigured(): void
    {
        config(['xs-lists' => []]);

        $this->artisan('translatable-lists:list')
            ->expectsTable(['List', 'Aliases', 'Source', 'Entries'], [])
            ->assertExitCode(0);
    }

    public function testCommandShowsNonTableWhenListsAreConfigured(): void
    {
        config(['xs-lists' => [
            'Foo' => [
                'name'    => 'Foo',
                'source'  => '/dev/null',
                'aliases' => [],
            ],
        ]]);

        $this->artisan('translatable-lists:list')
            ->expectsTable(['List', 'Aliases', 'Source', 'Entries'], [
                ['Foo', '', '/dev/null', 0],
            ])
            ->assertExitCode(0);
    }
}
