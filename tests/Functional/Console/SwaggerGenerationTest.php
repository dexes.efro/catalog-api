<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\Console;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

/**
 * @internal
 */
class SwaggerGenerationTest extends TestCase
{
    use DatabaseMigrations;

    public function testSwaggerDocumentationCanBeGenerated(): void
    {
        $this->artisan('l5-swagger:generate')
            ->assertSuccessful();
    }
}
