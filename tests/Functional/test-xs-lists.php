<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use App\Services\Lists\ListDataService;

return [
    'Overheid:TaxonomieBeleidsagenda' => [
        'name'     => 'Overheid:TaxonomieBeleidsagenda',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/overheid_taxonomiebeleidsagenda.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://standaarden.overheid.nl/owms/terms/Afval_(thema)' => [
                'labels' => [
                    'nl-NL' => 'Afval',
                    'en-US' => 'Waste',
                ],
                'parent' => 'https://standaarden.overheid.nl/owms/terms/Natuur_en_milieu',
            ],
        ],
    ],
    'DONL:Catalogs'                   => [
        'name'     => 'DONL:Catalogs',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/donl_catalogs.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://opendata.cbs.nl/ODataCatalog/' => [
                'labels' => [
                    'nl-NL' => 'Centraal Bureau voor de Statistiek',
                    'en-US' => 'Centraal Bureau voor de Statistiek',
                ],
                'slug'   => 'centraal_bureau_voor_de_statistiek',
            ],
        ],
    ],
    'DEXES:Catalogs'                   => [
        'name'     => 'DEXES:Catalogs',
        'source'   => [ListDataService::class, 'catalogsList'],
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://opendata.cbs.nl/ODataCatalog/' => [
                'labels' => [
                    'nl-NL' => 'Centraal Bureau voor de Statistiek',
                    'en-US' => 'Centraal Bureau voor de Statistiek',
                ],
                'slug'   => 'centraal_bureau_voor_de_statistiek',
            ],
        ],
    ],
    'DEXES:Organization'               => [
        'name'     => 'DEXES:Organization',
        'source'   => 'https://api.staging.dexes.eu/api/lists/organizations',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => ['https://standaarden.overheid.nl/owms/terms/Amsterdam' => [
            'labels' => [
                'en-US' => 'Gemeente Amsterdam',
            ],
        ]],
    ],
    'DONL:Language'                   => [
        'name'     => 'DONL:Language',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/donl_language.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://publications.europa.eu/resource/authority/language/NLD' => [
                'labels' => [
                    'nl-NL' => 'Nederlands',
                    'en-US' => 'Dutch',
                ],
            ],
        ],
    ],
    'DEXES:License'                    => [
        'name'     => 'DEXES:License',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/donl_license.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://creativecommons.org/publicdomain/zero/1.0/deed.nl' => [
                'labels' => [
                    'nl-NL' => 'CC-0 (1.0)',
                    'en-US' => 'CC-0 (1.0)',
                ],
            ],
        ],
    ],
    'Overheid:Openbaarheidsniveau'    => [
        'name'     => 'Overheid:Openbaarheidsniveau',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/overheid_openbaarheidsniveau.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://publications.europa.eu/resource/authority/access-right/NON_PUBLIC' => [
                'labels' => [
                    'nl-NL' => 'Niet Publiek',
                    'en-US' => 'Non Public',
                ],
            ],
        ],
    ],
    'DONL:WOBUitzondering'            => [
        'name'     => 'DONL:WOBUitzondering',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/donl_wobuitzondering.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [],
    ],
    'Overheid:DatasetStatus'          => [
        'name'     => 'Overheid:DatasetStatus',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/overheid_dataset_status.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://data.overheid.nl/status/beschikbaar' => [
                'labels' => [
                    'nl-NL' => 'Beschikbaar',
                    'en-US' => 'Available',
                ],
            ],
        ],
    ],
    'Overheid:Frequency'              => [
        'name'     => 'Overheid:Frequency',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/overheid_frequency.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://publications.europa.eu/resource/authority/frequency/ANNUAL' => [
                'labels' => [
                    'nl-NL' => 'Jaarlijks',
                    'en-US' => 'Annual',
                ],
            ],
        ],
    ],
    'ADMS:Changetype'                 => [
        'name'     => 'ADMS:Changetype',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/adms_changetype.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            ':created' => [
                'labels' => [
                    'nl-NL' => 'aangemaakt',
                    'en-US' => 'created',
                ],
            ],
        ],
    ],
    'ADMS:DistributieStatus'          => [
        'name'     => 'ADMS:DistributieStatus',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/adms_distributiestatus.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://purl.org/adms/status/Completed' => [
                'labels' => [
                    'nl-NL' => 'afgerond',
                    'en-US' => 'completed',
                ],
            ],
        ],
    ],
    'MDR:Filetype'                    => [
        'name'     => 'MDR:Filetype',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/mdr_filetype_nal.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://publications.europa.eu/resource/authority/file-type/MAP_PRVW' => [
                'labels' => [
                    'nl-NL' => 'ArcGIS Map Preview',
                    'en-US' => 'ArcGIS Map Preview',
                ],
            ],
        ],
    ],
    'IANA:MediaTypes'                 => [
        'name'     => 'IANA:MediaTypes',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/iana_mediatypes.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://www.iana.org/assignments/media-types/application/1d-interleaved-parityfec' => [
                'labels' => [
                    'nl-NL' => 'application/1d-interleaved-parityfec',
                    'en-US' => 'application/1d-interleaved-parityfec',
                ],
            ],
        ],
    ],
    'DONL:DistributionType'           => [
        'name'     => 'DONL:DistributionType',
        'source'   => 'https://waardelijsten.DCAT-ap-donl.nl/donl_distributiontype.json',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://data.overheid.nl/distributiontype/dataschema' => [
                'labels' => [
                    'nl-NL' => 'Data schema',
                    'en-US' => 'Data scheme',
                ],
            ],
        ],
    ],
    'DEXES:Dataspaces' => [
        'name'     => 'DEXES:Dataspaces',
        'source'   => 'https://api.staging.dexes.eu/api/lists/dataspaces',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://open.dexspace.nl' => [
                'labels' => [
                    'nl-NL' => 'DataSpace',
                    'en-US' => 'DataSpace',
                ],
            ],
        ],
    ],
    'DEXES:Policies' => [
        'name'     => 'DEXES:Policies',
        'source'   => 'https://api.staging.dexes.eu/api/lists/policies',
        'aliases'  => [],
        'language' => 'en-US',
        'data'     => [
            'https://dexes.eu/policies/12d' => [
                'labels' => [
                    'nl-NL' => 'policy',
                    'en-US' => 'policy',
                ],
            ],
        ],
    ],
];
