<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Http\Middleware;

use App\Http\Middleware\RedirectIfAuthenticated;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Tests\TestCase;

/**
 * @internal
 */
class RedirectIfAuthenticatedTest extends TestCase
{
    public function testUnauthenticatedUsersAreAllowed(): void
    {
        /** @var RedirectIfAuthenticated $middleware */
        $middleware = app(RedirectIfAuthenticated::class);
        $request    = Request::create('/');

        $this->assertTrue($middleware->handle($request, function() {
            return true;
        }));
    }

    public function testAuthenticatedUsersReceiveRedirectResponse(): void
    {
        /** @var RedirectIfAuthenticated $middleware */
        $middleware = app(RedirectIfAuthenticated::class);
        $request    = Request::create('/');

        $this->actingAs(User::factory()->make());

        $response = $middleware->handle($request, function() {
            return false;
        });

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals(url(RouteServiceProvider::HOME), $response->getTargetUrl());
    }
}
