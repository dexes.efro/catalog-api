<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Http\Middleware;

use App\Http\Middleware\Authenticate;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Tests\TestCase;

/**
 * @internal
 */
class AuthenticateTest extends TestCase
{
    public function testAuthenticatedUsersAreAllowed(): void
    {
        /** @var Authenticate $middleware */
        $middleware = app(Authenticate::class);
        $request    = Request::create('/');

        try {
            $this->actingAs(User::factory()->make());
            $this->assertTrue($middleware->handle($request, function() {
                return true;
            }));
        } catch (AuthenticationException) {
            $this->fail('Authenticated user did not pass through the Authenticate middleware');
        }
    }

    public function testUnauthenticatedUsersResultsInAuthenticationException(): void
    {
        /** @var Authenticate $middleware */
        $middleware = app(Authenticate::class);
        $request    = Request::create('/');

        try {
            $middleware->handle($request, function() {
                $this->fail('Unauthenticated user was allowed to continue');
            });
        } catch (AuthenticationException $e) {
            $this->assertEquals('Unauthenticated.', $e->getMessage());
        }
    }
}
