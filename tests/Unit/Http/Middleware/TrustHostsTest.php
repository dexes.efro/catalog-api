<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Http\Middleware;

use App\Http\Middleware\TrustHosts;
use Tests\TestCase;

/**
 * @internal
 */
class TrustHostsTest extends TestCase
{
    public function testHostsArrayIsNotEmpty(): void
    {
        /** @var TrustHosts $middleware */
        $middleware = app(TrustHosts::class);

        $this->assertNotEmpty($middleware->hosts());
    }
}
