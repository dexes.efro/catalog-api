<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Models;

use App\Models\Offer;
use RuntimeException;
use Tests\TestCase;

/**
 * @internal
 */
class OfferTest extends TestCase
{
    public function testSolrDocumentThrowsRuntimeExceptionOnInvalidJSON(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Offer contains invalid JSON');

        (new Offer([
            'id'   => 1,
            'url'  => 'https://example.com/1',
            'json' => '[{]',
        ]))->asSolrDocument();
    }

    public function testSolrDocumentIncludesDefaultFields(): void
    {
        $offer = new Offer([
            'id'   => 1,
            'url'  => 'https://example.com/1',
            'json' => '[]',
        ]);

        $this->assertEquals([
            'title'           => 'Offer 1',
            'exchange_type'   => 'offer',
            'exchange_json'   => '[]',
            'condition_count' => 0,
            'condition_types' => [],
        ], $offer->asSolrDocument());
    }

    public function testSolrDocumentConditionsEmptyWhenNotProvided(): void
    {
        $json  = json_encode([
            'conditions' => [],
        ]);
        $offer = new Offer([
            'id'   => 1,
            'url'  => 'https://example.com/1',
            'json' => $json,
        ]);

        $this->assertEquals([
            'title'           => 'Offer 1',
            'exchange_type'   => 'offer',
            'exchange_json'   => $json,
            'condition_count' => 0,
            'condition_types' => [],
        ], $offer->asSolrDocument());
    }

    public function testSolrDocumentConditionTypeIsAddedWhenPresent(): void
    {
        $json  = json_encode([
            'conditions' => [[
                'type' => 'foo',
            ]],
        ]);
        $offer = new Offer([
            'id'   => 1,
            'url'  => 'https://example.com/1',
            'json' => $json,
        ]);

        $this->assertEquals([
            'title'           => 'Offer 1',
            'exchange_type'   => 'offer',
            'exchange_json'   => $json,
            'condition_count' => 1,
            'condition_types' => ['foo'],
        ], $offer->asSolrDocument());
    }

    public function testSolrDocumentConditionTypesAreDistinct(): void
    {
        $json  = json_encode([
            'conditions' => [[
                'type' => 'foo',
            ], [
                'type' => 'foo',
            ]],
        ]);
        $offer = new Offer([
            'id'   => 1,
            'url'  => 'https://example.com/1',
            'json' => $json,
        ]);

        $this->assertEquals([
            'title'           => 'Offer 1',
            'exchange_type'   => 'offer',
            'exchange_json'   => $json,
            'condition_count' => 2,
            'condition_types' => ['foo'],
        ], $offer->asSolrDocument());
    }
}
