<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Models;

use App\Models\Dataspace;
use Tests\TestCase;

/**
 * @internal
 */
class DataspaceTest extends TestCase
{
    public function testGetListItemFromDataspace(): void
    {
        $dataspace = new Dataspace([
            'name'             => 'test-dataspace',
            'title'            => 'Test Dataspace',
            'intro_text'       => 'Welcome',
            'asset_url'        => 'asset.url',
            'ruleframework_id' => '1',
            'uri'              => 'https://example.com/test-dataspace',
            'logo_url'         => 'logo.url',
            'join_url'         => 'join.url',
        ]);

        $expected = [
            'https://example.com/test-dataspace' => [
                'labels' => [
                    'nl-NL' => 'Test Dataspace',
                    'en-US' => 'Test Dataspace',
                ],
            ],
        ];

        $result = $dataspace->getListItem();
        $this->assertIsArray($result);
        $this->assertEquals($expected, $result);
    }
}
