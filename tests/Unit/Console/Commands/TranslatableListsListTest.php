<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\TranslatableListsList;
use Illuminate\Console\Command;
use Tests\TestCase;

/**
 * @internal
 */
class TranslatableListsListTest extends TestCase
{
    public function testClassExtendsAppropriateBaseClass(): void
    {
        $this->assertInstanceOf(Command::class, app(TranslatableListsList::class));
    }
}
