<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Services\Lists\TranslatableList;

use App\Services\Lists\ListClient\TranslatableList\TranslatableList;
use Tests\TestCase;

/**
 * @internal
 */
final class TranslatableListTest extends TestCase
{
    public function testBasicGettersAndSetters(): void
    {
        $name     = 'Foo';
        $data     = ['lorem' => 'ipsum'];
        $language = 'en-US';
        $source   = 'unit-test';
        $aliases  = ['Bar', 'Baz'];

        $list = new TranslatableList(
            $name,
            $data,
            $language,
            $source,
            $aliases
        );

        $this->assertEquals($name, $list->getTitle());
        $this->assertEquals($data, $list->getData());
        $this->assertEquals($language, $list->getLanguageCode());
        $this->assertEquals($source, $list->getSource());
        $this->assertEquals($aliases, $list->getAliases());
    }

    public function testSoureGetAsArray(): void
    {
        $name     = 'Foo';
        $data     = ['lorem' => 'ipsum'];
        $language = 'en-US';
        $source   = ['class-string', 'function_name'];
        $aliases  = ['Bar', 'Baz'];

        $list = new TranslatableList(
            $name,
            $data,
            $language,
            $source,
            $aliases
        );

        $this->assertEquals($name, $list->getTitle());
        $this->assertEquals($data, $list->getData());
        $this->assertEquals($language, $list->getLanguageCode());
        $this->assertEquals($source[1], $list->getSource());
        $this->assertEquals($aliases, $list->getAliases());
    }

    public function testSizeIsAccurate(): void
    {
        $list = new TranslatableList('foo', [], 'en-US', '/dev/null');

        $this->assertEquals(0, $list->size());

        $list = new TranslatableList('foo', [
            'Lorem' => [],
            'Ipsum' => [],
        ], 'en-US', '/dev/null'
        );

        $this->assertEquals(2, $list->size());
    }

    public function testLabelIsNotTranslatedWhenNoMatchingIdentifierIsPresent(): void
    {
        $emptyList         = new TranslatableList('foo', [], 'en-US', '/dev/null');
        $listWithoutLabels = new TranslatableList('foo', [
            'Foo' => [],
        ], 'en-US', '/dev/null');
        $listWithoutLanguageLabel = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [
                    'nl-NL' => 'Hello there',
                ],
            ],
        ], 'en-US', '/dev/null');
        $listWithoutMatchingLabels = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [
                    'nl-NL' => 'Ipsum',
                ],
            ],
        ], 'en-US', '/dev/null');

        $this->assertEquals('Lorem', $emptyList->labelToIdentifier('Lorem'));
        $this->assertEquals('Lorem', $listWithoutLabels->labelToIdentifier('Lorem'));
        $this->assertEquals('Lorem', $listWithoutLanguageLabel->labelToIdentifier('Lorem'));
        $this->assertEquals('Lorem', $listWithoutMatchingLabels->labelToIdentifier('Lorem'));
    }

    public function testLabelIsTranslatedToIdentifierWhenMatchIsFound(): void
    {
        $list = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [
                    'en-US' => 'Ipsum',
                ],
            ],
        ], 'en-US', '/dev/null');

        $this->assertEquals('Foo', $list->labelToIdentifier('Ipsum'));
    }

    public function testIdentifierIsNotTranslatedWhenNoMatchingLabelIsPresent(): void
    {
        $emptyList         = new TranslatableList('foo', [], 'en-US', '/dev/null');
        $listWithoutLabels = new TranslatableList('foo', [
            'Foo' => [],
        ], 'en-US', '/dev/null');
        $listWithWrongLanguageLabels = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [
                    'nl-NL' => 'Bar',
                ],
            ],
        ], 'en-US', '/dev/null');

        $this->assertEquals('Foo', $emptyList->identifierToLabel('Foo'));
        $this->assertEquals('Foo', $listWithoutLabels->identifierToLabel('Foo'));
        $this->assertEquals('Foo', $listWithWrongLanguageLabels->identifierToLabel('Foo'));
    }

    public function testIdentifierIsTranslatedWhenMatchIsFound(): void
    {
        $list = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [
                    'en-US' => 'Bar',
                ],
            ],
        ], 'en-US', '/dev/null');

        $this->assertEquals('Bar', $list->identifierToLabel('Foo'));
    }

    public function testHasFieldReturnsFalseIfIdentifierIsNotPresent(): void
    {
        $list = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [
                    'en-US' => 'Bar',
                ],
            ],
        ], 'en-US', '/dev/null');

        $this->assertFalse($list->entryHasField('Lorem', 'Ipsum'));
    }

    public function testHasFieldReturnsFalseWhenFieldIsNotPresent(): void
    {
        $list = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [
                    'en-US' => 'Bar',
                ],
            ],
        ], 'en-US', '/dev/null');

        $this->assertFalse($list->entryHasField('Foo', 'Ipsum'));
    }

    public function testHasFieldReturnsFalseWhenFieldIsEmpty(): void
    {
        $list = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [],
            ],
        ], 'en-US', '/dev/null');

        $this->assertFalse($list->entryHasField('Foo', 'labels'));
    }

    public function testHasFieldReturnsTrueWhenFieldIsNotEmpty(): void
    {
        $list = new TranslatableList('foo', [
            'Foo' => [
                'labels' => [
                    'en-US' => 'Bar',
                ],
            ],
        ], 'en-US', '/dev/null');

        $this->assertTrue($list->entryHasField('Foo', 'labels'));
    }
}
