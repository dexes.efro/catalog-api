<?php

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Services\Lists\TranslatableList;

use App\Services\Lists\ListClient\TranslatableList\DataFormatter;
use App\Services\Lists\ListClient\TranslatableList\TranslatableList;
use Tests\TestCase;

/**
 * @internal
 */
final class DataFormatterTest extends TestCase
{
    public function testFlattenReturnsAnEmptyArrayForEmptyList(): void
    {
        $formatter = new DataFormatter();
        $list      = new TranslatableList('foo', [], 'en-US', '');

        $this->assertEquals([], $formatter->flatten($list));
    }

    public function testFlattenReturnsAnEmptyArrayForDataWithoutLabels(): void
    {
        $listData = [[
            'foo' => 'bar',
        ], [
            'baz' => 'lorem',
        ]];

        $formatter = new DataFormatter();
        $list      = new TranslatableList('foo', $listData, 'en-US', '');

        $this->assertEquals([], $formatter->flatten($list));
    }

    public function testFlattenReturnsAnEmptyArrayForDataWithoutSpecificLanguageLabels(): void
    {
        $listData = [[
            'labels' => [
                'bar' => 'baz',
            ],
        ], [
            'labels' => [
                'foo' => 'lorem',
            ],
        ]];

        $formatter = new DataFormatter();
        $list      = new TranslatableList('foo', $listData, 'en-US', '');

        $this->assertEquals([], $formatter->flatten($list));
    }

    public function testFlattenReturnsOnlyValidEntries(): void
    {
        $listData = [
            'my-identifier' => [
                'labels' => [
                    'en-US' => 'baz',
                ],
            ],
            'some-other-identifier' => [
                'labels' => [
                    'foo' => 'lorem',
                ],
            ],
        ];

        $formatter = new DataFormatter();
        $list      = new TranslatableList('foo', $listData, 'en-US', '');

        $this->assertEquals([
            'my-identifier' => 'baz',
        ], $formatter->flatten($list));
    }

    public function testFlattenUsesCacheStoreOnSubsequentRequests(): void
    {
        $formatter = new DataFormatter();
        $list      = $this->getMockBuilder(TranslatableList::class)
            ->disableOriginalConstructor()
            ->getMock();

        $list->expects($this->exactly(3))
            ->method('getData')
            ->willReturn([
                'my-special-identifier' => [
                    'labels' => [
                        'en-US' => 'baz',
                    ],
                ],
            ]);

        $formatter->flatten($list);
        $formatter->flatten($list);
    }
}
