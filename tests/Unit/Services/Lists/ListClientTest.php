<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Services\Lists;

use App\Services\Lists\ListClient\ListClient;
use App\Services\Lists\ListClient\TranslatableList\DataFormatter;
use App\Services\Lists\ListClient\TranslatableList\TranslatableListFactory;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class ListClientTest extends TestCase
{
    public function testSingleName(): void
    {
        /** @var TranslatableListFactory $listFactory */
        $listFactory = $this->getMockBuilder(TranslatableListFactory::class)
            ->disableOriginalConstructor()->getMock();

        /** @var DataFormatter $dataFormatter */
        $dataFormatter = $this->getMockBuilder(DataFormatter::class)->getMock();

        $knownLists = [
            'foo' => [
                'name'    => 'foo',
                'source'  => 'bar',
                'aliases' => [
                    'baz',
                ],
            ],
        ];

        $listClient = new ListClient($listFactory, $dataFormatter, $knownLists);

        $this->assertEquals(array_keys($knownLists), $listClient->names());
    }

    public function testMultipleNames(): void
    {
        /** @var TranslatableListFactory $listFactory */
        $listFactory = $this->getMockBuilder(TranslatableListFactory::class)
            ->disableOriginalConstructor()->getMock();

        /** @var DataFormatter $dataFormatter */
        $dataFormatter = $this->getMockBuilder(DataFormatter::class)->getMock();

        $knownLists = [
            'foo' => [
                'name'    => 'foo',
                'source'  => 'bar',
                'aliases' => [
                    'baz',
                ],
            ],
            'foo2' => [
                'name'    => 'foo2',
                'source'  => 'bar2',
                'aliases' => [
                    'baz2',
                ],
            ],
        ];

        $listClient = new ListClient($listFactory, $dataFormatter, $knownLists);

        $this->assertEquals(array_keys($knownLists), $listClient->names());
    }
}
