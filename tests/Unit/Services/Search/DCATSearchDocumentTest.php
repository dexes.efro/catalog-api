<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/catalog-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Services\Search;

use App\Services\Search\DCATSearchDocument;
use Tests\TestCase;

/**
 * @internal
 */
class DCATSearchDocumentTest extends TestCase
{
    public function testDatasetSearchDocumentDatasetMappingWithEmptyDataset(): void
    {
        $this->assertEquals([
            'sys_type' => 'dataset',
        ], DCATSearchDocument::fromDataset([])->getFields());
    }

    public function testDatasetFieldsAreNotMappedWithoutMapping(): void
    {
        config(['xs-content-types.dexes-dataset.search.mapping' => []]);

        $this->assertEquals([
            'sys_type' => 'dataset',
        ], DCATSearchDocument::fromDataset([
            'some-field' => 'some-value',
        ])->getFields());
    }

    public function testDatasetPropertyIsMappedWhenAvailableAndMappingIsPresent(): void
    {
        config(['xs-content-types.dexes-dataset.search.mapping.my-field' => ['my-target-field']]);

        $this->assertEquals([
            'sys_type'        => 'dataset',
            'my-target-field' => ['my-value'],
        ], DCATSearchDocument::fromDataset([
            'my-field' => 'my-value',
        ])->getFields());
    }

    public function testDatasetPropertyIsMappedToMultipleFieldsWhenAvailableAndMappingIsPresent(): void
    {
        config(['xs-content-types.dexes-dataset.search.mapping.my-field' => [
            'my-target-field',
            'another-target-field',
        ]]);

        $this->assertEquals([
            'sys_type'             => 'dataset',
            'my-target-field'      => ['my-value'],
            'another-target-field' => ['my-value'],
        ], DCATSearchDocument::fromDataset([
            'my-field' => 'my-value',
        ])->getFields());
    }

    public function testMultipleDatasetFieldsCanBeMappedToSameTargetKey(): void
    {
        config(['xs-content-types.dexes-dataset.search.mapping' => [
            'my-first-field'  => ['my-target-field'],
            'my-second-field' => ['my-target-field'],
        ]]);

        $this->assertEquals([
            'sys_type'        => 'dataset',
            'my-target-field' => ['my-value', 'another-value'],
        ], DCATSearchDocument::fromDataset([
            'my-first-field'  => 'my-value',
            'my-second-field' => 'another-value',
        ])->getFields());
    }

    public function testBooleanValuesAreConvertedToKeyNamesOrIgnored(): void
    {
        config(['xs-content-types.dexes-dataset.search.mapping' => [
            'my-field' => ['my-target-field'],
        ]]);

        $this->assertEquals([
            'sys_type'             => 'dataset',
            'my-target-field'      => ['my-field'],
        ], DCATSearchDocument::fromDataset([
            'my-field' => true,
        ])->getFields());

        $this->assertEquals([
            'sys_type'             => 'dataset',
        ], DCATSearchDocument::fromDataset([
            'my-field' => false,
        ])->getFields());
    }
}
